import Swal from 'sweetalert2'
import loading from './loading.gif'
function PleaseWaitLoader(){
    this.start = function (title){
        return(
            Swal.fire({
                title: title,
                imageUrl: loading,
                imageWidth: 100,
                imageHeight: 100,
                allowOutsideClick: false,
                showConfirmButton: false,
            })
        )
    }
    this.stop = function (){
        Swal.close()
    }
}
export default PleaseWaitLoader