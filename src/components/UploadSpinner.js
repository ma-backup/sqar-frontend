import Button from 'react-bootstrap/Button';
import Spinner from 'react-bootstrap/Spinner';

function UploadSpinner() {
  return (
    <>
      <Button className='btn primary' disabled style={{zIndex: 9999, position: 'fixed', top: '40%', left: '46%'}}>
        <Spinner
          as="span"
          animation="grow"
          size="sm"
          role="status"
          aria-hidden="true"
        />
        &nbsp;<i className="fa-solid fa-upload"></i> Uploading...
      </Button>
    </>
  );
}

export default UploadSpinner;