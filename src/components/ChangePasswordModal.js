import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'
import {useState} from 'react'
import { useEffect } from 'react'
import Swal from 'sweetalert2'
import PleaseWaitLoader from './PleaseWaitLoader'
export default function ChangePasswordModal(props){
    const swalLoading = new PleaseWaitLoader()
    const [currentPassword, setCurrentPassword] = useState('')
    const [newPassword, setNewPassword] = useState('')
    const [newPassword2, setNewPassword2] = useState('')
    useEffect(() => {
        if(props.show){
            document.getElementById('currentpass').focus()
        }        
    }, [props.show])

    async function ChangePassword(){
        Swal.fire({
            title: 'Confirmation',
            text: 'Are you sure to change your password? This will also log you out!',
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3C5393',
            confirmButtonText: "Yes, I know what I'm doing"
        }).then((res) => {
            if(res.isConfirmed){
                swalLoading.start('Processing...')
                fetch(`${process.env.REACT_APP_SQAR_API}/api/ChangePassword`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        empNum: localStorage.getItem('empNum'),
                        password: currentPassword,
                        newPassword: newPassword
                    })
                })
                .then(response => response.json())
                .then(result => {
                    swalLoading.stop()
                    Swal.fire({
                        title: result.success ? 'Success' : 'Error',
                        icon: result.success ? 'success' : 'error',
                        text: result.message,
                        confirmButtonColor: '#3C5393',
                    }).then(() => {
                        if(result.success){
                            window.location.href = '/logout'
                        } 
                    })                               
                })
            }            
        })
    }

    function HandleKeyPress(e){
        if(!currentPassword || !newPassword || !newPassword2 || !(newPassword == newPassword2)){
            return
        }
        if(e.key == 'Enter'){
            ChangePassword()
        }
    }
    return(
        <Modal
            {...props}
            size="md"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                Change Password
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <label htmlFor="currentpass">Enter current password</label>
                <input
                id='currentpass'
                placeholder='Current Password'
                className="form-control"
                type='password'
                value={currentPassword}
                onChange={(e) => setCurrentPassword(e.target.value)}
                onKeyPress={HandleKeyPress}
                />
                <hr/>
                <label htmlFor="newpass">Enter new password</label>
                <input
                id='newpass'
                placeholder='New Password'
                className="form-control"
                type='password'
                value={newPassword}
                onChange={(e) => setNewPassword(e.target.value)}
                onKeyPress={HandleKeyPress}
                />
                <label htmlFor="newpass2">Re-enter new password</label>
                <input
                id='newpass2'
                placeholder='Re-enter New Password'
                className={(newPassword != newPassword2 && newPassword2 != '') ? "form-control is-invalid" : (newPassword2 != '') ? "form-control is-valid" : "form-control"}
                type='password'
                value={newPassword2}
                onChange={(e) => setNewPassword2(e.target.value)}
                onKeyPress={HandleKeyPress}
                />
                <div className="invalid-feedback">
                    <i className="bx bx-radio-circle"></i>
                    Passwords don't match.
                </div>
            </Modal.Body>
            <Modal.Footer>
                <Button className='ma-gray-button' onClick={() => props.onHide()}>
                <i className="fa-solid fa-xmark"></i> Close
                </Button>
                <Button
                className='ma-blue-button'
                disabled={!currentPassword || !newPassword || !newPassword2 || !(newPassword == newPassword2)}
                onClick={ChangePassword}
                >
                    <i className="fa-solid fa-check"></i> Save
                </Button>
            </Modal.Footer>
        </Modal>
    )
}