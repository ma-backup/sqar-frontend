import React from 'react'
import {Table, Container, Row, Col, Button, Form, Tooltip, Dropdown, Card, Badge, Modal, ButtonGroup, InputGroup} from 'react-bootstrap'
import Swal from 'sweetalert2'
import TextareaAutosize from 'react-textarea-autosize'
import Select from 'react-select'

import {useState, useEffect, useContext} from 'react'
import {useNavigate} from 'react-router-dom'

import Loading from '../components/Loading'
import UploadSpinner from '../components/UploadSpinner'
import assuredlogo from '../components/assured-logo.jpg'
import ViewContext from '../ViewContext'

export default function ForSqarProcess(){

	const navigate = useNavigate()
	const {view, setView} = useContext(ViewContext)
	const {forSqarData, setForSqarData} = useContext(ViewContext)
	const {sqarWaiter, setSqarWaiter} = useContext(ViewContext)
	const {forProcessWaiter, setForProcessWaiter} = useContext(ViewContext)
	const {majorDefects} = useContext(ViewContext)
	const {techSuppMngrSupplier} = useContext(ViewContext)

	const viewSqar = () => {
		setView({
			viewForProcess: false
		})
		navigate("/")
	}

  	const renderTooltipGenerateSQAR = (props) => (
	    <Tooltip id="button-tooltip" {...props}>
	      Process SQAR
	    </Tooltip>


	    // <OverlayTrigger
	    //   placement="right"
	    //   delay={{ show: 250, hide: 400 }}
	    //   overlay={renderTooltipGenerateSQAR}
	    // >
	    // </OverlayTrigger>
	);

  	//for modal losebutton
	const handleClose = () => {
		setShowSqarForm(false)
		setCurrentSelectedImage(null)
		setAddIllustration(false)
		setIllustrations([])
		setIllustrationScript([])
		setImageTitle('')
		setDefects([])
		setIsdefectMajor(false)
		setIsDefectAffectFunctional(false)
		setIsCriticalNGCheckPoint(false)
		setIsIncludeDimensionalDefect(false)

		setLotNo('')
		setRowNumber('')
		setDateReceived('')
		setPoNumber('')
		setInvoice('')
		setPartNo('')
		setPartName('')
		setInvoiceQty('')
		setAffectedQty('')
		setSampleSize('')
		setSupplier('')
		setMaker('')
		setMaterialType('')
		setMatCodeBoxSeqId('')
	}

	//date converter 
	const convertDate = (date) => {

		let month = date.slice(5, 7)
		let day = date.slice(8, 10)
		let year = date.slice(0, 4)

		let months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
		
		let newDate = months[parseInt(month)-1] + " " + day + ", " + year
		return newDate
	}
	
	let currentPageItems = [];

	const [finalSqarDataPageLength, setFinalSqarDataPageLength] = useState(0) //for pagination
	const [currentPage, setCurrentPage] = useState(1)
	const [forProcess, setForProcess] = useState([])
	const [searchKeyword, setSearchKeyword] = useState('')
	const [isLoading, setIsLoading] = useState(false)
	const [isUploading, setIsUploading] = useState(false)
	const [viewTable, setViewTable] = useState(true)
	const [showSqarForm, setShowSqarForm] = useState(false)
	
	// const [defectRank, setDefectRank] = useState('')
	const [functionalTrblClass, setFunctionalTrblClass] = useState(false)
	const [isDefectMajor, setIsdefectMajor] = useState(false)
	// const [isIncludeTechSuppMngrSuppier, setIsIncludeTechSuppMngrSuppier] = useState(0)
	const [isDefectAffectFunctional, setIsDefectAffectFunctional] = useState(false)
	const [rowNumber, setRowNumber] = useState('')
	const [dateReceived, setDateReceived] = useState('')
	const [poNumber, setPoNumber] = useState('')
	const [invoice, setInvoice] = useState('')
	const [partNo, setPartNo] = useState('')
	const [partName, setPartName] = useState('')
	const [lotNo, setLotNo] = useState('')
	const [invoiceQty, setInvoiceQty] = useState('')
	const [affectedQty, setAffectedQty] = useState('')
	const [inputQty, setInputQty] = useState(0)
	const [sampleSize, setSampleSize] = useState(0)	
	const [rejectionRate, setRecjectionRate] = useState(0)	
	const [supplier, setSupplier] = useState('')
	const [maker, setMaker] = useState('')
	const [materialType, setMaterialType] = useState('')
	const [defects, setDefects] = useState(Array)
	const [refundDebitChecked, setRefundDebitChecked] = useState(false)
	const [isCriticalNGCheckPoint, setIsCriticalNGCheckPoint] = useState(false)
	const [isIncludeDimensionalDefect, setIsIncludeDimensionalDefect] = useState(false)
	const [matCodeBoxSeqId, setMatCodeBoxSeqId] = useState('')
	const [partNameOptions, setPartNameOptions] = useState([])
	const [isGettingPartNameOptions, setIsGettingPartNameOptions] = useState(false)
	
	const [rejectQty, setRejectQty] = useState('')
	const [defects1, setDefects1] = useState(Array)

	const [rejectQty2, setRejectQty2] = useState('')
	const [defects2, setDefects2] = useState(Array)

	const [rejectQty3, setRejectQty3] = useState('')
	const [defects3, setDefects3] = useState(Array)

	const [totalRejectQty, setTotalRejectQty] = useState(0)

	const [sqarControlNo, setSqarControlNo] = useState('')
	const [defectInfoNo, setDefectInfoNo] = useState('')


	const [addIllustration, setAddIllustration] = useState(false)
	const [currentSelectedImage, setCurrentSelectedImage] = useState(null)
	const [imageTitle, setImageTitle] = useState('')
	const [illustrations, setIllustrations] = useState([])
	const [illustrationScript, setIllustrationScript] = useState([])
	const addIllustrationScript = () =>{
		return(
			<>
			<tr>
				<td style={{width: '50%', textAlign: 'center', verticalAlign: 'middle'}}
					rowSpan='2'
				>
					<center>
					{
						currentSelectedImage != null ?
							<img alt="not found" width={'100%'} className="img-fluid" src={URL.createObjectURL(currentSelectedImage)} />

						:
							<Form.Group controlId="formFile" className="mb-3">
						        <Form.Label><i className="fa-solid fa-photo-film"></i> Photo of defective material</Form.Label>
						        <Form.Control 
						        style={{width: 400}} 
						        type="file"
						        onChange={(event) => {
						          setCurrentSelectedImage(event.target.files[0]);
						        }}
								/>
						    </Form.Group>
					}
				    </center>
				</td>
				<td style={{height: 18}}>
					<input
						style={{width: '100%', height: '100%', border: 'none', fontSize: 16, fontWeight: 'bold'}}
						placeholder='<Image title>'
						id='imageTitle'
						defaultValue={imageTitle}
						onChange={(event) => setImageTitle(event.target.value)}
					/>
				</td>
			</tr>
			<tr>		        				
				<td>
					<TextareaAutosize
						minRows={5}
						style={{width: '100%', height: '100%', border: 'none', whiteSpace: 'pre-wrap'}}
						rows='4'
						placeholder='<Image Description>'
						id='imageDescription'
					>
					</TextareaAutosize>
				</td>
			</tr>
			<tr>
				<td colSpan='2'>
					<Button className="ma-gray-button mx-1"
        					onClick={()=>cancelAttach()}
        			>
        				<i className="fa-solid fa-xmark"></i> CANCEL
        			</Button>
					{
						currentSelectedImage && imageTitle ?
							<>
							<Button
								className='ma-blue-button'
								onClick={() => attachIllustration()}
							>
								<i className="fa-solid fa-link"></i> ATTACH
							</Button>							
							</>
						:
							<Button className='ma-blue-button' disabled><i className="fa-solid fa-link"></i> ATTACH</Button>
					}
					{
						currentSelectedImage ?
							<Button 
					 			className="ma-red-button mx-1" onClick={()=>setCurrentSelectedImage(null)}>
					 			<i className="fa-solid fa-eraser"></i> REMOVE PHOTO
					 		</Button>
					 	:
					 		''
					}	
				</td>
			</tr>
			</>
			)
	}	
	const cancelAttach = () => {
		setCurrentSelectedImage(null)
		setImageTitle('')		
		document.getElementById('imageDescription').value = ''
		setAddIllustration(false)
	}
	const attachIllustration = () => {
		let newItem = {
			image: currentSelectedImage,
			title: document.getElementById('imageTitle').value,
			description: document.getElementById('imageDescription').value
		}
		setIllustrations(currentItems => [...currentItems, newItem])
		setCurrentSelectedImage(null)
		setImageTitle('')
		setAddIllustration(false)
		document.getElementById('imageDescription').value = ''
	}
	const buildIllustrationScript = () => {
		if(illustrations.length > 0){
			setIllustrationScript(
				illustrations.map((item, index) => {
					return (
						<>
						<tr>
							<td style={{width: '50%', textAlign: 'center', verticalAlign: 'middle'}}
								rowSpan='2'
							>
								<center>
								{												
									<img alt="not found" width={'100%'} className="img-fluid" src={URL.createObjectURL(item.image)} />
								}
								</center>
							</td>
							<td style={{height: 18, fontWeight: 'bold', fontSize: 16}}>
								{item.title}
								<button 
								className="btn btn-danger align-right px-2 py-0"
								onClick={() => {
									setIllustrations(currentIllustrations => {
										return currentIllustrations.filter((_, i) => i != index)
									})
									setIllustrationScript(currentIllustrationsScript => {
										return currentIllustrationsScript.filter((_, i) => i != index)
									})							
								}}
								>
									<i className="fa-solid fa-xmark"></i>
								</button>
							</td>
						</tr>
						<tr>		        				
							<td style={{whiteSpace: 'pre-wrap'}}>
								{item.description}
							</td>
						</tr>
						</>
					)
				})
			)
		}
	}
	useEffect(() => {
		buildIllustrationScript()
	}, [illustrations])

	const getData = (data) => {
		setPartNameOptions([])
		setIsGettingPartNameOptions(true)
		console.log(techSuppMngrSupplier)
		console.log(majorDefects)
		console.log(data)

		fetch(`${process.env.REACT_APP_SQAR_API}/api/for-sqar-process/get-rejects`,{
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				"INVOICE": data.INVOICE,
				"MATCODEBOX": data.MATCODEBOX
			})
		})
		.then(response => response.json())
		.then(result => {
			console.log(result)
			
			let totalDefectQty = 0
			for(let i = 0; i < result.length; i++){
				for(let j = 0; j < result[i].length; j++){
					if(result[i][j].DEFECT_QTY != null 
					&& result[i][j].DEFECT_QTY != '' 
					&& result[i][j].DEFECT != null 
					&& result[i][j].DEFECT != ''){

						console.log(result[i][j].DEFECT)
						console.log(result[i][j].DEFECT_QTY)
						let newDefectItem = {
							defect: result[i][j].DEFECT,
							defectQty: result[i][j].DEFECT_QTY
						}

						setDefects(currentItems => [...currentItems, newDefectItem])

						totalDefectQty += parseFloat(result[i][j].DEFECT_QTY)						

						if(data.MATERIAL_TYPE == 'METAL'){
							for(let k = 0; k < majorDefects.METAL_MAJOR_DEFECTS.length; k++){
								if(result[i][j].DEFECT.toLowerCase().search(majorDefects.METAL_MAJOR_DEFECTS[k].toLowerCase()) > -1){
									setIsdefectMajor(true)
									console.log('MAJOR DEFECT DETECTED ====>>>> ' + result[i][j].DEFECT)
									if(i == 2){
										setIsDefectAffectFunctional(true)
										console.log('DEFECT THAT AFFECT FUNCTIONAL DETECTED ====>>>> ' + result[i][j].DEFECT)
									}
								}
							}
						}

						if(data.MATERIAL_TYPE == 'PLASTIC'){
							for(let k = 0; k < majorDefects.PLASTIC_MAJOR_DEFECTS.length; k++){
								if(result[i][j].DEFECT.toLowerCase().search(majorDefects.PLASTIC_MAJOR_DEFECTS[k].toLowerCase()) > -1){
									setIsdefectMajor(true)
									console.log('MAJOR DEFECT DETECTED ====>>>> ' + result[i][j].DEFECT)
									if(i == 2){
										setIsDefectAffectFunctional(true)
										console.log('DEFECT THAT AFFECT FUNCTIONAL DETECTED ====>>>> ' + result[i][j].DEFECT)
									}
								}
							}
						}

						if(data.MATERIAL_TYPE == 'ELECTRICAL'){
							for(let k = 0; k < majorDefects.PCB_MAJOR_DEFECTS.length; k++){
								if(result[i][j].DEFECT.toLowerCase().search(majorDefects.PCB_MAJOR_DEFECTS[k].toLowerCase()) > -1){
									setIsdefectMajor(true)
									console.log('MAJOR DEFECT DETECTED ====>>>> ' + result[i][j].DEFECT)
									if(i == 2){
										setIsDefectAffectFunctional(true)
										console.log('DEFECT THAT AFFECT FUNCTIONAL DETECTED ====>>>> ' + result[i][j].DEFECT)
									}
								}
							}
						}

						if(i == 1){
							setIsIncludeDimensionalDefect(true)
							console.log("Defects include dimensional")
						}
					}
				}
			}
			setTotalRejectQty(totalDefectQty)
		})

		fetch(`${process.env.REACT_APP_SQAR_API}/api/for-sqar-process/check-critical-ng-checkpoint`, {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				"INVOICE": data.INVOICE,
				"MATCODEBOX": data.MATCODEBOX
			})
		})
		.then((response) => response.json())
		.then((result) => {
			if(result[0].CriticalCheckPointNGCount > 0){
				setIsCriticalNGCheckPoint(true)
			}
		})

		setRowNumber(data.ROW_NUMBER)
		setDateReceived(data.DATE_RECEIVED)
		setPoNumber(data.PO_NUMBER)
		setInvoice(data.INVOICE)
		setPartNo(data.PART_NO)
		setPartName(data.PART_NAME)
		setInvoiceQty(data.INVOICE_QTY)
		setAffectedQty(data.AFFECTED_QTY)
		setSampleSize(data.SAMPLE_SIZE)
		setSupplier(data.SUPPLIER)
		setMaker(data.MAKER)
		setMaterialType(data.MATERIAL_TYPE)
		setMatCodeBoxSeqId(data.MATCODEBOX)
		setShowSqarForm(true)

		//GENERATE SQAR NUMBER 
		//get count of sqars for sqar control no
		// fetch(`${process.env.REACT_APP_SQAR_API}/api/sqar/count`)
		// .then(response => response.json())
		// .then(result => {
		// 	let first3Char = "MAT"
		// 	let yearLast2Digits = new Date().getFullYear().toString().slice(-2)
		// 	let sqarLastNumbers
		// 	if(result.length > 0){
		// 		if(result[0].SQAR_COUNTS != ''){
		// 			sqarLastNumbers = parseInt(result[0].SQAR_COUNTS) + 1
		// 		}else{
		// 			sqarLastNumbers = 1
		// 		}
		// 	}else{
		// 		sqarLastNumbers = 1
		// 	}	
		// 	setSqarControlNo(first3Char + "-" + yearLast2Digits + "-" + ((sqarLastNumbers < 100) ? (sqarLastNumbers < 10) ? '00' + sqarLastNumbers : '0' + sqarLastNumbers : sqarLastNumbers))
		// })

		//GENERATE SQAR NUMBER UPDATE
		fetch(`${process.env.REACT_APP_SQAR_API}/api/sqar/get-last-sqar-control-no`)
		.then(response => response.json())
		.then(result => {
			let first3Char = "MAT"
			let currYearStr = (new Date()).getFullYear().toString().slice(2, 4)
			let currMonthStr = ((new Date()).getMonth() + 1).toString().length == 1 ? "0" + ((new Date()).getMonth() + 1).toString() : ((new Date()).getMonth() + 1).toString()
			let newSqarNo = ''
			if(result.length > 0){
				let latestSqarControlNo = result[0].SQAR_Control_Number
				let sqarYear = latestSqarControlNo.slice(3, 5)
				let sqarMonth = latestSqarControlNo.slice(5, 7)
				let sqarNo = latestSqarControlNo.slice(7, 9)
				let sqarNoPlus1Str = (parseInt(sqarNo) + 1).toString()
				
 				if(sqarYear != currYearStr){
					newSqarNo = first3Char + currYearStr + currMonthStr + "01"
				}else{
					if(sqarMonth != currMonthStr){ 
						newSqarNo = first3Char + currYearStr + currMonthStr + "01"
					}else{
						newSqarNo = sqarNoPlus1Str.length == 1 ? first3Char + currYearStr + currMonthStr + "0" + sqarNoPlus1Str : first3Char + currYearStr + currMonthStr + sqarNoPlus1Str
					}
				}
				setSqarControlNo(newSqarNo)
			}else{
				newSqarNo = first3Char + currYearStr + currMonthStr + "01"
				setSqarControlNo(newSqarNo)
			}
		})

		//GET AFFECTED LOT NUMBERS
		fetch(`${process.env.REACT_APP_SQAR_API}/api/for-sqar-process/get-affected-lot-numbers`,{
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				matCodeBoxSeqId: data.MATCODEBOX
			})
		})
		.then(response => response.json())
		.then(result => {
			let lotStr = ''
			for(let i = 0; i < result.length; i++){
				if(i > 0){
					lotStr += " | "
				}
				lotStr += result[i]
			}
			setLotNo(lotStr)
		})

		//GET PARTNAMES
		fetch(`${process.env.REACT_APP_SQAR_API}/api/ms21-get-part-name`, {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				partNo: data.PART_NO
			})
		})
		.then(response => response.json())
		.then(result => {
			result.map(item => {
				setPartNameOptions(currentItems => [...currentItems, {value: item.partName, label: item.partName}])
			})
			setIsGettingPartNameOptions(false)
		})
	}

	const generateSqar = () => {
		setIsUploading(true)
		//get data entered in modal
		let noOfIssuedSqar = document.getElementById('noIssuedSqarMonth').value
		let withSqarResponse = document.getElementById('withSqarResponseMonth').value
		let applicableModel = document.getElementById('applicableModel').value
		let contactPerson = document.getElementById('attentionTo').value
		let deffectOccurence = document.getElementById('deffectOccurence').value
		let inputQuantity = document.getElementById('inputQuantity').value
		let sampleSize = document.getElementById('sampleSize').value
		let remarks = document.getElementById('remarks').value
		let ngAmount = document.getElementById('ngAmount').value
		let defectRank = document.getElementById('defectRank').innerText
		let affectedLotNos = document.getElementById('lotNos').value
		console.log(affectedLotNos)
		let isIncludeTechSuppMngrSuppier = 0;

		//CHECK IF NEED TO INCLUDE SUPP TECH MANAGER
		if(materialType == 'PLASTIC' && techSuppMngrSupplier.includes(supplier) && defectRank == 'A'){
			console.log('include sir fernan')
			isIncludeTechSuppMngrSuppier = 1;
		}

		let defectsArrStr = "["
		let defectsQtyArrStr = "["
		let defectsContainerArr = ['defectOne', 'defectTwo', 'defectThree', 'defectFour', 'defectFive', 'defectSix', 'defectSeven', 'defectEight', 'defectNine']
		let defectsQtyContainerArr = ['rejectQty1', 'rejectQty2', 'rejectQty3', 'rejectQty4', 'rejectQty5', 'rejectQty6', 'rejectQty7', 'rejectQty8', 'rejectQty9']
		let firstLoop = true

		//this is for the new created table for the defects
		let sqarDefects = []

		for(let i = 0; i < defectsContainerArr.length; i++){
			if(document.getElementById(defectsContainerArr[i]).value != '' 
				&& document.getElementById(defectsQtyContainerArr[i]).value != ''){
				
				if(!firstLoop){
					 // defectsArrStr += ', '
					 defectsQtyArrStr += ', '
				}
				defectsArrStr += "[" + document.getElementById(defectsContainerArr[i]).value + "]"
				defectsQtyArrStr += document.getElementById(defectsQtyContainerArr[i]).value
				
				sqarDefects.push({
					defect: document.getElementById(defectsContainerArr[i]).value,
					quantity: document.getElementById(defectsQtyContainerArr[i]).value
				})

				firstLoop = false
			}
		}
		defectsArrStr += "]"
		defectsQtyArrStr += "]"

		let troubleClassification = []
		let troubleClassificationSTRING = ''
		if(document.getElementById('documentsLabelsCheckbox').checked){
			troubleClassification.push('DOCUMENTS/LABELS')
		}
		if(document.getElementById('physicalPropertiesCheckbox').checked){
			troubleClassification.push('PHYSICAL PROPERTIES')
		}
		if(document.getElementById('dimensionalCheckbox').checked){
			troubleClassification.push('DIMENSIONAL')
		}
		if(document.getElementById('functionalCheckbox').checked){
			troubleClassification.push('FUNCTIONAL')
		}
		if(document.getElementById('greenPurchasingCheckbox').checked){
			troubleClassification.push('GREEN PURCHASING STD.')
		}		
		for(let i = 0; i < troubleClassification.length; i++){
			if(i > 0){
				troubleClassificationSTRING += ', '
			}
			troubleClassificationSTRING += troubleClassification[i]
		}

		let affectedLotARRAY = []
		let affectedLotSTRING = ''
		let affectedLotIsRTV = 0
		let affectedLotIsOkToUse = 0
		if(document.getElementById('rtvCheckbox').checked){
			// affectedLotARRAY.push('RTV')
			affectedLotIsRTV = 1
		}
		if(document.getElementById('okToUseCheckbox').checked){
			// affectedLotARRAY.push('OK TO USE')
			affectedLotIsOkToUse = 1
		}
		if(document.getElementById('supplierSortingCheckBox').checked){
			affectedLotARRAY.push('SUPPLIER SORTING / REWORK')
		}
		if(document.getElementById('matSortingCheckbox').checked){
			affectedLotARRAY.push('MAT SORTING / REWORK (WITH CHARGE SHEET)')
		}
		for(let i = 0; i < affectedLotARRAY.length; i++){
			if(i > 0){
				affectedLotSTRING += ', '
			}
			affectedLotSTRING += affectedLotARRAY[i]
		}

		let defectiveMaterialARRAY = []
		let defectiveMaterialSTRING = ''
		if(document.getElementById('replacementCheckbox').checked){
			defectiveMaterialARRAY.push('REPLACEMENT')
		}
		if(document.getElementById('refundDebitCheckbox').checked){
			defectiveMaterialARRAY.push('REFUND / DEBIT')
		}
		for(let i = 0; i < defectiveMaterialARRAY.length; i++){
			if(i > 0){
				defectiveMaterialSTRING += ', '
			}
			defectiveMaterialSTRING += defectiveMaterialARRAY[i]
		}

		let ngMaterialDisposalARRAY = []
		let ngMaterialDisposalSTRING = ''
		if(document.getElementById('matDisposalCheckbox').checked){
			ngMaterialDisposalARRAY.push('MAT DISPOSAL FEE BY SUPPLIER')
		}
		if(document.getElementById('ngMatRTVCheckbox').checked){
			ngMaterialDisposalARRAY.push('RTV')
		}
		for(let i=0; i < ngMaterialDisposalARRAY.length; i++){
			if(i > 0){
				ngMaterialDisposalSTRING += ', '
			}
			ngMaterialDisposalSTRING += ngMaterialDisposalARRAY[i]
		}
		fetch(`${process.env.REACT_APP_SQAR_API}/api/sqar/add`,{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				SQAR_Control_Number: sqarControlNo,
                SQAR_PO_Number: poNumber,
                SQAR_Invoice_DR: invoice,
                SQAR_Invoice_Qty: invoiceQty,
                SQAR_Supplier_Name: supplier,
                SQAR_No_Issued: noOfIssuedSqar,
                SQAR_Maker_Name: maker,
                SQAR_Response_Month: withSqarResponse,
                SQAR_Received_Date: dateReceived,
                SQAR_Contact_Person: contactPerson, 
                SQAR_Part_Name: partName,
                SQAR_Part_Number: partNo,
                SQAR_Model: applicableModel,
                SQAR_Rejected_Qty: parseInt(totalRejectQty),
                SQAR_Deffects: defectsArrStr,
                SQAR_Rejected_Rate: document.getElementById('rejectRate').value,
                SQAR_Remarks: remarks,
                SQAR_Affected_Qty: parseInt(affectedQty),
                SQAR_Affected_Lot_No: affectedLotNos,
                SQAR_Trouble_Classification: troubleClassificationSTRING,
                SQAR_Prepared_By: localStorage.getItem('fullName'),
                SQAR_Defect_Rank: defectRank,
                SQAR_Affected_Lot: affectedLotSTRING,
                SQAR_Status: 1,
                SQAR_Defective_Mat: defectiveMaterialSTRING,
                SQAR_NG_MAT_DispOSAL: ngMaterialDisposalSTRING,
                SQAR_Sample_Size: parseInt(sampleSize),
                SQAR_Input_QTY: parseInt(inputQuantity),
                SQAR_NG_Amount: parseFloat(ngAmount),
                SQAR_Deffect_Occurence: deffectOccurence,
                SQAR_Defects_Qty: defectsQtyArrStr,
				SQAR_Include_Supp_Tech_Mngr: isIncludeTechSuppMngrSuppier,
				SQAR_Affected_Lot_IsRTV: affectedLotIsRTV,
				SQAR_Affected_Lot_IsOkToUse: affectedLotIsOkToUse,
				SQAR_MatCodeBoxSeqId: matCodeBoxSeqId,
				SQAR_Defect_Info_No: defectInfoNo
			})
		})
		.then((response) => response.json())
		.then( async (result) => {
			if(result){
				//SAVE DEFECTS TO SQAR DEFECTS TABLE
				sqarDefects.map(async item => {
					await fetch(`${process.env.REACT_APP_SQAR_API}/api/sqar/save-defects`, {
						method: 'POST',
						headers: {'Content-Type': 'application/json'},
						body: JSON.stringify({
							sqarControlNo: sqarControlNo,
							defect: item.defect,
							quantity: item.quantity
						})
					})
					.then(response => response.json())
					.then(result => console.log(result))
				})

				//SAVE ILLUSTRATIONS
				let success = true;
				for(let i = 0; i < illustrations.length; i++){
					const formData = new FormData();
					formData.append('image', illustrations[i].image)
					await fetch(`${process.env.REACT_APP_SQAR_API}/api/sqar/save-illustrations`, {
						method: 'POST',
						headers: {
							'Accept': 'application/json'
						},
						body: formData
					})
					.then((response) => response.json())
					.then((result) => {
						console.log(result)				
						if(result.image){
							fetch(`${process.env.REACT_APP_SQAR_API}/api/sqar/update-image-access-key-info`,{
								method: 'POST',
								headers: {
									'Content-Type': 'application/json'
								},
								body: JSON.stringify({
									imageId: result.id,
									imageAccessKey: sqarControlNo + '-illustrations',
									title: illustrations[i].title,
									description: illustrations[i].description
								})
							})
							.then((response) => response.json())
							.then((result) => {
								if(result != 1){
									success = false;
								}
							})
						}else{
							success = false;
						}
					})
				}
				if(success){
					Swal.fire({
						title: 'Success',
						icon: 'success',
						confirmButtonColor: "#3C5393",
						text: "SQAR Generated Successfully!"
					})	
				}else{
					Swal.fire({
						title: 'Uploading Photo Error',
						icon: 'error',
						confirmButtonColor: "#3C5393",
						text: "Something went wrong!"
					})
				}											
			}else{
				Swal.fire({
					title: 'Error',
					icon: 'error',
					confirmButtonColor: "#3C5393",
					text: "Something went wrong!"
				})	
			}
			setIsUploading(false)
			setSqarWaiter(!sqarWaiter)
			setForProcessWaiter(!forProcessWaiter)
			setShowSqarForm(false)
			setCurrentSelectedImage(null)
			setAddIllustration(false)
			setIllustrations([])
			setIllustrationScript([])
			setImageTitle('')								
		})
	}

	const checkInputtedDefect = () => {
		//ID'S OF THE TEXTBOXES AND TEXT AREAS
		let defectsContainerArr = ['defectOne', 'defectTwo', 'defectThree', 'defectFour', 'defectFive', 'defectSix', 'defectSeven', 'defectEight', 'defectNine']
		let defectsQtyContainerArr = ['rejectQty1', 'rejectQty2', 'rejectQty3', 'rejectQty4', 'rejectQty5', 'rejectQty6', 'rejectQty7', 'rejectQty8', 'rejectQty9']
		let majorDefectDetected = false
		
		for(let i = 0; i < defectsContainerArr.length; i++){
			let enteredDefect = document.getElementById(defectsContainerArr[i]).value
			let enteredDefectQty = document.getElementById(defectsQtyContainerArr[i]).value
			
			if(materialType == "METAL"){
				majorDefects.METAL_MAJOR_DEFECTS.map(defect => {
					if(enteredDefect && enteredDefectQty > 0){
						if(enteredDefect.toLowerCase().search(defect.toLowerCase()) > -1){
							majorDefectDetected = true
							console.log("DETECTED METAL MAJOR DEFECT ==>> " + enteredDefect)
						}
					}
				})
			}

			if(materialType == "PLASTIC"){
				majorDefects.PLASTIC_MAJOR_DEFECTS.map(defect => {
					if(enteredDefect && enteredDefectQty > 0){
						if(enteredDefect.toLowerCase().search(defect.toLowerCase()) > -1){
							majorDefectDetected = true
							console.log("DETECTED PLASTIC MAJOR DEFECT ==>> " + enteredDefect)
						}
					}
				})
			}

			if(materialType == "ELECTRICAL"){
				majorDefects.PCB_MAJOR_DEFECTS.map(defect => {
					if(enteredDefect && enteredDefectQty > 0){
						if(enteredDefect.toLowerCase().search(defect.toLowerCase()) > -1){
							majorDefectDetected = true
							console.log("DETECTED ELECTRICAL MAJOR DEFECT ==>> " + enteredDefect)
						}
					}
				})
			}

		}

		setIsdefectMajor(majorDefectDetected)
	}

	const calculateRejectRate = () => {
		checkInputtedDefect()

		setRejectQty(parseInt(document.getElementById('rejectQty1').value))
		document.getElementById('sampleSize').value == '' ? setSampleSize(0) : setSampleSize(parseInt(document.getElementById('sampleSize').value))
		document.getElementById('inputQuantity').value == '' ? setInputQty(0) : setInputQty(parseInt(document.getElementById('inputQuantity').value))
		let rejectQtys = []
		let rejectQtyTOTAL = 0

		if(document.getElementById('rejectQty1').value != ""){
			rejectQtys.push(parseInt(document.getElementById('rejectQty1').value))
		}
		if(document.getElementById('rejectQty2').value != ""){
			rejectQtys.push(parseInt(document.getElementById('rejectQty2').value))
		}
		if(document.getElementById('rejectQty3').value != ""){
			rejectQtys.push(parseInt(document.getElementById('rejectQty3').value))
		}
		if(document.getElementById('rejectQty4').value != ""){
			rejectQtys.push(parseInt(document.getElementById('rejectQty4').value))
		}
		if(document.getElementById('rejectQty5').value != ""){
			rejectQtys.push(parseInt(document.getElementById('rejectQty5').value))
		}
		if(document.getElementById('rejectQty6').value != ""){
			rejectQtys.push(parseInt(document.getElementById('rejectQty6').value))
		}
		if(document.getElementById('rejectQty7').value != ""){
			rejectQtys.push(parseInt(document.getElementById('rejectQty7').value))
		}
		if(document.getElementById('rejectQty8').value != ""){
			rejectQtys.push(parseInt(document.getElementById('rejectQty8').value))
		}
		if(document.getElementById('rejectQty9').value != ""){
			rejectQtys.push(parseInt(document.getElementById('rejectQty9').value))
		}
		for(let i = 0; i < rejectQtys.length; i++){
			rejectQtyTOTAL += rejectQtys[i]
		}
		setTotalRejectQty(rejectQtyTOTAL)
	}

	const refresh = () => {
		setForProcess([])
		setCurrentPage(1)
		setForProcessWaiter(!forProcessWaiter)
	}

	const searchForSQARProcess = (event) => {
		event.preventDefault()
		setSearchKeyword(event.target.value)
		constructUI(true)
	}

	const nextPage = () => {
		setCurrentPage(currentPage + 1)
	}

	const prevPage = () => {
		setCurrentPage(currentPage - 1)
	}

	const constructUI = (isSearch) => {
		let searchedItems = [];
		let forSqarData2 = forSqarData
		let newSearchKeyword = document.getElementById('searchBox').value

		if(isSearch && newSearchKeyword != ""){		
			for (let i = 0; i < forSqarData.length; i++){
				if((forSqarData[i].INVOICE || '').toLowerCase().includes(newSearchKeyword.toLowerCase()) 
					|| (forSqarData[i].PART_NO || '').toLowerCase().includes(newSearchKeyword.toLowerCase())
					|| (forSqarData[i].DATE_RECEIVED || '').toLowerCase().includes(newSearchKeyword.toLowerCase())
					|| (forSqarData[i].DATE_INSPECTED || '').toLowerCase().includes(newSearchKeyword.toLowerCase())
					|| (forSqarData[i].PART_NAME || '').toLowerCase().includes(newSearchKeyword.toLowerCase())
					|| (forSqarData[i].SUPPLIER || '').toLowerCase().includes(newSearchKeyword.toLowerCase())
					|| (forSqarData[i].MAKER || '').toLowerCase().includes(newSearchKeyword.toLowerCase())
					|| (forSqarData[i].MATERIAL_TYPE || '').toLowerCase().includes(newSearchKeyword.toLowerCase())
					|| (forSqarData[i].MATCODEBOX || '').toLowerCase().includes(newSearchKeyword.toLowerCase())
					|| (forSqarData[i].PO_NUMBER || '').toLowerCase().includes(newSearchKeyword.toLowerCase())){
					searchedItems.push(forSqarData[i])
				}
			}
			forSqarData2 = searchedItems
		}

		currentPageItems = []
		//SET 100 ITEMS ON TABLE
		if(forSqarData2){
			//If the current page reach the last page that does not have 100 items
			if(currentPage * 100 > forSqarData2.length){
				for(let i = currentPage * 100 - 100; i < (currentPage * 100) - (currentPage * 100 - forSqarData2.length) ; i++){
					if(i == (currentPage * 100) - (currentPage * 100 - forSqarData2.length)){
						break;
					}
					currentPageItems.push(forSqarData2[i])
				}
			}else{
				for(let i = currentPage * 100 - 100; i < currentPage * 100 ; i++){
					if(i == currentPage * 100){
						break;
					}
					currentPageItems.push(forSqarData2[i])
				}
			}			
		}
		// setFinalSqarDataPageLength(currentPageItems.length)
		setFinalSqarDataPageLength(forSqarData)
		let rowNo = 0;
		setForProcess(									
			currentPageItems.map(item => {
				rowNo++
				return(
					(viewTable) ?
						<tr key={item.ROW_NUMBER}>
							<td>{item.ROW_NUMBER}</td>
							<td style={{textAlign: 'center'}}>
								{
									item.SQAR_CONTROL_NO ?
										<Badge bg="success">
											Generated <br/>
											{item.SQAR_CONTROL_NO}
										</Badge>
									:
										<Button
										className="process-sqar-button"
										disabled={localStorage.getItem('position') != 'QC Leadgirl'}
										onClick={() => getData(item)}>
											<i className="fa-solid fa-marker"></i>
										</Button>
								}							    								
							</td>
							<td><b>{item.INVOICE}</b></td>
							<td>{(item.DATE_INSPECTED) ? convertDate(item.DATE_INSPECTED.slice(0, 10)) : item.DATE_INSPECTED}</td>							
							<td>{(item.DATE_RECEIVED) ? convertDate(item.DATE_RECEIVED.slice(0, 10)) : item.DATE_RECEIVED}</td>							
							<td>{item.PART_NO}</td>
							<td>{item.PART_NAME}</td>
							<td>{item.INVOICE_QTY}</td>
							<td>{item.AFFECTED_QTY}</td>
							<td>{item.SAMPLE_SIZE}</td>							
							<td>{item.DEFECT_QTY}</td>							
							<td>{item.REJECT_RATE}</td>							
							<td>{item.SUPPLIER}</td>
							<td>{item.MAKER}</td>
							<td>{item.MATERIAL_TYPE}</td>
							<td>{item.OVERALL_JUDGEMENT}</td>
							<td>{item.PRODUCTION_STATUS}</td>
							<td>{item.MATCODEBOX}</td>
						</tr>
					:
						<Col xs={12} sm={6} md={4} lg={4} xl={3} className="mt-3 rounded" key={item.ROW_NUMBER}>
							<Card className="productCard h-100 border shadow-sm">
							  <Card.Header className="py-2">
							  </Card.Header>
						      <Card.Body>
							  	<div><b>Date Inspected:</b> {(item.DATE_INSPECTED) ? convertDate(item.DATE_INSPECTED.slice(0, 10)) : item.DATE_INSPECTED}</div>
						      	<div><b>Date Received:</b> {(item.DATE_RECEIVED) ? convertDate(item.DATE_RECEIVED.slice(0, 10)) : item.DATE_RECEIVED}</div>					      					      
						      	<div><b>Invoice No: &nbsp; {item.INVOICE}</b></div>							      					      
						      	<div><b>Part No:</b> &nbsp; {item.PART_NO}</div>							      					      
						      	<div><b>Part Name:</b> &nbsp; {item.PART_NAME}</div>
						      	<div><b>Invoice Qty:</b> &nbsp; {item.INVOICE_QTY}</div>							      					      
						      	<div><b>Affected Qty:</b> &nbsp; {item.AFFECTED_QTY}</div>							      					      
						      	<div><b>Sample Size:</b> &nbsp; {item.SAMPLE_SIZE}</div>     
						      	<div><b>Defect Qty:</b> &nbsp; {item.DEFECT_QTY}</div>     
						      	<div><b>Reject Rate:</b> &nbsp; {item.REJECT_RATE}</div>     
						      	<div><b>Supplier:</b> &nbsp; {item.SUPPLIER}</div>							      					      
						      	<div><b>Maker:</b> &nbsp; {item.MAKER}</div>							      					      
						      	<div><b>Material Type:</b> &nbsp; {item.MATERIAL_TYPE}</div>
						      	<div><b>Overall Judgement:</b> &nbsp; {item.OVERALL_JUDGEMENT}</div>
						      	<div><b>Production Status:</b> &nbsp; {item.PRODUCTION_STATUS}</div>
						      	<div><b>MATCodeBox</b> &nbsp; {item.MATCODEBOX}</div>			        
						      </Card.Body>
						      <Card.Footer className="card-footer py-2">
						      	{
						      	 localStorage.getItem('position') == 'QC Leadgirl' ?			      	
						      	 	<Button className="process-sqar-button" onClick={() => getData(item)}><i className="fa-solid fa-marker"></i> PROCESS SQAR</Button>
						      	 :
						      	 	<Button className="process-sqar-button" disabled><i className="fa-solid fa-marker"></i> PROCESS SQAR</Button>
						      	}
						      </Card.Footer>
						    </Card>
						</Col>
				)
			})
		)
		setIsLoading(false)		
	}

	useEffect(() => {
		setTimeout(() => {
			constructUI(false)
		})	
	}, [forSqarData, currentPage, viewTable])

	const handlePartNameChange = (selectedOption) => {
		setPartName(selectedOption.value)
	}

	return(
		<>
			{
				isUploading ?
					<UploadSpinner/>
				:
					<></>
			}
			<Modal
				show={showSqarForm}
		        onHide={()=>handleClose()}
		        size="xl"
		        aria-labelledby="example-custom-modal-styling-title"
		      >
		        <Modal.Header closeButton>
		        </Modal.Header>
		        <Modal.Body>
		        	<Table bordered>
		        		<tbody>
		        			<tr>
		        				<td colSpan="6">
									<img src="/malogo.png" width="100" height="100" className="img-fluid"/>
									<span style={{position: 'absolute', top: 71, marginLeft: 10}}>QR-QA-121004R14</span>
		        					<img src={assuredlogo} width="100" height="100" className="img-fluid align-right"/>
		        				</td>
		        			</tr>
		        			<tr>
		        				<td colSpan="4" style={{backgroundColor: 'rgb(153, 102, 255)', fontSize: 25}}><center><b className="text-white">SUPPLIER QUALITY ABNORMALITY REPORT (SQAR)</b></center></td>		        				
		        				<td style={{backgroundColor: 'rgb(248, 203, 173)', textAlign: 'right', verticalAlign: 'middle'}}><b>CONTROL NO.</b></td>
		        				<td className="ma-disabled-backcolor"
		        					style={{verticalAlign: 'middle', textAlign: 'center'}}>        					
		        					{sqarControlNo}		        				
		        				</td>      					        				
		        			</tr>
		        		</tbody>
		        	</Table>

		        	<Table bordered>
		        		<tbody>		        			
		        			<tr>
		        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>SUPPLIER NAME</b></td>
		        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>No. of Issued<br/>SQAR (Month)</b></td>		        				
		        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>PART NAME</b></td>
		        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>PART NUMBER</b></td>
		        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>DATE SENT</b></td>
		        			</tr>
		        			<tr>
		        				<td className="ma-disabled-backcolor">
		        					<center>{supplier}</center>
		        				</td>
		        				<td style={{verticalAlign: 'middle'}}>
			        				<input type="number"
			        						id="noIssuedSqarMonth"
			        						placeholder="Click to add" 
			        						style={{textAlign: 'center', width: '100%', height: '100%', border: 'none'}}
			        				/>
		        				</td>
		        				<td className="">
									{partName ? 
										<center>{partName}</center>
									:
										<Select
										className="basic-single"
										classNamePrefix="select"
										options={partNameOptions}
										isLoading={isGettingPartNameOptions}
										onChange={handlePartNameChange}
										/>
									}
		        				</td>
		        				<td className="ma-disabled-backcolor">
		        					<center>{partNo}</center>
		        				</td>
		        				<td className="ma-disabled-backcolor">
			        				{/*<input 
			        					type="date"
			        					id="dateSent"
			        					style={{textAlign: 'center', width: '100%', height: '100%', border: 'none'}}
			        				/>*/}
		        				</td>
		        			</tr>
		        			<tr>
		        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>MAKER NAME</b></td>
		        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>With SQAR<br/>Response (Month)</b></td>
		        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>AFFECTED QUANTITY</b></td>
		        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>APPLICABLE MODEL</b></td>
		        				<td style={{backgroundColor: 'rgb(248, 203, 173)', textAlign: 'center', verticalAlign: 'middle'}}><b>RESPONSE DUE DATE <br/> (MATERIAL DISPOSITION)</b></td>
		        			</tr>
		        			<tr>
		        				<td className="ma-disabled-backcolor"><center>{maker}</center></td>
		        				<td style={{verticalAlign: 'middle'}}>
			        				<input 
			        					type="number"
			        					id="withSqarResponseMonth"
			        					placeholder="Click to add" 
			        					style={{textAlign: 'center', width: '100%', height: '100%', border: 'none'}}
			        				/>
		        				</td>
		        				<td className="ma-disabled-backcolor"><center>{affectedQty}</center></td>
		        				<td style={{verticalAlign: 'middle'}}>
			        				<input 
			        					type="text"
			        					id="applicableModel"
			        					placeholder="Click to add" 
			        					style={{textAlign: 'center', width: '100%', height: '100%', border: 'none'}}
			        				/>
		        				</td>
		        				<td className="ma-disabled-backcolor">
	        						{/*<input 
	        							type="date"
	        							id="responseDueDate"
	        							style={{textAlign: 'center', width: '100%', height: '100%', border: 'none'}}
	        						/>*/}
		        				</td>
		        			</tr>
		        			<tr>
		        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>ATTENTION TO</b></td>
		        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>DEFECT <br/>OCCURRENCE</b></td>
		        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>INPUT QUANTITY</b></td>
		        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>DATE RECEIVED</b></td>
		        				<td style={{backgroundColor: 'rgb(248, 203, 173)', textAlign: 'center', verticalAlign: 'middle'}}><b>RESPONSE DUE DATE<br/>(RCCA)</b></td>
		        			</tr>
		        			<tr>
		        				<td>
		        					<input 
		        						type="text"
		        						id="attentionTo"
		        						placeholder="Click to add" 
		        						style={{textAlign: 'center', width: '100%', height: '100%', border: 'none'}}
		        					/>
		        				</td>
		        				<td>
		        					<input 
		        						type="text"
		        						id="deffectOccurence"
		        						placeholder="Click to add" 
		        						style={{textAlign: 'center', width: '100%', height: '100%', border: 'none'}}
		        					/>
		        				</td>
		        				<td>
		        					<input 
		        						type="number" 
		        						id="inputQuantity"
		        						placeholder="Click to add"
		        						style={{textAlign: 'center', width: '100%', height: '100%', border: 'none'}}
										onChange={event => calculateRejectRate()}
		        					/>
		        				</td>
		        				<td className="ma-disabled-backcolor"><center>{(dateReceived) ? convertDate(dateReceived) : dateReceived}</center></td>
		        				<td className="ma-disabled-backcolor">
	        						{/*<input 
	        							type="date" 
	        							id="responseDueDateRCAA" 
	        							style={{textAlign: 'center', width: '100%', height: '100%', border: 'none'}}
	        						/>*/}
		        				</td>
		        			</tr>
		        			<tr>
		        				<td style={{backgroundColor: 'rgb(248, 203, 173)', textAlign: 'center'}}><b>DEFECT(S)</b></td>
		        				<td style={{backgroundColor: 'rgb(248, 203, 173)', textAlign: 'center'}}><b>REJECT QTY</b></td>
		        				<td style={{backgroundColor: 'rgb(248, 203, 173)', textAlign: 'center'}}><b>SAMPLE SIZE</b></td>
		        				<td style={{backgroundColor: 'rgb(248, 203, 173)', textAlign: 'center'}}><b>INVOICE / D.R.</b></td>
		        				<td style={{backgroundColor: 'rgb(248, 203, 173)', textAlign: 'center'}}><b>LOT NO(S)</b></td>
		        			</tr>
		        			<tr>
		        				<td className="text-center">
		        					<textarea
		        						className="text-center"
		        						rows="1"
		        						id="defectOne"
		        						// defaultValue={defects1}
		        						defaultValue={defects.length > 0 ? defects[0].hasOwnProperty('defect') ? defects[0].defect : "" : ""}
		        						style={{verticalAlign: 'middle', width: '100%', height: '100%', border: 'none'}}
										onChange={() => checkInputtedDefect()}
									>
		        					</textarea>		        					
		        				</td>
		        				<td className="text-center" style={{verticalAlign: 'middle'}}>
		        					<input 
	        							id="rejectQty1"
	        							type="number"
	        							defaultValue={defects.length > 0 ? defects[0].hasOwnProperty('defectQty') ? defects[0].defectQty : "" : ""}
	        							style={{textAlign: 'center', width: '100%', height: '100%', border: 'none'}}
	        							onChange={event => calculateRejectRate()}
		        					/>
		        				</td>
		        				<td className="" style={{textAlign: 'center', verticalAlign: 'middle'}}>
		        					<input 
	        							id="sampleSize"
										type="number"
	        							placeholder="Click to add"
	        							value={sampleSize}
	        							style={{textAlign: 'center', width: '100%', height: '100%', border: 'none'}}
	        							onChange={event => calculateRejectRate()}
		        					/>
		        				</td>
		        				<td className="ma-disabled-backcolor" style={{textAlign: 'center', verticalAlign: 'middle'}}>
		        					{invoice}
		        				</td>
		        				<td className="" rowSpan="3">
									<TextareaAutosize
										minRows={5}
		        						className="text-center"
		        						id="lotNos"
		        						style={{width: '100%', textAlign: 'center', verticalAlign: 'middle', border: 'none'}}
		        						rows="1"
		        						defaultValue={lotNo}
		        					>
		        					</TextareaAutosize>
								</td>
		        			</tr>
		        			<tr>
		        				<td className="text-center">
		        					<textarea
		        						className="text-center"
		        						rows="1"
		        						id="defectTwo"
		        						defaultValue={defects.length > 1 ? defects[1].hasOwnProperty('defect') ? defects[1].defect : "" : ""}
		        						style={{verticalAlign: 'middle', width: '100%', height: '100%', border: 'none'}}
										onChange={() => checkInputtedDefect()}
									>
		        					</textarea>
		        				</td>
		        				<td style={{verticalAlign: 'middle'}}>
		        					<input 
		        						type="number"
		        						id="rejectQty2"
		        						defaultValue={defects.length > 1 ? defects[1].hasOwnProperty('defectQty') ? defects[1].defectQty : "" : ""}
		        						style={{textAlign: 'center', verticalAlign: 'middle', width: '100%', height: '100%', border: 'none'}}
		        						onChange={event => calculateRejectRate()}
		        					/>
		        				</td>
		        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>REJECT RATE (%)</b></td>
		        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>D.I. NO.</b></td>
		        			</tr>
		        			<tr>
		        				<td>
		        					<textarea
		        						className="text-center"
		        						rows="1"
		        						id="defectThree"
		        						// defaultValue={defects3}
		        						defaultValue={defects.length > 2 ? defects[2].hasOwnProperty('defect') ? defects[2].defect : "" : ""}
		        						style={{width: '100%', height: '100%', border: 'none'}}
										onChange={() => checkInputtedDefect()}
									>
		        					</textarea>
		        				</td>
		        				<td style={{verticalAlign: 'middle'}}>
		        					<input
		        						type="number"
		        						id="rejectQty3"
		        						// defaultValue={(rejectQty3 > 0) ? rejectQty3 : ''}
		        						defaultValue={defects.length > 2 ? defects[2].hasOwnProperty('defectQty') ? defects[2].defectQty : "" : ""}
		        						style={{textAlign: 'center', verticalAlign: 'middle', width: '100%', height: '100%', border: 'none'}}
		        						onChange={event => calculateRejectRate()}
		        					/>
		        				</td>
		        				<td style={{textAlign: 'center', verticalAlign: 'middle'}}>
		        					<input 
		        					 type="number"
		        					 id="rejectRate"
		        					 style={{textAlign: 'center', verticalAlign: 'middle', width: '100%', height: '100%', border: 'none', fontWeight: 'Bold'}}
		        					 value={(totalRejectQty/(parseInt(sampleSize) + parseInt(inputQty)) * 100).toFixed(2)}
		        					 onChange={event => setRecjectionRate(event.target.value)}
		        					/>
		        				</td>
		        				<td style={{textAlign: 'center', verticalAlign: 'middle'}}>
		        					<input
									type="text"
									id="defectInfoNo"
									placeholder="click to add"
									style={{textAlign: 'center', width: '100%', height: '100%', border: 'none'}}
									value={defectInfoNo}
									onChange={e => setDefectInfoNo(e.target.value)}
									/>
		        				</td>
		        			</tr>
		        			<tr>
		        				<td>
		        					<textarea
		        						rows="1"
		        						id="defectFour"
		        						defaultValue={defects.length > 3 ? defects[3].hasOwnProperty('defect') ? defects[3].defect : "" : ""}
		        						style={{width: '100%', height: '100%', border: 'none', textAlign: 'center', verticalAlign: 'middle'}}
										onChange={() => checkInputtedDefect()}
									>
		        					</textarea>
		        				</td>
		        				<td>
		        					<input
		        						type="number"
		        						id="rejectQty4"
		        						defaultValue={defects.length > 3 ? defects[3].hasOwnProperty('defectQty') ? defects[3].defectQty : "" : ""}
		        						style={{textAlign: 'center', verticalAlign: 'middle', width: '100%', height: '100%', border: 'none'}}
		        						onChange={event => calculateRejectRate()}
		        					/>
		        				</td>
		        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}} ><b>DEFECT RANK</b></td>
		        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>RANKING CRITERIA</b></td>		
		        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>TROUBLE CLASSIFICATION</b></td>		
		        			</tr>
		        			<tr>
		        				<td>
		        					<textarea 
		        						rows="1"
		        						id="defectFive"
		        						defaultValue={defects.length > 4 ? defects[4].hasOwnProperty('defect') ? defects[4].defect : "" : ""}
		        						style={{width: '100%', height: '100%', border: 'none', textAlign: 'center', verticalAlign: 'middle'}}
										onChange={() => checkInputtedDefect()}
									>
		        					</textarea>
		        				</td>
		        				<td>
		        					<input
		        						type="number"
		        						id="rejectQty5"
		        						defaultValue={defects.length > 4 ? defects[4].hasOwnProperty('defectQty') ? defects[4].defectQty : "" : ""}
		        						style={{textAlign: 'center', verticalAlign: 'middle', width: '100%', height: '100%', border: 'none'}}
		        						onChange={event => calculateRejectRate()}
		        					/>
		        				</td>
		        				<td className="ma-disabled-backcolor text-center" id="defectRank" style={{fontSize: 20, fontWeight: 'bold', color: 'blue'}}>
		        					{
		        						(((parseInt(totalRejectQty)/parseInt(sampleSize) * 100).toFixed(2) >= 2 && (isDefectMajor)) 
											|| isDefectAffectFunctional 
											|| isCriticalNGCheckPoint
											|| isIncludeDimensionalDefect
											|| functionalTrblClass) ?
		        							"A"
		        						:
		        							"B"
		        					}
		        				</td>
								<td rowSpan="3" style={{}}>
									<b>RANK A</b> - Major Defects,<br/>Not Following UL Requirements
								</td>
								<td>
									<Form.Check
							            type='checkbox'
							            // name='troubleClassification'
							            id='documentsLabelsCheckbox'
							            label='DOCUMENTS/LABELS'
							          />
								 </td>
		        			</tr>
		        			<tr>
		        				<td>
		        					<textarea 
		        						rows="1"
		        						id="defectSix"
		        						defaultValue={defects.length > 5 ? defects[5].hasOwnProperty('defect') ? defects[5].defect : "" : ""}
		        						style={{width: '100%', height: '100%', border: 'none', textAlign: 'center', verticalAlign: 'middle'}}
										onChange={() => checkInputtedDefect()}
									>
		        					</textarea>
		        				</td>
		        				<td>
		        					<input 
		        						type="number"
		        						id="rejectQty6"
		        						defaultValue={defects.length > 5 ? defects[5].hasOwnProperty('defectQty') ? defects[5].defectQty : "" : ""}
		        						style={{textAlign: 'center', verticalAlign: 'middle', width: '100%', height: '100%', border: 'none'}}
		        						onChange={event => calculateRejectRate()}
		        					/>
		        					</td>
		        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>REMARKS</b></td>
		        				<td>
									<Form.Check 
							            type='checkbox'
							            // name='troubleClassification'
							            id='physicalPropertiesCheckbox'
							            label='PHYSICAL PROPERTIES'
							          />
								 </td>
		        			</tr>
		        			<tr>
		        				<td>
		        					<textarea
		        						rows="1"
		        						id="defectSeven"
		        						defaultValue={defects.length > 6 ? defects[6].hasOwnProperty('defect') ? defects[6].defect : "" : ""}
		        						style={{width: '100%', height: '100%', border: 'none', textAlign: 'center', verticalAlign: 'middle'}}
										onChange={() => checkInputtedDefect()}
									>
		        					</textarea>
		        				</td>
		        				<td>
		        					<input 
		        						type="number"
		        						id="rejectQty7"
		        						defaultValue={defects.length > 6 ? defects[6].hasOwnProperty('defectQty') ? defects[6].defectQty : "" : ""}
		        						style={{textAlign: 'center', verticalAlign: 'middle', width: '100%', height: '100%', border: 'none'}}
		        						onChange={event => calculateRejectRate()}
		        					/>
		        				</td>
		        				<td rowSpan="4">
		        					<textarea 
		        						rows="6"
		        						id="remarks"
		        						style={{whiteSpace: 'pre-wrap', width: '100%', height: '100%', border: 'none'}}
		        					>
		        					</textarea>
		        				</td>
		        				<td>
									<Form.Check 
							            type='checkbox'
							            // name='troubleClassification'
							            id='dimensionalCheckbox'
							            label='DIMENSIONAL'
							          />
								 </td>	        				
		        			</tr>
		        			<tr>
		        				<td>
		        					<textarea
		        						rows="1"
		        						id="defectEight"
		        						defaultValue={defects.length > 7 ? defects[7].hasOwnProperty('defect') ? defects[7].defect : "" : ""}
		        						style={{width: '100%', height: '100%', border: 'none', textAlign: 'center', verticalAlign: 'middle'}}
										onChange={() => checkInputtedDefect()}
		        					>	
		        					</textarea>
		        				</td>
		        				<td>
		        					<input
		        						type="number"
		        						id="rejectQty8"
		        						defaultValue={defects.length > 7 ? defects[7].hasOwnProperty('defectQty') ? defects[7].defectQty : "" : ""}
		        						style={{textAlign: 'center', verticalAlign: 'middle', width: '100%', height: '100%', border: 'none'}}
		        						onChange={event => calculateRejectRate()}
		        					/>
		        				</td>
		        				<td rowSpan="3" style={{}}>
		        					<b>RANK B</b> - Minor Defects,<br/> No Major effect in function
		        				</td>
		        				<td>
									<Form.Check 
							            type='checkbox'
							            id='functionalCheckbox'
							            label='FUNCTIONAL'
										onChange={(e) => setFunctionalTrblClass(e.target.checked)}
							          />
								 </td>
		        			</tr>
		        			<tr>
		        				<td>
		        					<textarea
		        						rows="1"
		        						id="defectNine"
		        						defaultValue={defects.length > 8 ? defects[8].hasOwnProperty('defect') ? defects[8].defect : "" : ""}
		        						style={{width: '100%', height: '100%', border: 'none', textAlign: 'center', verticalAlign: 'middle'}}
										onChange={() => checkInputtedDefect()}
		        					>
		        					</textarea>
		        				</td>
		        				<td>
		        					<input
		        						type="number" 
		        						id="rejectQty9"
		        						defaultValue={defects.length > 8 ? defects[8].hasOwnProperty('defectQty') ? defects[8].defectQty : "" : ""}
		        						style={{textAlign: 'center', verticalAlign: 'middle', width: '100%', height: '100%', border: 'none'}}
		        						onChange={event => calculateRejectRate()}
		        					/>
		        				</td>
		        				<td>
									<Form.Check 
							            type='checkbox'
							            // name='troubleClassification'
							            id='greenPurchasingCheckbox'
							            label='GREEN PURCHASING STD.'
							          />
								 </td>
		        			</tr>
		        			<tr>
		        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>TOTAL DEFECT QTY</b></td>
		        				<td style={{textAlign: 'center'}}>
		        					<input
		        						type="number"
		        						id="reject10"
		        						readOnly
		        						value={totalRejectQty}
		        						style={{textAlign: 'center', verticalAlign: 'middle', width: '100%', height: '100%', border: 'none'}}
		        					/>
		        				</td>
		        				<td></td>
		        			</tr>
		        			<tr>
		        				<td colSpan="5"></td>
		        			</tr>
		        			<tr>
		        				<td colSpan="5" style={{textAlign: 'center', backgroundColor: 'rgb(180, 198, 231)'}}><b>MAT MATERIAL DISPOSITION</b></td>
		        			</tr>
		        			<tr>
		        				<td colSpan="3" style={{backgroundColor: 'rgb(248, 203, 173)', fontStyle: 'italic', textAlign: 'center'}}><b>AFFECTED LOT / INVOICE NUMBER</b></td>
		        				<td style={{backgroundColor: 'rgb(248, 203, 173)', fontStyle: 'italic', textAlign: 'center'}}><b>DEFECTIVE MATERIAL</b></td>
		        				<td style={{backgroundColor: 'rgb(248, 203, 173)', fontStyle: 'italic', textAlign: 'center'}}><b>NG MATERIAL DISPOSAL</b></td>
		        			</tr>
		        			<tr>
		        				<td>
		        					<Form.Check 
							            type='radio'
							            // name='affectedLotRTV'
										name='okToUseORRTV'
							            id='rtvCheckbox'
							            label='RTV'
							          />
		        				</td>
		        				<td colSpan="2">
		        					<Form.Check 
							            type='radio'
							            name='affectedLot'
							            id='supplierSortingCheckBox'
							            label='SUPPLIER SORTING / REWORK'
							          />
		        				</td>
		        				<td>
		        					<Form.Check 
							            type='radio'
							            name='deffectiveMaterial'
							            id='replacementCheckbox'
							            label='REPLACEMENT'
							            onChange={()=>{
							            	setRefundDebitChecked(true)
							            	document.getElementById('ngAmount').value = ""
							            }}
							          />
		        				</td>
		        				<td>
		        					<Form.Check 
							            type='radio'
							            name='ngMaterialDisposal'
							            id='matDisposalCheckbox'
							            label='MAT DISPOSAL FEE BY SUPPLIER'
							          />
		        				</td>
		        			</tr>
		        			<tr>
		        				<td>
		        					<Form.Check 
							            type='radio'
							            // name='affectedLotOkToUse'
										name='okToUseORRTV'
							            id='okToUseCheckbox'
							            label='OK TO USE'
							          />
		        				</td>
		        				<td colSpan="2">
		        					<Form.Check 
							            type='radio'
							            name='affectedLot'
							            id='matSortingCheckbox'
							            label='MAT SORTING / REWORK (WITH CHARGE SHEET)'
							          />
		        				</td>
		        				<td>
		        					<Form.Check 
							            type='radio'
							            name='deffectiveMaterial'
							            id='refundDebitCheckbox'
							            label='REFUND / DEBIT'
							            onChange={()=>{
							            	setRefundDebitChecked(false)
							            }}
							          />
		        				</td>
		        				<td>
		        					<Form.Check 
							            type='radio'
							            name='ngMaterialDisposal'
							            id='ngMatRTVCheckbox'
							            label='RTV'
							          />
		        				</td>
		        			</tr>
		        			<tr>
		        				<td></td>
		        				<td colSpan="2"></td>
		        				<td>NG AMOUNT: $&nbsp;
		        					<input
		        						type="number"
		        						disabled={refundDebitChecked}
		        						id="ngAmount"
		        						style={{textAlign: 'center', verticalAlign: 'middle', width: '50%', height: '100%', border: 'none'}}
		        					/>
		        				</td>
		        				<td></td>
		        			</tr>		        			
		        		</tbody>
		        	</Table>

		        	<Table bordered>
		        		<tbody>
		        			<tr>
		        				<td style={{fontWeight: 'bold', textAlign: 'center', backgroundColor: '#FFFF65'}}
		        					colSpan='2'
		        				>ILLUSTRATIONS</td>
		        			</tr>
		        			{illustrationScript}
		        			{
		        				addIllustration ?
		        					addIllustrationScript()
		        				:
		        					<tr>
		        						<td colSpan='2'>
		        							<center>		        							
				        					<Button				        					
				        						className='ma-blue-button my-2'
				        						onClick={() => setAddIllustration(true)}
				        					>
				        						<i className="fa-solid fa-image"></i> ADD ILLUSTRATION
				        					</Button>
				        					</center>
		        						</td>
		        					</tr>
		        			}      			
		        		</tbody>
		        	</Table>

		        	<Table>
		        		<tbody>
		        			<tr bordered="false">
		        				<td>
		        					{
		        						!addIllustration ?
		        							<Button 
				        						className="align-right ma-blue-button"
				        						onClick={() => generateSqar()}
				        					>
				        						<i className="fa-solid fa-file-circle-plus"></i> GENERATE SQAR
				        					</Button>
				        				:
				        					<Button 
				        						className="align-right ma-blue-button"
				        						disabled
				        					>
				        						<i className="fa-solid fa-file-circle-plus"></i> GENERATE SQAR
				        					</Button>
		        					}
		        					
		        					<div className="align-right">&nbsp;</div>
				        			<Button className="align-right ma-gray-button"
				        					onClick={()=>handleClose()}
				        			>
				        				<i className="fa-solid fa-xmark"></i> CANCEL
				        			</Button>
				        		</td>
		        			</tr>
		        		</tbody>
		        	</Table>

		        </Modal.Body>
		    </Modal>

			<Card className="shadow m-3 mt-4" style={{height: 'auto'}}>
				<Card.Body>
					<div className="row mx-2 pb-2">
						<div className="col-12 col-lg-4">
							<ButtonGroup>
								<Button className="selected-tab-button">For SQAR Process</Button>
								<Button className="ma-blue-button" onClick={() => viewSqar()}>Generated SQAR</Button>
							</ButtonGroup>
						</div>
						<div className="col">
							<div className="d-flex justify-content-end">
								<Button className="ma-blue-button" onClick={() => refresh()}><i className="fa-solid fa-arrow-rotate-right"></i> Refresh</Button> &nbsp;
								<Dropdown>
									<Dropdown.Toggle className="view-dropdown" id="dropdown-basic" style={{ width: 90 }}>
										{
											(viewTable) ?
												<>Table </>
												:
												<>Cards </>
										}
									</Dropdown.Toggle>
									<Dropdown.Menu>
										{
											(viewTable) ?
												<>
													<Dropdown.Item onClick={() => {
														setForProcess([])
														setViewTable(false)
													}}>
														Cards
													</Dropdown.Item>
												</>
												:
												<>
													<Dropdown.Item onClick={() => {
														setForProcess([])
														setViewTable(true)
													}}>
														Table
													</Dropdown.Item>
												</>
										}
									</Dropdown.Menu>
								</Dropdown>
							</div>
						</div>
						<div className="col-12 col-md-6 col-lg-4">
							<InputGroup>
								<Form.Control
									type="search"
									placeholder="Search"
									id="searchBox"
									aria-label="search"
									aria-describedby="basic-addon2"
									value={searchKeyword}
									onChange={event => searchForSQARProcess(event)}
									autoComplete="off"
								/>
								<InputGroup.Text id="basic-addon2" className="ma-blue-background ma-white-font"><i className="fa-solid fa-magnifying-glass"></i></InputGroup.Text>
							</InputGroup>
						</div>
					</div>

				    <div className="table-container" style={{height: '65vh'}}>
						{
						(viewTable) ?				
							<Table bordered hover style={{whiteSpace: 'nowrap', fontSize: 14}}>
								<tbody>								
									<tr style={{position: 'sticky', top: 0, zIndex: 1}}>
										<th>#</th>
										<th>Action</th>
										<th>Invoice</th>
										<th>Date Inspected</th>
										<th>Date Received</th>					
										<th>Part No</th>
										<th>Part Name</th>
										<th>Invoice Qty</th>
										<th>Affected Qty</th>
										<th>Sample Size</th>
										<th>Defect Qty</th>
										<th>Reject Rate</th>
										<th>Supplier</th>
										<th>Maker</th>
										<th>Material Type</th>
										<th>Overall Judgement</th>
										<th>Production Status</th>
										<th>MATCodeMax</th>
									</tr>
									{forProcess}																	
								</tbody>
							</Table>
						:
							<>
								<Row>
									{forProcess}
								</Row>
							</>
						}
						{
							(forProcess.length > 0) ?
								<>
								</>
							:
								<Container className="mt-2">
									<Loading/>
								</Container>																					
						}
					</div>
					<div className="row" style={{margin: 0, userSelect: 'none'}}>
						<div className="col">
							{
								(finalSqarDataPageLength < 1) ?
									<><p style={{marginLeft: 15, fontSize: 14}}>Loading...</p></>
								:
									// (currentPage * 100 > finalSqarDataPageLength) ?
									(currentPage * 100 > forSqarData.length) ?
										<>
										{/* <p style={{marginLeft: 15, fontSize: 20}}>Showing {currentPage * 100 - 99} to {(currentPage * 100) - (currentPage * 100 - finalSqarDataPageLength)} of {forSqarData.length} entries</p>							 */}
										<p style={{marginLeft: 15, fontSize: 14}}>Showing {currentPage * 100 - 99} to {forSqarData.length} of {forSqarData.length} entries</p>								
										</>
									:
										<>
										<p style={{marginLeft: 15, fontSize: 14}}>Showing {currentPage * 100 - 99} to {currentPage * 100} of {forSqarData.length} entries</p>								
										</>
							}
						</div>
						<div className="col">
							<div className="row pagination">
								<div className="col" style={{padding: 0, margin: 0}} >
									{
										(currentPage == 1) ?
											<Button className="ma-blue-button" disabled><i className="fa-solid fa-arrow-left"></i></Button>
										:
											<Button 
												className="ma-blue-button"
												onClick={() => prevPage()}
											>
												<i className="fa-solid fa-arrow-left"></i>
											</Button>
									}
								</div>
								<div className="col" style={{margin:0, paddingLeft: 5, paddingRight: 5}}>
									<div className="rounded ma-blue-background" 
										style={{height: 40, width: 45, textAlign: 'center', verticalAlign: 'middle', fontSize: 25, color: 'white'}}
									>
										{currentPage}
									</div>
								</div>
								<div className="col" style={{padding: 0, margin: 0}} >
									{
										(currentPage == Math.ceil(forSqarData.length / 100)) ?
											<Button className="ma-blue-button" disabled><i className="fa-solid fa-arrow-right"></i></Button>
										:
											<Button 
												className="ma-blue-button"
												onClick={() => nextPage()}
											>
												<i className="fa-solid fa-arrow-right"></i>
											</Button>
									}
								</div>
							</div>
						</div>
					</div>
				</Card.Body>
			</Card>
		</>
	)
}