import Offcanvas from 'react-bootstrap/Offcanvas';
import * as Utils from '../other/Utils';
export default function NotificationOffCanvas(props){

    const { notifications, GetNotifications, show, setShow, OpenSqarModal } = props;
    const handleClose = () => setShow(false);

    const MarkAsRead = (item) => {
        if(!item) return
        
        let bodyData = { id: item.ID };
        fetch(`${process.env.REACT_APP_SQAR_API}/api/notifications/read`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(bodyData)
        })
        .then((response) => response.json())
        .then((result) => {
            if(!result.response) return
            GetNotifications()
        })
    }

    // const RemoveNotification = (item) => {
    //     if(!item) return

    //     let bodyData = { id: item.ID };
    //     fetch(`${process.env.REACT_APP_SQAR_API}/api/notifications/delete`, {
    //         method: 'POST',
    //         headers: { 'Content-Type': 'application/json' },
    //         body: JSON.stringify(bodyData)
    //     })
    //     .then((response) => response.json())
    //     .then((result) => {
    //         if(!result.response) return
    //         GetNotifications()
    //     })
    // }

    const NotifClick = (item) => {
        OpenSqarModal(item)
        MarkAsRead(item)
    }

    const Notification = (item, index) => {

        return (
            <div
            key={index}
            className={`notification ${item.IsRead == 0 ? 'bg-light-primary' : ''} px-3 pt-2 pb-2 rounded-3 mb-1`}
            style={{fontSize: 14, cursor: 'pointer'}}
            >
                <div
                className="d-flex justify-content-between mb-2"
                onClick={() => NotifClick(item)}
                >
                    <span className="text-muted" style={{fontSize: 12}}>{Utils.ToReadableDatetime(item.NotificationDate)}</span>
                </div>
                <div
                className="row"
                onClick={() => NotifClick(item)}
                >
                    <div className="col-auto d-flex align-items-center">
                        <span className={`badge ${item.Title.includes('Waiting') ? 'bg-info' : 'bg-success'} rounded-circle`}>
                            <i className={`${item.Icon} fs-4 p-2`}></i>
                        </span>
                    </div>
                    <div className="col ps-1">
                        <div className="fw-bold text-dark">{item.Title}</div>
                        {item.Description.split('|').map((description, idx) => <div key={idx} className="text-secondary">{description}</div> )}
                        
                    </div>
                    <div className="col-auto d-flex align-items-center">
                        {item.IsRead == 0 ? <i className="fa-solid fa-circle text-primary"></i> : ''}
                    </div>
                </div>
                {item.IsRead == 0
                ?   <div className="d-flex justify-content-end">
                        <a onClick={() => MarkAsRead(item)}>Mark as read</a>
                    </div>
                :   ''
                }
            </div>
        )
    }

    return(
        <Offcanvas show={show} onHide={handleClose} placement={'end'}>
            <Offcanvas.Header closeButton className="pb-0">
                <Offcanvas.Title style={{fontSize: 20}}>Notifications</Offcanvas.Title>
            </Offcanvas.Header>
            <Offcanvas.Body className="pt-4">
                {notifications && (
                    notifications.data.map((item, index) => Notification(item, index))
                )}
                {notifications.data.length < 1 && (<p>You have no notifications...</p>)}
            </Offcanvas.Body>
        </Offcanvas>
    )

}