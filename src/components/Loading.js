import {Spinner, Row, Col} from 'react-bootstrap'

export default function Loading () {
	return(
		<Row>
			<Col xs="5">
			</Col>
			<Col xs="2" className="d-flex justify-content-center align-items-center">
				<Spinner animation="border" variant="primary" style={{width: '3rem', height: '3rem'}}/>
			</Col>
			<Col xs="5">
			</Col>
		</Row>		
	)
}