import {React, useState, useContext} from 'react'
import {Modal, Button} from 'react-bootstrap'
import Swal from 'sweetalert2'
import TextareaAutosize from 'react-textarea-autosize'
import ViewContext from '../ViewContext'

function CancelSqar(props){ 
    const [cancelRemarks, setCancelRemarks] = useState('')
    const {sqarWaiter, setSqarWaiter} = useContext(ViewContext)

    const Cancel = () => {
        Swal.fire({
            title: 'Please Confirm',
            text: 'Are you sure to cancel this SQAR? This is not revertable!',
            icon: 'question',
            confirmButtonColor: "#3C5393",
            confirmButtonText: 'Yes',
            showCancelButton: true
        }).then(result => {
            if(result.isConfirmed){
                fetch(`${process.env.REACT_APP_SQAR_API}/api/sqar/cancel`, {
                    method: 'POST',
                    headers: {'Content-Type': 'application/json'},
                    body: JSON.stringify({
                        sqarControlNo: props.data,
                        cancelledBy: localStorage.getItem('fullName'),
                        cancelRemarks: cancelRemarks
                    })
                })
                .then(response => response.json())
                .then(result => {
                    if(result == 1){
                        Swal.fire({
                            title: 'Success',
                            text: 'SQAR cancelled successfully!',
                            icon: 'success',
                            confirmButtonColor: "#3C5393"
                        })
                        .then(result => {
                            props.onHide(true)
                            setCancelRemarks('')
                        })                        
                    }else{
                        Swal.fire({
                            title: 'Error',
                            text: 'Something went wrong. Please try again!',
                            icon: 'error',
                            confirmButtonColor: "#3C5393"
                        })
                        .then(result => {
                            props.onHide(true)
                            setCancelRemarks('')
                        })  
                    }
                    setSqarWaiter(!sqarWaiter)
                })
            }
        })
    } 

    return(
        <Modal
        {...props}
        size="md"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        >
            <Modal.Header closeButton>
            </Modal.Header>
            <Modal.Body>
                <h6>Cancellation Remarks</h6>
                <TextareaAutosize
                className="border-none w-100"
                minRows={5}
                defaultValue={cancelRemarks}
                placeholder=" add cancellation remarks..."
                onChange={e => setCancelRemarks(e.target.value)}
                />
            </Modal.Body>
            <Modal.Footer>
                <Button 
                className="ma-red-button"
                onClick={() => Cancel()}
                >
                    <i className="fa-sharp fa-solid fa-ban"></i> Cancel SQAR
                </Button>
                <Button 
                className="ma-gray-button" 
                onClick={() => {props.onHide(); setCancelRemarks('')}}
                >
                    <i className="fa-solid fa-xmark"></i> Close
                </Button>               
            </Modal.Footer>
        </Modal>
    );
}
export default CancelSqar;