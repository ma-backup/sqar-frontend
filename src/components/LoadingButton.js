import Button from 'react-bootstrap/Button';
import Spinner from 'react-bootstrap/Spinner';

function LoadingButton() {
  return (
    <>
      <Button className='ma-blue-button' disabled>
        <Spinner
          as="span"
          animation="grow"
          size="sm"
          role="status"
          aria-hidden="true"
        />
        &nbsp;<i className="fa-solid fa-download"></i> Loading illustrations...
      </Button>
    </>
  );
}

export default LoadingButton;