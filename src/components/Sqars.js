import {Table, Container, Row, Col, Button, Form, OverlayTrigger, Tooltip, Dropdown, Card, Modal, ButtonGroup, InputGroup, Badge, Alert} from 'react-bootstrap'
import {useState, useEffect, useContext, forwardRef, useImperativeHandle} from 'react'
import {useNavigate} from 'react-router-dom'
// import { withRouter } from 'react-router'
// import jsPDF from 'jspdf'
// import html2canvas from 'html2canvas'
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
import Swal from 'sweetalert2'

import Loading from '../components/Loading'
import LoadingButton from '../components/LoadingButton'
import UploadSpinner from '../components/UploadSpinner'
import CancelSqar from './CancelSqarModal'
import assuredlogo from '../components/assured-logo.jpg'
import maLogo from '../components/malogo.png'
import maLogoJpeg from '../components/malogo-jpeg.jpg'
import maLogoFormno from '../components/malogo-formno.jpg'
import sampleSign from '../components/signatures/sample-signature.png'
import sampleSignJpeg from '../components/signatures/sample-sign.jpg'
import checked from '../components/checked-box.jpg'
import unChecked from '../components/box.jpg'
import closedIcon from '../components/icon-closed.jpg'

//SIGNATURES
import noSignature from '../components/signatures/no-signature.jpg'
import noSignature2 from '../components/signatures/no-signature2.jpg'
import qcLeadGirlSign_Jane from '../components/signatures/iqc-leadgirl-sign-jane.jpg'
import qcLeadGirlSign_sheryl from '../components/signatures/iqc-leadgirl-sign-sheryl.jpg'
// import qcSpvrAlyssaSign from '../components/signatures/qc-sprvr-sign-alyssa.jpg'
// import iqcSrSprvrEmilSign from '../components/signatures/iqc-sr-sprvr-sign-emil.jpg'
// import iqcSrSprvrEmilSign2 from '../components/signatures/iqc-sr-sprvr-sign-emil-2.jpg'
import iqcSrSpvrRoseSign from '../components/signatures/iqc-sr-sprvr-sign-rose.jpg'
import qcEngineerAljadeSign from '../components/signatures/qc-engineer-sign-aljade.jpg'
import iqcSrSpvrRoseSign2 from '../components/signatures/iqc-sr-sprvr-sign-rose-2.jpg'
import qcSectionHeadLoidaSign from '../components/signatures/qc-section-head-sign-loida.jpg'
import qcAsstmngrCristySign from '../components/signatures/qc-asst-mngr-sign-cristy.jpg'
import qcAsstmngrGeorgeSign from '../components/signatures/qc-asst-mngr-sign-george.jpg'
import suppTechMngrFernanSign from '../components/signatures/supp-tect-mngr-sign-fernan.jpg'
import asstFactMngrSign from '../components/signatures/asst-fact-mngr-gigi.jpg'
import factMngrSign from '../components/signatures/fact-mngr-sign-goto.jpg'
import presSignHizakae from "../components/signatures/pres-sign-hizakae.jpg"

import ViewContext from '../ViewContext'

const Sqars = forwardRef((props, ref) => {

	const navigate = useNavigate()
	const {view, setView} = useContext(ViewContext)
	const {sqarData, setSqarData} = useContext(ViewContext)
	const {sqarWaiter, setSqarWaiter} = useContext(ViewContext)
	const {sqarStatusDefinition} = useContext(ViewContext)

	//FOR ADDING SQAR ATTACHMENTS OR SUPPORTING DOCUMENTS
	const [selectedFile, setSelectedFile] = useState(null)
	const [fileDescription, setFileDescription] = useState(null)
	const [supportingFilesData, setSupportingFilesData] = useState(null)
	const addSupportDocuments = () => {
		setIsUploadingSupportDocs(true)
		let path;
		let fileId;
		const formData = new FormData()
		formData.append('file', selectedFile)
		fetch(`${process.env.REACT_APP_SQAR_API}/api/sqar/save-supporting-documents`,{
			method: 'POST',
			headers: {
				'Accept': 'application/json'
			},
			body: formData
		})
		.then((response) => response.json())
		.then((result) => {
			if(!result.success){
				console.log(result.error.file)
				Swal.fire({
					title: 'Error',
					icon: 'error',
					confirmButtonColor: "#3C5393",
					text: result.error.file
				})
				setIsUploadingSupportDocs(false)
			}else{
				path = result.file.store_path
				fileId = result.file.id
				fetch(`${process.env.REACT_APP_SQAR_API}/api/sqar/update-supporting-document-information`, {
					method:'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						id: result.file.id,
						fileName: selectedFile.name,
						fileDescription: fileDescription,
						sqarControlNo: sqarControlNo
					})
				})
				.then((response) => response.json())
				.then((result) => {
					if(result == 1){						
						Swal.fire({
							title: 'Success',
							icon: 'success',
							confirmButtonColor: "#3C5393",
							text: "File Successfully Attached."
						})						
					}else{
						Swal.fire({
							title: 'Error',
							icon: 'error',
							confirmButtonColor: "#3C5393",
							text: "Something went wrong."
						})
					}
					let newItem = {
						name: selectedFile.name,
						description: fileDescription,
						store_path: path,
						id: fileId
					}
					setSelectedFile(null)
					setFileDescription(null)
					document.getElementById('fileInput').value = null
					document.querySelector('.fileDescription').value = ''
					setSupportingFilesData(currentItems => [...currentItems, newItem])
					setIsUploadingSupportDocs(false)
				})
			}
		})
	}
	const generateTableRowScript = () => {
		let tableRowScript = []
		if(supportingFilesData){
			for(let i = 0; i < supportingFilesData.length; i++){
				supportingFilesData.map((item) => {		
					let fileType = (item.store_path).substr((item.store_path).length -3);
					if(fileType == "doc"){
						fileType = "DOCUMENT"
					}else if(fileType == "ocx"){
						fileType = "WORD DOCUMENT"
					}else if(fileType == "pdf"){
						fileType = "PDF"
					}else if(fileType == "txt"){
						fileType = "TEXT"
					}else if(fileType == "csv"){
						fileType = "CSV"
					}else if(fileType == "lsx"){
						fileType = "EXCEL"
					}
					tableRowScript.push(
						<tr key={item.id}>
							<td>
								{
									fileType == 'DOCUMENT' ? <i className="fa-solid fa-file"></i>
									:
										fileType == 'WORD DOCUMENT' ? <i className="fa-solid fa-file-word"></i>
										:
											fileType == 'PDF' ? <i className="fa-solid fa-file-pdf"></i>
											:
												fileType == 'TEXT' ? <i className="fa-solid fa-file-lines"></i>
												:
													fileType == 'CSV' ? <i className="fa-solid fa-file-csv"></i>
													:
														<i className="fa-solid fa-file-excel"></i>
								} 
								{" " + fileType}
							</td>
							<td>{item.name}</td>
							<td>{item.description}</td>
							<td><a className="btn ma-blue-button text-white" href={`${process.env.REACT_APP_SQAR_API}/api/` + item.store_path} target="_blank"><i className="fa-solid fa-eye"></i> OPEN</a></td>
						</tr>
					)
				})
				return tableRowScript;				
			}
		}
	}
	useEffect(() => {
		generateTableRowScript()
	}, [supportingFilesData])

	//FOR SAVING SUPPLIER RESPONSE ON MATERIAL DISPOSITION (IMAGE)
	const [selectedImage, setSelectedImage] = useState(null);
	const [imageName, setImageName] = useState(null);
	const saveSupplierReplyPhoto = () => {
		const formData = new FormData();
		formData.append('image', selectedImage)
		fetch(`${process.env.REACT_APP_SQAR_API}/api/image`, {
			method: 'POST',
			headers: {
				'Accept': 'application/json'
			},
			body: formData
		})
		.then(response => response.json())
		.then(result => {

			if(result.image){
				fetch(`${process.env.REACT_APP_SQAR_API}/api/sqar/update-image-access-key`,{
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						imageId: result.id,
						imageAccessKey: sqarControlNo + '-supplier-material-disposition'
					})
				})
				.then(response => response.json())
				.then(result => {
					if(result == 1){
						Swal.fire({
							title: 'Success',
							icon: 'success',
							confirmButtonColor: "#3C5393",
							text: "Photo successfully uploaded."
						})										
					}else{
						Swal.fire({
							title: 'Error',
							icon: 'error',
							confirmButtonColor: "#3C5393",
							text: "Something went wrong."
						})
					}
				})
				setImageName(result.image)
				setSelectedImage(null)
				
				//UPDATE SQAR STATUS TO "SUPPLIER MATERIAL DISPOSITION RECEIVED"
				fetch(`${process.env.REACT_APP_SQAR_API}/api/sqar/update-status-4`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						sqarControlNo: sqarControlNo
					})
				})
				.then((response) => response.json())
				.then((result) => {
					console.log(result)
					setSqarWaiter(!sqarWaiter)
				})
				
			}else{
				Swal.fire({
					title: 'Error',
					icon: 'error',
					confirmButtonColor: "#3C5393",
					text: result.message
				})
			}		
		})
	}

	//FOR SAVING SUPPLIER RESPONSE ON ROOT CAUSE AND CORRECTIVE ACTION OR RCCA (IMAGE)
	const [isAddRccaImage, setIsAddRccaImage] = useState(false)
	const [isUploaded, setIsUploaded] = useState(false)
	const [rccaResponseDate, setRccaResponseDate] = useState('')
	const [currentSelectedRccaImage, setCurrentSelectedRccaImage] = useState(null)
	const [selectedRCCAImages, setSelectedRCCAImages] = useState(Array)
	const [rccaImagesNames, setRccaImagesNames] = useState(Array)
	const [rccaImagesScript, setRccaImagesScript] = useState(Array)
	const addRccaImage = () => {
		setSelectedRCCAImages(currentImages => [...currentImages, currentSelectedRccaImage])
		setCurrentSelectedRccaImage(null)
		setIsAddRccaImage(false)
	}
	const attachImageScript = () => {

		return(
			<>
			<tr style={{height: 200}}>
				<td colSpan="8" style={{textAlign: 'center', verticalAlign: 'middle'}}>
					<center>
						{
							currentSelectedRccaImage ? 
								<a style={{cursor: 'zoom-in'}} href={URL.createObjectURL(currentSelectedRccaImage)} target="_blank">
			        			<img alt="not found" width={"100%"} className="img-fluid" src={URL.createObjectURL(currentSelectedRccaImage)} />
			        			</a>
			        		:
			        			<Form.Group controlId="formFile" className="mb-3">
							        <Form.Label><i className="fa-solid fa-photo-film"></i> Attach Root Cause and Corrective Action</Form.Label>
							        <Form.Control 
							        style={{width: 400}} 
							        type="file"
							        disabled={!((localStorage.getItem('position') == 'IQC Sr. Supervisor' 
									|| localStorage.getItem('position') == 'QC Engineer' 
									|| localStorage.getItem('position') == 'QC Section Head' 
									|| localStorage.getItem('position') == 'QC Assistant Manager')
									&& generalStatus == "OPEN")}
							        onChange={(event) => {
							          setCurrentSelectedRccaImage(event.target.files[0]);
							        }}
									/>
							    </Form.Group>
						}				      	
				    </center>
				</td>
			</tr>
			{
				!currentSelectedRccaImage && isAddRccaImage ?
					<tr>
						<td><Button className="ma-gray-button align-right" onClick={() => setIsAddRccaImage(false)}><i className="fa-solid fa-xmark"></i> CANCEL</Button></td>
					</tr>
				:
					<></>
			}
			{
			currentSelectedRccaImage && (
			<tr>
				<td colSpan="8">
			 		<div>
			 			<Button
			 			className="ma-blue-button align-right"
			 			onClick={() => addRccaImage()}
			 			>
			 				<i className="fa-solid fa-link"></i> ATTACH
			 			</Button>

				 		<Button 
				 		className="ma-red-button align-right mx-1" onClick={()=>setCurrentSelectedRccaImage(null)}>
				 			<i className="fa-solid fa-eraser"></i> REMOVE PHOTO
				 		</Button>				        					 		
			 		</div>	        					 	
			 	</td>
			</tr>
			)
			}
			</>
		)
	}
	const buildRccaImagesScript = () => {			
		if(selectedRCCAImages.length > 0){		
			setRccaImagesScript(currentImagesScripts => [...currentImagesScripts, 
				<tr key={rccaImagesScript.length + 1} style={{height: 200}}>
					<td colSpan="8" style={{textAlign: 'center', verticalAlign: 'middle'}}>
						<center>
		        			<a style={{cursor: 'zoom-in'}} href={URL.createObjectURL(selectedRCCAImages[selectedRCCAImages.length -1])} target="_blank">
		        			<img alt="not found" width={"100%"} className="img-fluid" src={URL.createObjectURL(selectedRCCAImages[selectedRCCAImages.length -1])} />
		        			</a>
					    </center>
					</td>
				</tr>
			])
		}else if(rccaImagesNames.length > 0){			
			rccaImagesNames.map((item) => {
				setRccaImagesScript(currentImagesScripts => [...currentImagesScripts,
					<tr key={item.id} style={{height: 200}}>
						<td colSpan="8" style={{textAlign: 'center', verticalAlign: 'middle'}}>
							<center>
			        			<a style={{cursor: 'zoom-in'}} href={`${process.env.REACT_APP_SQAR_API}/api/image/` + item.image} target="_blank">
			        			<img alt="not found" width={"100%"} className="img-fluid" src={`${process.env.REACT_APP_SQAR_API}/api/image/` + item.image} />
			        			</a>
						    </center>
						</td>
					</tr>
				])				
			})
		}	
	}
	useEffect(() => {
		buildRccaImagesScript()
	}, [selectedRCCAImages, rccaImagesNames])

	const uploadRccaImages = async () => {
		setIsUploadingSupportDocs(true)
		let error = false
		const year = (new Date()).getFullYear()
		const month = (new Date()).getMonth() + 1
		const day = (new Date()).getDate()
		const hour = (new Date()).getHours()
		const minute = (new Date()).getMinutes()
		const second = (new Date()).getSeconds()
		const responseDate = [year, month < 10 ? '0' + month : month, day < 10 ? '0' + day : day].join('-')
		const hourMinuteSecond = [hour < 10 ? '0' + hour : hour, minute < 10 ? '0' + minute : minute, second < 10 ? '0' + second : second].join(':')
		const fullDateTime = [responseDate, hourMinuteSecond].join(' ')

		let tempRccaImagesNames = []
		for (let i = 0; i < selectedRCCAImages.length; i++){
			const formData = new FormData()
			formData.append('image', selectedRCCAImages[i])
			await fetch(`${process.env.REACT_APP_SQAR_API}/api/image`, {
				method: 'POST',
				headers: {
					'Accept': 'application/json'
				},
				body: formData
			})
			.then((response) => response.json())
			.then((result) => {
				tempRccaImagesNames.push(result)
				if(result.image){
					fetch(`${process.env.REACT_APP_SQAR_API}/api/sqar/update-image-access-key`,{
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						},
						body: JSON.stringify({
							imageId: result.id,
							imageAccessKey: sqarControlNo + '-supplier-rcca'
						})
					})
					.then((response) => response.json())
					.then((result) => {
						if(result !== 1){
							error = true
						}
					})
				}else{
					Swal.fire({
						title: 'Error',
						icon: 'error',
						confirmButtonColor: "#3C5393",
						text: result.message
					})
				}
			})
		}		
		if(!error){
			//UPDATE SUPPLIER RCCA RESPONSE DATE
			fetch(`${process.env.REACT_APP_SQAR_API}/api/sqar/update-supplier-rcca-response-date`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					sqarControlNo: sqarControlNo,
					rccaDateReceived: fullDateTime
				})
			})
			.then((response) => response.json())
			.then((result) => {
				if(result == 1){
					Swal.fire({
						title: 'Success',
						icon: 'success',
						confirmButtonColor: "#3C5393",
						text: "Photo successfully uploaded."
					})
					setRccaImagesScript([])
					setRccaImagesNames(tempRccaImagesNames)
					setSelectedRCCAImages([])
					setIsUploadingSupportDocs(false)
					setIsUploaded(true)
					setRccaResponseDate(fullDateTime)
					setSqarWaiter(!sqarWaiter)								
				}else{
					Swal.fire({
						title: 'Error',
						icon: 'error',
						confirmButtonColor: "#3C5393",
						text: "ERROR ON UPDATING THE SUPPLIER RCCA RESPONSE DATE."
					})
				}
			})
		}else{
			Swal.fire({
				title: 'Error',
				icon: 'error',
				confirmButtonColor: "#3C5393",
				text: "ERROR ON SAVING THE IMAGE TO THE SERVER."
			})
		}
	}

	//ILLUSTRATION IMAGENAME STORAGE
	const [illustrationsData, setIllustrationsData] = useState(Array)
	const [illustrationScript, setIllustrationScript] = useState([])
	const [viewIllustrations, setViewIllustrations] = useState(false)
	const illustrationScriptConstructor = () => {
		if(illustrationsData.length > 0){
			for(let i = 0; i < illustrationsData.length; i++){
				setIllustrationScript(currentItems => [...currentItems, 
				<>
					<tr key={illustrationsData[i].id}>
						<td style={{width: '50%', textAlign: 'center'}}
							rowSpan='2'
						>
							<center>
							{												
								<a style={{cursor: 'zoom-in'}} href={`${process.env.REACT_APP_SQAR_API}/api/image/` + illustrationsData[i].image} target="_blank">
								<img alt="not found" width={'100%'} className="img-fluid" src={`${process.env.REACT_APP_SQAR_API}/api/image/` + illustrationsData[i].image} />
								</a>
							}
						    </center>
						</td>
						<td style={{height: 18, fontWeight: 'bold', fontSize: 16}}>
							{illustrationsData[i].title}
						</td>
					</tr>
					<tr>		        				
						<td style={{whiteSpace: 'pre-wrap'}}>
							{illustrationsData[i].description}
						</td>
					</tr>
				</>
			])

			}			
		}
		setIsIllustrationsLoading(false)
	}
	useEffect(() => {
		illustrationScriptConstructor()
	}, [illustrationsData])

	const viewForProcess = () => {
		setView({
			viewForProcess: true
		})
		navigate("/")
	}

  	const renderTooltipOpenSQAR = (props) => (
	    <Tooltip id="button-tooltip" {...props}>
	      View SQAR
	    </Tooltip>
	)

  	//for modal closebutton
	const handleClose = () => {
		// setSelectedRCCAImage(null)
		// setRccaImageName(null)
		setSelectedImage(null)
		setImageName(null)
		setModalPage(1)
		setShowSqarForm(false)

		setQcSpvrApprove(false)
		setIqcSrSpvrApprove(false)
		setQcSectionHeadApprove(false)
		setQcAsstMngrApprove(false)
		setTechSuppMngrApprove(false)
		setAsstFactMngrApprove(false)
		setFactMngrApprove(false)
		setIsIncludeTechSuppMngrSuppier(null)

		setSupMatDispo_isApprovedBy_IQCSrSupervisor(false)
		setSupMatDispo_isApprovedBy_QCSectionHead(false)
		setSupMatDispo_isApprovedBy_QCAsstManager(false)

		setSupRCCA_isApprovedBy_IQCSrSupervisor(false)
		setSupRCCA_isApprovedBy_QCSectionHead(false)
		setSupRCCA_isApprovedBy_QCAsstManager(false)

		setIllustrationScript([])
		setIllustrationsData([])

		setViewIllustrations(false)

		setSupportingFilesData(null)
		setIsAddRccaImage(false)
		setCurrentSelectedRccaImage(null)
		setSelectedRCCAImages([])
		setRccaImagesNames([])
		setRccaImagesScript([])
		setIsUploaded(false)
		setMatComments('')

		setFirstDeliveryCheckTemp(firstDeliveryCheckTemp => ({
			...firstDeliveryCheckTemp,
			date: '',
			quantity: '',
			isOk: ''
		}))
		setSecondDeliveryCheckTemp(secondDeliveryCheckTemp => ({
			...secondDeliveryCheckTemp,
			date: '',
			quantity: '',
			isOk: ''
		}))
		setThirdDeliveryCheckTemp(thirdDeliveryCheckTemp => ({
			...thirdDeliveryCheckTemp,
			date: '',
			quantity: '',
			isOk: ''
		}))

		setIsAffectedLotRTV('')
		setIsAffectedLotOkToUse('')
		setGeneralStatus('')

	}

	const [modalPage, setModalPage] = useState(1)
	const modalPageSwitch = (switchToPage) => {
		setModalPage(modalPage + switchToPage)
	}

	//date converter 
	const convertDate = (date) => {

		let month = date.slice(5, 7)
		let day = date.slice(8, 10)
		let year = date.slice(0, 4)

		let months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
		
		let newDate = months[parseInt(month)-1] + " " + day + ", " + year
		return newDate
	}
	
	let currentPageItems = [];

	const [searchedItemsLength, setSearchedItemsLength] = useState(0)
	const [currentPage, setCurrentPage] = useState(1)
	const [searchKeyword, setSearchKeyword] = useState('')
	const [isLoading, setIsLoading] = useState(false)
	const [isUploadingSupportDocs, setIsUploadingSupportDocs] = useState(false)
	const [isIllustrationsLoading, setIsIllustrationsLoading] = useState(false)
	const [viewTable, setViewTable] = useState(true)
	const [showSqarForm, setShowSqarForm] = useState(false)
	const [forProcess, setForProcess] = useState([])
	const [cancelSqarModalShow, setCancelSqarModalShow] = useState(false);
	
	const [sqars, setSqars] = useState([])

	const [sqarControlNo, setSqarControlNo] = useState('')
	const [supplier, setSupplier] = useState('')
	const [noIssuedSqarMonth, setNoIssuedSqarMonth] = useState('')
	const [partName, setPartName] = useState('')
	const [partNo, setPartNo] = useState('')
	const [dateSent, setDateSent] = useState('')
	const [responseDueDateMD, setResponseDueDateMD] = useState('')
	const [responseDueDateRCAA, setResponseDueDateRCAA] = useState('')
	const [makerName, setMakerName] = useState('')
	const [withSqarResponseMonth, setWithSqarResponseMonth] = useState('')	
	const [affectedQuantity, setAffectedQuantity] = useState('')
	const [applicableModel, setApplicableModel] = useState('')	
	const [contactPerson, setContactPerson] = useState('')
	const [defectOccurence, setDefectOccurence] = useState('')
	const [inputQuantity, setInputQuantity] = useState('')
	const [dateReceived, setDateReceived] = useState('')	
	const [defects, setDefects] = useState('')
	const [defectsArr, setDefectsArr] = useState(Array) //Storage for converted defects (sting to array)
	const [defectsQty, setDefectsQty] = useState(Array)
	const [rejectQuantity, setRejectQuantity] = useState('')
	const [sampleSize, setSampleSize] = useState('')
	const [invoice, setInvoice] = useState('')
	const [rejectRate, setRejectRate] = useState('')
	const [lotNos, setLotNos] = useState('')
	const [deffectRank, setDeffectRank] = useState('')
	const [troubleClassification, setTroubleClassification] = useState('')
	const [remarks, setRemarks] = useState('')
	const [totalDefectsQuantity, setTotalDefectsQuantity] = useState('')
	const [affectedLotInvoiceNumber, setAffectedLotInvoiceNumber] = useState('')
	const [deffectiveMaterial, setDeffectiveMaterial] = useState('')
	const [ngMaterialDisposal, setNgMaterialDisposal] = useState('')
	const [ngAmount, setNGAmount] =  useState('')
	const [sqarStatus, setSqarStatus] = useState(null)
	const [matCommentsRequest, setMatCommentsRequest] = useState('')
	const [matComments, setMatComments] = useState('')
	const [isRccaDispositionApproved, setIsRccaDispositionApproved] = useState(null)
	const [isIncludeTechSuppMngrSuppier, setIsIncludeTechSuppMngrSuppier] = useState(null)
	const [firstDeliveryCheck, setFirstDeliveryCheck] = useState({
		date: '',
		quantity: '',
		isOk: ''
	})
	const [secondDeliveryCheck, setSecondDeliveryCheck] = useState({
		date: '',
		quantity: '',
		isOk: ''
	})
	const [thirdDeliveryCheck, setThirdDeliveryCheck] = useState({
		date: '',
		quantity: '',
		isOk: ''
	})
	const [isAffectedLotRTV, setIsAffectedLotRTV] = useState('')
	const [isAffectedLotOkToUse, setIsAffectedLotOkToUse] = useState('')
	const [defectInfoNo, setDefectInfoNo] = useState('')
	const [generalStatus, setGeneralStatus] = useState('')
	const [cancelReason, setCancelReason] = useState('')

	const [preparedBy, setPreparedBy] = useState('')
	const [approvedByQCSupervisor, setApprovedByQCSupervisor] = useState('')
	const [approvedByIQCSrSupervisor, setApprovedByIQCSrSupervisor] = useState('')
	const [approvedByQCSectionHead, setApprovedByQCSectionHead] = useState('')
	const [approvedByQCAsstManager, setApprovedByQCAsstManager] = useState('')
	const [approvedByTechSuppMngr, setApprovedByTechSuppMngr] = useState('')
	const [approvedByAsstFactMngr, setApprovedByAsstFactMngr] = useState('')
	const [approvedByFactMngr, setApprovedByFactMngr] = useState('')

	const [supMatDispoApprovedBy_IQCSrSupervisor, setSupMatDispoApprovedBy_IQCSrSupervisor] = useState('')
	const [supMatDispoApprovedBy_QCSectionHead, setSupMatDispoApprovedBy_QCSectionHead] = useState('')
	const [supMatDispoApprovedBy_QCAsstManager, setSupMatDispoApprovedBy_QCAsstManager] = useState('')

	const [supRCCAApprovedBy_IQCSrSupervisor, setSupRCCAApprovedBy_IQCSrSupervisor] = useState('')
	const [supRCCAApprovedBy_QCSectionHead, setSupRCCAApprovedBy_QCSectionHead] = useState('')
	const [supRCCAApprovedBy_QCAsstManager, setSupRCCAApprovedBy_QCAsstManager] = useState('')

	const getData = (item) => {		
		setSqarControlNo(item.SQAR_Control_Number)
		setSupplier(item.SQAR_Supplier_Name)
		setNoIssuedSqarMonth(item.SQAR_No_Issued)
		setPartName(item.SQAR_Part_Name)
		setPartNo(item.SQAR_Part_Number)
		setDateSent(item.SQAR_Date_Sent)
		setResponseDueDateMD(item.SQAR_Response_Due_Date)
		setResponseDueDateRCAA(item.SQAR_Response_Due_Date_RCAA)
		setMakerName(item.SQAR_Maker_Name)
		setWithSqarResponseMonth(item.SQAR_Response_Month)
		setAffectedQuantity(item.SQAR_Affected_Qty)
		setApplicableModel(item.SQAR_Model)		
		setContactPerson(item.SQAR_Contact_Person)
		setDefectOccurence(item.SQAR_Deffect_Occurence)
		setInputQuantity(item.SQAR_Input_QTY)
		setDateReceived(item.SQAR_Received_Date)		
		setDefects(item.SQAR_Deffects)		
		setRejectQuantity(item.SQAR_Rejected_Qty) //total of all defects
		setSampleSize(item.SQAR_Sample_Size)
		setInvoice(item.SQAR_Invoice_DR)
		setRejectRate(item.SQAR_Rejected_Rate)
		setLotNos(item.SQAR_Affected_Lot_No)
		setDeffectRank(item.SQAR_Defect_Rank)
		setTroubleClassification(item.SQAR_Trouble_Classification)
		setRemarks(item.SQAR_Remarks)
		setTotalDefectsQuantity(item.SQAR_Rejected_Qty)
		setAffectedLotInvoiceNumber(item.SQAR_Affected_Lot)
		setDeffectiveMaterial(item.SQAR_Defective_Mat)
		setNgMaterialDisposal(item.SQAR_NG_MAT_DispOSAL)
		setNGAmount(item.SQAR_NG_Amount)
		setPreparedBy(item.SQAR_Prepared_By)
		setApprovedByQCSupervisor(item.SQAR_QC_Supervisor)
		setApprovedByIQCSrSupervisor(item.SQAR_IQC_Sr_Supervisor)
		setApprovedByQCSectionHead(item.SQAR_QC_SH)
		setApprovedByQCAsstManager(item.SQAR_Asst_Mgr)
		setApprovedByTechSuppMngr(item.SQAR_Tech_Supp_Mngr)
		setApprovedByAsstFactMngr(item.SQAR_Asst_Factory_Mngr)
		setApprovedByFactMngr(item.SQAR_Factory_Mngr)
		setDefectsQty(JSON.parse(item.SQAR_Defects_Qty))
		// setSqarStatus(sqarStatusDefinition.filter(stat => stat.SQAR_Status_No == item.SQAR_Status))
		setSqarStatus(item.SQAR_Status)
		setSupMatDispoApprovedBy_IQCSrSupervisor(item.SQAR_SupMatDispo_IQC_Sr_Supvr_Approval)
		setSupMatDispoApprovedBy_QCSectionHead(item.SQAR_SupMatDispo_QC_Sec_Head_Approval)
		setSupMatDispoApprovedBy_QCAsstManager(item.SQAR_SupMatDispo_QC_Asst_Mangr_Approval)
		setMatCommentsRequest(item.SQAR_MAT_RCCA_Comments_Request)
		setIsRccaDispositionApproved(item.SQAR_RCCA_Disposition_isApproved)
		setRccaResponseDate(item.SQAR_RCCA_Response_Date)
		setSupRCCAApprovedBy_IQCSrSupervisor(item.SQAR_SupRCCA_IQC_Sr_Supvr_Approval)
		setSupRCCAApprovedBy_QCSectionHead(item.SQAR_SupRCCA_QC_Sec_Head_Approval)
		setSupRCCAApprovedBy_QCAsstManager(item.SQAR_SupRCCA_QC_Asst_Mangr_Approval)
		setIsIncludeTechSuppMngrSuppier(item.SQAR_Include_Supp_Tech_Mngr)
		setGeneralStatus(item.SQAR_General_Status)
		setCancelReason(item.SQAR_Cancel_Remarks)
		setFirstDeliveryCheck({
			date: item.SQAR_First_Delivery_Date,
			quantity: item.SQAR_First_Delivery_Qty ? parseFloat(item.SQAR_First_Delivery_Qty).toFixed(2) : '',
			isOk: item.SQAR_First_Delivery_isOK,
			approver: item.First_Delivery_Approver
		})
		setSecondDeliveryCheck({
			date: item.SQAR_Second_Delivery_Date,
			quantity: item.SQAR_Second_Delivery_Qty ? parseFloat(item.SQAR_Second_Delivery_Qty).toFixed(2) : '',
			isOk: item.SQAR_Second_Delivery_isOK,
			approver: item.Second_Delivery_Approver
		})
		setThirdDeliveryCheck({
			date: item.SQAR_Third_Delivery_Date,
			quantity: item.SQAR_Third_Delivery_Qty ? parseFloat(item.SQAR_Third_Delivery_Qty).toFixed(2) : '',
			isOk: item.SQAR_Third_Delivery_isOK,
			approver: item.Third_Delivery_Approver
		})
		setIsAffectedLotRTV(item.SQAR_Affected_Lot_IsRTV)
		setIsAffectedLotOkToUse(item.SQAR_Affected_Lot_IsOkToUse)
		setDefectInfoNo(item.SQAR_Defect_Info_No)
		setShowSqarForm(true)

		let defectsStr = item.SQAR_Deffects
		let defectsArr = []
		let defectQtyArr = JSON.parse(item.SQAR_Defects_Qty)
		let eachDefect = ""
		for(let i = 1; i < defectsStr.length -1; i++){
			if(defectsStr[i] != "[" && defectsStr[i] != "]"){
				eachDefect += defectsStr[i]
			}else if(defectsStr[i] == "["){
				continue
			}
			else{
				defectsArr.push(eachDefect)
				eachDefect = ""
			}
		}
		setDefectsArr(defectsArr)
		let defectsContainerArr = ['defectOne', 'defectTwo', 'defectThree', 'defectFour', 'defectFive', 'defectSix', 'defectSeven', 'defectEight', 'defectNine']
		let defectsQtyContainerArr = ['rejectQty1', 'rejectQty2', 'rejectQty3', 'rejectQty4', 'rejectQty5', 'rejectQty6', 'rejectQty7', 'rejectQty8', 'rejectQty9']
		let totalRejectQty = 0
		setTimeout(() => {
			for(let i = 0; i < defectsArr.length; i++){
				document.getElementById(defectsContainerArr[i]).innerHTML = defectsArr[i]
				document.getElementById(defectsQtyContainerArr[i]).innerHTML = defectQtyArr[i]
				totalRejectQty += defectQtyArr[i]
			}
				document.getElementById('totalRejectQty').innerHTML = totalRejectQty
		})

		//getImageName supplier material disposition
		fetch(`${process.env.REACT_APP_SQAR_API}/api/sqar/get-image-name`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				imageAccessKey: item.SQAR_Control_Number + '-supplier-material-disposition'
			})
		})
		.then(response => response.json())
		.then(result => {
			if(result.length > 0){
				setImageName(result[0].image)
			}
		})

		//get Image name supplier rcca
		fetch(`${process.env.REACT_APP_SQAR_API}/api/sqar/get-image-name`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				imageAccessKey: item.SQAR_Control_Number + '-supplier-rcca'
			})
		})
		.then(response => response.json())
		.then(result => {
			if(result.length > 0){
				setRccaImagesNames(result)
			}
		})

		setIsIllustrationsLoading(true)
		//GET ILLUSTRATIONS
		fetch(`${process.env.REACT_APP_SQAR_API}/api/sqar/get-image-name`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				imageAccessKey: item.SQAR_Control_Number + '-illustrations'
			})
		})
		.then((response) => response.json())
		.then((result) => {
			if(result.length > 0){
				setIllustrationsData(result)
			}else{
				setIsIllustrationsLoading(false)
			}			
		})

		//GET ATTACHED FILE INFORMATION
		fetch(`${process.env.REACT_APP_SQAR_API}/api/sqar/get-attached-file-information`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				sqarControlNo: item.SQAR_Control_Number
			})
		})
		.then((response) => response.json())
		.then((result) => {
			setSupportingFilesData(result)
		})
	}

	const [qcSpvrApprove, setQcSpvrApprove] = useState(false)
	const [iqcSrSpvrApprove, setIqcSrSpvrApprove] = useState(false)
	const [qcSectionHeadApprove, setQcSectionHeadApprove] = useState(false)
	const [qcAsstMngrApprove, setQcAsstMngrApprove] = useState(false)
	const [techSuppMngrApprove, setTechSuppMngrApprove] = useState(false)
	const [asstFactMngrApprove, setAsstFactMngrApprove] = useState(false)
	const [factMngrApprove, setFactMngrApprove] = useState(false)

	const [supMatDispo_isApprovedBy_IQCSrSupervisor, setSupMatDispo_isApprovedBy_IQCSrSupervisor] = useState(false)
	const [supMatDispo_isApprovedBy_QCSectionHead, setSupMatDispo_isApprovedBy_QCSectionHead] = useState(false)
	const [supMatDispo_isApprovedBy_QCAsstManager, setSupMatDispo_isApprovedBy_QCAsstManager] = useState(false)

	const approve = (approvedBy) => {		

		//This is for adding days to date sent to get the due dates
		Date.prototype.addDays = function(days) {		    
		    if(days == 2){
		    	this.setDate(this.getDate() + parseInt(days));
			    if(this.toString().slice(0, 3) == 'Sat'){
			    	this.setDate(this.getDate() + parseInt(2));
			    }
			    if(this.toString().slice(0, 3) == 'Sun'){
			    	this.setDate(this.getDate() + parseInt(2));
			    }
			    return this;
			}
			if(days == 7){
				this.setDate(this.getDate());
				let i = 0;
				while (i < 7){
					console.log(i + this)
					if(this.toString().slice(0, 3) == 'Fri'){

						this.setDate(this.getDate() + parseInt(3));
						i++

					}else{

						this.setDate(this.getDate() + parseInt(1));
						i++
					}
				}
				return this;
			}
		    
		}
		//format date to 'yyyy-mm-dd'
		const formatDate = (date) => {
			let year = date.getFullYear()
			let month = '' + (date.getMonth() + 1)
			let day = '' + date.getDate()

			if(month.length < 2){
				month = '0' + month
			}
			if(day.length < 2){ 
				day = '0' + day
			}

			return [year, month, day].join('-')
		}
		let today = formatDate(new Date())
		let todayPlusTwoDays = formatDate((new Date()).addDays(2))
		let todayPlusSevenDays = formatDate((new Date()).addDays(7))


		Swal.fire({
		  title: 'Approval Confirmation',
		  text: "Are you sure to approve this SQAR? You won't be able to revert this!",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3C5393',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, I approve it!'
		}).then((result) => {
		  	if (result.isConfirmed) {
			    fetch(`${process.env.REACT_APP_SQAR_API}/api/sqar/approve`,{
					method: 'POST',
					headers: {'Content-Type': 'application/json'},
					body: JSON.stringify({
						"position": localStorage.getItem("position"),
						"fullName": localStorage.getItem("fullName"),
						"sqarControlNo": sqarControlNo
					})
				})
				.then(response => response.json())
				.then(result => {
					if(result == 1){
						Swal.fire({
							title: 'Success',
							icon: 'success',
							confirmButtonColor: "#3C5393",
							text: "Successfully approved."
						})
						if(approvedBy == 1){
							setIqcSrSpvrApprove(true)
						}
						if(approvedBy == 2){
							setQcSectionHeadApprove(true)
						}
						if(approvedBy == 3){
							setQcAsstMngrApprove(true)
						}
						if(approvedBy == 4){
							setTechSuppMngrApprove(true)
						}
						if(approvedBy == 5){
							setAsstFactMngrApprove(true)
						}
						if(approvedBy == 6){
							setFactMngrApprove(true)
							setApprovedByFactMngr(localStorage.getItem('fullName'))
						}
					}else{
						Swal.fire({
							title: 'Error',
							icon: 'error',
							confirmButtonColor: "#3C5393",
							text: "Something went wrong. Please try again."
						})
					}
					//CHECK IF FULLY SIGNED THEN UPDATE STATUS
					fetch(`${process.env.REACT_APP_SQAR_API}/api/sqar/update-status`, {
						method: 'POST',
						headers: {'Content-Type': 'application/json'},
						body: JSON.stringify({
							 "sqarControlNo": sqarControlNo,
							 "defectRank": deffectRank,
							 "supMatDisposalDueDate": todayPlusTwoDays,
							 "supRccaDueDate": todayPlusSevenDays
						})
					})
					.then(response => response.json())
					.then(result => {
						if(result.updateSqarResult){
							setSqarStatus(2)
							setDateSent(today)
							setResponseDueDateMD(todayPlusTwoDays)
							setResponseDueDateRCAA(todayPlusSevenDays)
						}
						setSqarWaiter(!sqarWaiter)
					})			
				})
			}
		})
		
	}

	const setSqarDateSent = (selectedDate, sqarControlNo) => {

		Swal.fire({
			title: 'Setting Date Sent Confirmation',
			text: 'Are you sure to set the date sent'
		})

		fetch(`${process.env.REACT_APP_SQAR_API}/api/sqar/set-date-sent`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				sqarControlNo: sqarControlNo,
				dateSent: selectedDate
			})
		})
		.then((response) => response.json())
		.then((result) => {
			console.log(result)
		})
	}

	const approveSupMatDisposal = (approvedBy) => {
		Swal.fire({
		  title: 'Approval Confirmation',
		  text: "Approve supplier material disposition? You won't be able to revert this!",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3C5393',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, I approve it!'
		})
		.then((result) => {
			if(result.isConfirmed){
				fetch(`${process.env.REACT_APP_SQAR_API}/api/sqar/approve-supplier-material-disposal`,{
					method: 'POST',
					headers: {'Content-Type': 'application/json'},
					body: JSON.stringify({
						position: localStorage.getItem("position"),
						fullName: localStorage.getItem("fullName"),
						sqarControlNo: sqarControlNo
					})
				}).then(response => response.json()).then(result => {
					if(result == 1){
						Swal.fire({
							title: 'Success',
							icon: 'success',
							confirmButtonColor: "#3C5393",
							text: "Successfully approved."
						})
						if(approvedBy == 1){
							setSupMatDispo_isApprovedBy_IQCSrSupervisor(true)
						}
						if(approvedBy == 2){
							setSupMatDispo_isApprovedBy_QCSectionHead(true)
						}
						if(approvedBy == 3){
							setSupMatDispo_isApprovedBy_QCAsstManager(true)
						}
					}else{
						Swal.fire({
							title: 'Error',
							icon: 'error',
							confirmButtonColor: "#3C5393",
							text: "Something went wrong."
						})
					}
					setSqarWaiter(!sqarWaiter)
				})
			}
		})		
	}

	const [supRCCA_isApprovedBy_IQCSrSupervisor, setSupRCCA_isApprovedBy_IQCSrSupervisor] = useState(false)
	const [supRCCA_isApprovedBy_QCSectionHead, setSupRCCA_isApprovedBy_QCSectionHead] = useState(false)
	const [supRCCA_isApprovedBy_QCAsstManager, setSupRCCA_isApprovedBy_QCAsstManager] = useState(false)

	const approveSupRCCA = (approvedBy) => {
		Swal.fire({
		  title: 'Approval Confirmation',
		  text: "Approve supplier RCCA? You won't be able to revert this!",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3C5393',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, I approve it!'
		})
		.then((result) => {
			if(result.isConfirmed){
				fetch(`${process.env.REACT_APP_SQAR_API}/api/sqar/approve-supplier-rcca`,{
					method: 'POST',
					headers: {'Content-Type': 'application/json'},
					body: JSON.stringify({
						"position": localStorage.getItem("position"),
						"fullName": localStorage.getItem("fullName"),
						"sqarControlNo": sqarControlNo
					})
				}).then((response) => response.json()).then((result) => {
					if(result == 1){
						Swal.fire({
							title: 'Success',
							icon: 'success',
							confirmButtonColor: "#3C5393",
							text: "Successfully approved."
						})
						if(approvedBy == 1){
							setSupRCCA_isApprovedBy_IQCSrSupervisor(true)
						}
						if(approvedBy == 2){
							setSupRCCA_isApprovedBy_QCSectionHead(true)
						}
						if(approvedBy == 3){
							setSupRCCA_isApprovedBy_QCAsstManager(true)
						}
					}else{
						Swal.fire({
							title: 'Error',
							icon: 'error',
							confirmButtonColor: "#3C5393",
							text: "Something went wrong."
						})
					}
					setSqarWaiter(!sqarWaiter)
				})
			}
		})
	}

	//temporay containers for checking inputs
	const [firstDeliveryCheckTemp, setFirstDeliveryCheckTemp] = useState({
		date: '',
		quantity: '',
		isOk: ''
	})
	const [secondDeliveryCheckTemp, setSecondDeliveryCheckTemp] = useState({
		date: '',
		quantity: '',
		isOk: ''
	})
	const [thirdDeliveryCheckTemp, setThirdDeliveryCheckTemp] = useState({
		date: '',
		quantity: '',
		isOk: ''
	})
	const saveDeliveryCheck = (number) => {
		let deliveryData;
		if(number == 1){
			deliveryData = firstDeliveryCheckTemp
		}else if(number == 2){
			deliveryData = secondDeliveryCheckTemp
		}else{
			deliveryData = thirdDeliveryCheckTemp
		}
		deliveryData.isOk = true
		Swal.fire({
			title: 'Approval Confirmation',
		  	text: "Are you sure to approve the first delivery? You won't be able to revert this!",
		  	icon: 'warning',
		  	showCancelButton: true,
		  	confirmButtonColor: '#3C5393',
		  	cancelButtonColor: '#d33',
		  	confirmButtonText: 'Yes, I approve it!'
		})
		.then((result) => {
			if(result.isConfirmed){
				fetch(`${process.env.REACT_APP_SQAR_API}/api/sqar/approve-delivery`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						sqarControlNo: sqarControlNo,
						number: number,
						deliveryDate: deliveryData.date,
						deliveryQuantity: deliveryData.quantity,
						approver: localStorage.getItem("fullName")
					})
				})
				.then((response) => response.json())
				.then((result) => {
					if(result == 1){
						Swal.fire({
							title: 'Success',
							icon: 'success',
							text: 'Successfully approved.',
							confirmButtonColor: '#3C5393'
						})
						if(number == 1){
							setFirstDeliveryCheck(deliveryData)
						}else if(number == 2){
							setSecondDeliveryCheck(deliveryData)
						}else{
							setThirdDeliveryCheck(deliveryData)
							setSqarStatus(14)
						}
					}else{
						Swal.fire({
							title: 'Error',
							icon: 'error',
							text: 'Something went wrong.',
							confirmButtonColor: '#3C5393'
						})
					}
					setSqarWaiter(!sqarWaiter)
				})
			}
		})
	}

	const refresh = () => {
		setSqars([])
		setCurrentPage(1)
		setSqarWaiter(!sqarWaiter)
	}

	function getBase64ImageFromURL(url) {
	    return new Promise((resolve, reject) => {
	      var img = new Image();
	      img.setAttribute("crossOrigin", "anonymous");

	      img.onload = () => {
	        var canvas = document.createElement("canvas");
	        canvas.width = img.width;
	        canvas.height = img.height;

	        var ctx = canvas.getContext("2d");
	        ctx.drawImage(img, 0, 0);

	        var dataURL = canvas.toDataURL("image/png");

	        resolve(dataURL);
	      };

	      img.onerror = error => {
	        reject(error);
	      };

	      img.src = url;
	    });
	}

	//FOR GENERATING THE FIRST PART OF SQAR (DEFECTIVE MATERIAL INFORMATION UPTO MAT'S APPROVAL)
	const generatePDF = async () => {
		let illustrationsPDFMakeTableRowScript = [
			[
		  		  	{
		  		  		text: 'ILLUSTRATIONS',
		  		  		bold: true,
		  		  		fontSize: 9,
		  		  		alignment: 'center',
		  		  		fillColor: '#FFFF65',
		  		  		colSpan: 2,
		  		  	},
		  		  	''
		  	]
		 ]
		for(let i = 0; i < illustrationsData.length; i++){
			illustrationsPDFMakeTableRowScript.push(
			  [
	  		  	{
    				image: await getBase64ImageFromURL(process.env.REACT_APP_SQAR_API + '/api/image/' + illustrationsData[i].image),
    				width: 270,
    				alignment: 'center'
    			},
    			{
    				stack: [
    					{
    						text: illustrationsData[i].title,
		    				fontSize: 10,
		    				bold: true,
		    				margin: [0, 3, 0, 5]
    					},
    					{
			  		  		text: illustrationsData[i].description,
		    				fontSize: 9
			  		  	}
    				],
    				unbreakable: true 				
    			}
	  		  ]
			)
		}
		let illustrationsPDFMakeScript = [
			{
		      pageBreak: 'before',
			  columns: [
		        {
	          		image: maLogoJpeg, 
	          		fit: [50, 50],
	          		margin: [5, 0, 0, 0]
	          	}, 
		        {
	          		image: assuredlogo, 
	          		fit: [50, 50], alignment: 'right'
	          	}
		      ]
			},
			{
		      table: {
		        widths: ['*', 100, 75],
		        body: [
		          [ 
		          	{
		          		text: 'SUPPLIER QUALITY ABNORMALITY REPORT (SQAR)', 
		          		bold: true, 
		          		fontSize: 10,
		          		fillColor: '#9966FF', 
		          		alignment: 'center',
		          		margin: [0, 2, 0, 2]
		          	}, 
		          	{
		          		text: 'CONTROL NO.',
		          		fontSize: 10, 
		          		fillColor: '#F8CBAD', 
		          		alignment: 'right',
		          		margin: [0, 2, 0, 2]
		          	}, 
		          	{
		          		text: sqarControlNo,
		          		fontSize: 10,
		          		alignment: 'center',
		          		margin: [0, 2, 0, 2]
		          	}
		          ]
		        ]
		      },		      
		      margin: [0, 5, 0, 2]
		    },
			{
			  table: {			  	
			  	widths: ['*', '*'],
			  	body: illustrationsPDFMakeTableRowScript,
			  	dontBreakRows: true
			  }
			}
		]

		pdfMake.vfs = pdfFonts.pdfMake.vfs;
		var docDefinition = {
		  pageSize: 'A4',
		  pageMargins: [15, 20],
		  fontSizes: 11,
		  content: [
			{
			  columns: [
		        {
	          		image: await getBase64ImageFromURL(maLogoFormno),
	          		fit: [108, 50],
	          		margin: [5, 0, 0, 0]
	          	},
		        {
	          		image: assuredlogo, 
	          		fit: [50, 50], alignment: 'right'
	          	}
		      ]
			},
		    {
		      table: {
		        widths: ['*', 100, 75],
		        body: [
		          [ 
		          	{
		          		text: 'SUPPLIER QUALITY ABNORMALITY REPORT (SQAR)', 
		          		bold: true, 
		          		fontSize: 10,
		          		fillColor: '#9966FF', 
		          		alignment: 'center',
		          		margin: [0, 2, 0, 2]
		          	}, 
		          	{
		          		text: 'CONTROL NO.',
		          		fontSize: 10, 
		          		fillColor: '#F8CBAD', 
		          		alignment: 'right',
		          		margin: [0, 2, 0, 2]
		          	}, 
		          	{
		          		text: sqarControlNo,
		          		fontSize: 10,
		          		alignment: 'center',
		          		margin: [0, 2, 0, 2]
		          	}
		          ]
		        ]
		      },		      
		      margin: [0, 5, 0, 2]
		    },
		    {
		      table: {
		        widths: [ '*', 90, 90, '*', '*'],
		        body: [
		          [
		          	{
		          		text: 'SUPPLIER NAME', 
		          		fillColor: '#F8CBAD',
		          		fontSize: 8,
		          		alignment: 'center',
		          		// margin: [0, 3, 0, 0]
		          	},
		          	{
		          		text: 'No. of Issued SQAR (Month)',
		          		fillColor: '#F8CBAD',
		          		fontSize: 8,
		          		alignment: 'center'
		          	},
		          	{
		          		text: 'PART NAME',
		          		fillColor: '#F8CBAD',
		          		fontSize: 8,
		          		alignment: 'center',
		          		// margin: [0, 3, 0, 0]
		          	},
		          	{
		          		text: 'PART NUMBER',
		          		fillColor: '#F8CBAD',
		          		fontSize: 8,
		          		alignment: 'center',
		          		// margin: [0, 3, 0, 0]
		          	},
		          	{
		          		text: 'DATE SENT',
		          		fillColor: '#F8CBAD',
		          		fontSize: 8,
		          		alignment: 'center',
		          		// margin: [0, 3, 0, 0]
		          	}
		          ],
		          [
		          	{
		          		text: supplier,
		          		fontSize: 8,
		          		alignment: 'center',
		            },
		          	{
		          		text: noIssuedSqarMonth,
		          		fontSize: 8,
		          		alignment: 'center',
		            },
		          	{
		          		text: partName,
		          		fontSize: 8,
		          		alignment: 'center',
		            },
		          	{
		          		text: partNo,
		          		fontSize: 8,
		          		alignment: 'center',
		            },
		          	{
		          		text: dateSent ? convertDate(dateSent) : '',
		          		fontSize: 8,
		          		alignment: 'center',
		          	}
		          ],
		          [
		          	{
		          		text: 'MAKER NAME', 
		          		fillColor: '#F8CBAD',
		          		fontSize: 8,
		          		alignment: 'center',
		          		// margin: [0, 3, 0, 0]
		          	},
		          	{
		          		text: 'With SQAR Response (Month)',
		          		fillColor: '#F8CBAD',
		          		fontSize: 8,
		          		alignment: 'center'
		          	},
		          	{
		          		text: 'AFFECTED QUANTITY',
		          		fillColor: '#F8CBAD',
		          		fontSize: 8,
		          		alignment: 'center',
		          		// margin: [0, 3, 0, 0]
		          	},
		          	{
		          		text: 'APPLICABLE MODEL',
		          		fillColor: '#F8CBAD',
		          		fontSize: 8,
		          		alignment: 'center',
		          		// margin: [0, 3, 0, 0]
		          	},
		          	{
		          		text: 'RESPONSE DUE DATE (MATERIAL DISPOSITION)',
		          		fillColor: '#F8CBAD',
		          		fontSize: 8,
		          		alignment: 'center',
		          		// margin: [0, 3, 0, 0]
		          	}
		          ],
		          [
		          	{
		          		text: makerName,
		          		fontSize: 8,
		          		alignment: 'center',
		          	},
		          	{
		          		text: withSqarResponseMonth,
		          		fontSize: 8,
		          		alignment: 'center',
		          	},
		          	{
		          		text: affectedQuantity,
		          		fontSize: 8,
		          		alignment: 'center',
		          	},
		          	{
		          		text: applicableModel,
		          		fontSize: 8,
		          		alignment: 'center',
		          	},
		          	{
		          		text: responseDueDateMD ? convertDate(responseDueDateMD) : '',
		          		fontSize: 8,
		          		alignment: 'center',
		          	},
		          ],
		          [
		          	{
		          		text: 'ATTENTION TO', 
		          		fillColor: '#F8CBAD',
		          		fontSize: 8,
		          		alignment: 'center',
		          		// margin: [0, 3, 0, 0]
		          	},
		          	{
		          		text: 'DEFECT OCCURRENCE',
		          		fillColor: '#F8CBAD',
		          		fontSize: 8,
		          		alignment: 'center'
		          	},
		          	{
		          		text: 'INPUT QUANTITY',
		          		fillColor: '#F8CBAD',
		          		fontSize: 8,
		          		alignment: 'center',
		          		// margin: [0, 3, 0, 0]
		          	},
		          	{
		          		text: 'RECEIVED DATE',
		          		fillColor: '#F8CBAD',
		          		fontSize: 8,
		          		alignment: 'center',
		          		// margin: [0, 3, 0, 0]
		          	},
		          	{
		          		text: 'RESPONSE DUE DATE (RCCA)',
		          		fillColor: '#F8CBAD',
		          		fontSize: 8,
		          		alignment: 'center',
		          		// margin: [0, 3, 0, 0]
		          	}
		          ],
		          [
		          	{
		          		text: contactPerson,
		          		fontSize: 8,
		          		alignment: 'center',
		          	},
		          	{
		          		text: defectOccurence,
		          		fontSize: 8,
		          		alignment: 'center',
		          	},
		          	{
		          		text: inputQuantity,
		          		fontSize: 8,
		          		alignment: 'center',
		          	},
		          	{
		          		text: (dateReceived) ? convertDate(dateReceived) : "",
		          		fontSize: 8,
		          		alignment: 'center',
		          	},
		          	{
		          		text: responseDueDateRCAA ? convertDate(responseDueDateRCAA) : '',
		          		fontSize: 8,
		          		alignment: 'center',
		          	},
		          ],
		          [
		          	{
		          		text: 'DEFECTS', 
		          		fillColor: '#F8CBAD',
		          		fontSize: 8,
		          		alignment: 'center',
		          		// margin: [0, 3, 0, 0]
		          	},
		          	{
		          		text: 'REJECT QTY',
		          		fillColor: '#F8CBAD',
		          		fontSize: 8,
		          		alignment: 'center',
		          		// margin: [0, 3, 0, 0]
		          	},
		          	{
		          		text: 'SAMPLE SIZE',
		          		fillColor: '#F8CBAD',
		          		fontSize: 8,
		          		alignment: 'center',
		          		// margin: [0, 3, 0, 0]
		          	},
		          	{
		          		text: 'INVOICE / D.R.',
		          		fillColor: '#F8CBAD',
		          		fontSize: 8,
		          		alignment: 'center',
		          		// margin: [0, 3, 0, 0]
		          	},
		          	{
		          		text: 'LOT NO(S)',
		          		fillColor: '#F8CBAD',
		          		fontSize: 8,
		          		alignment: 'center',
		          		// margin: [0, 3, 0, 0]
		          	}
		          ],
		          [
		          	{
		          		text: defectsArr[0] ? defectsArr[0] : '',
		          		fontSize: 8,
		          		alignment: 'center',
		          		
		          	},
		          	{
		          		text: defectsQty[0] ? defectsQty[0] : '',
		          		fontSize: 8,
		          		alignment: 'center',
		          		
		          	},
		          	{
		          		text: sampleSize,
		          		fontSize: 8,
		          		alignment: 'center',
		          	},
		          	{
		          		text: invoice,
		          		fontSize: 8,
		          		alignment: 'center',
		          	},
		          	{
		          		text: lotNos,
		          		fontSize: 8,
		          		alignment: 'center',
		          		rowSpan: 3
		          	},
		          ],
		          [
		          	{
		          		text: defectsArr[1] ? defectsArr[1] : '',
		          		fontSize: 8,
		          		alignment: 'center',
		          		
		          	},
		          	{
		          		text: defectsQty[1] ? defectsQty[1] : '',
		          		fontSize: 8,
		          		alignment: 'center',
		          		
		          	},
		          	{
		          		text: 'REJECT RATE (%)',
		          		fillColor: '#F8CBAD',
		          		fontSize: 8,
		          		alignment: 'center'
		          	},
		          	{
		          		text: 'D.I. NO.',
		          		fillColor: '#F8CBAD',
		          		fontSize: 8,
		          		alignment: 'center'
		          	},
		          	''
		          ],
		          [
		          	{
		          		text: defectsArr[2] ? defectsArr[2] : '',
		          		fontSize: 8,
		          		alignment: 'center',
		          		
		          	},
		          	{
		          		text: defectsQty[2] ? defectsQty[2] : '',
		          		fontSize: 8,
		          		alignment: 'center',
		          		
		          	},
		          	{
		          		text: parseFloat(rejectRate).toFixed(2),
		          		fontSize: 8,
		          		alignment: 'center'
		          	},
		          	{
		          		text: defectInfoNo,
		          		fontSize: 8,
		          		alignment: 'center'
		          	},
		          	''
		          ],
		          [
		          	{
		          		text: defectsArr[3] ? defectsArr[3] : '',
		          		fontSize: 8,
		          		alignment: 'center',
		          		
		          	},
		          	{
		          		text: defectsQty[3] ? defectsQty[3] : '',
		          		fontSize: 8,
		          		alignment: 'center',
		          		
		          	},
		          	{
		          		text: 'DEFECT RANK',
		          		fillColor: '#F8CBAD',
		          		fontSize: 8,
		          		alignment: 'center'
		          	},
		          	{
		          		text: 'RANKING CRITERIA',
		          		fillColor: '#F8CBAD',
		          		fontSize: 8,
		          		alignment: 'center'
		          	},
		          	{
		          		text: 'TROUBLE CLASSIFICATION',
		          		fillColor: '#F8CBAD',
		          		fontSize: 8,
		          		alignment: 'center'
		          	}
		          ],
		          [
		          	{
		          		text: defectsArr[4] ? defectsArr[4] : '',
		          		fontSize: 8,
		          		alignment: 'center',
		          		
		          	},
		          	{
		          		text: defectsQty[4] ? defectsQty[4] : '',
		          		fontSize: 8,
		          		alignment: 'center',
		          		
		          	},
		          	{
		          		text: deffectRank,
		          		color: 'blue',
		          		bold: true,
		          		fontSize: 20,
		          		alignment: 'center',
		          		// margin: [0, 0, 0, 0],
		          		rowSpan: 2
		          	},
		          	{
		          		text: 'RANK A - Major Defects, Not Following UL Requirements',
		          		fontSize: 8,
		          		alignment: 'center',
		          		rowSpan: 3
		          	},
		          	{
		          		columns: [
		          			{
		          				image: ("ifEmpty " + troubleClassification).includes('DOCUMENTS/LABELS') ? checked : unChecked, 
			          			fit: [10, 10],
			          			// margin: [0, 0, 6, 0],
			          			width: 10
		          			},
		          			{
		          				text: 'DOCUMENTS / LABELS',
				          		fontSize: 8,
				          		alignment: 'center',
				          		width: '*'
		          			}
		          		],
		          	}
		          ],
		          [
		          	{
		          		text: defectsArr[5] ? defectsArr[5] : '',
		          		fontSize: 8,
		          		alignment: 'center',
		          		
		          	},
		          	{
		          		text: defectsQty[5] ? defectsQty[5] : '',
		          		fontSize: 8,
		          		alignment: 'center',
		          		
		          	},
		          	'',
		          	'',
		          	{
			          	columns: [
		          			{
		          				image: ("ifEmpty " + troubleClassification).includes('PHYSICAL PROPERTIES') ? checked : unChecked,
			          			fit: [10, 10],
			          			// margin: [2, 0, 0, 0],
			          			width: 10
		          			},
		          			{
		          				text: 'PHYSICAL PROPERTIES',
				          		fontSize: 8,
				          		alignment: 'center',
				          		width: '*'
		          			}
		          		]
			        }
		          ],
		          [
		          	{
		          		text: defectsArr[6] ? defectsArr[6] : '',
		          		fontSize: 8,
		          		alignment: 'center',
		          		
		          	},
		          	{
		          		text: defectsQty[6] ? defectsQty[6] : '',
		          		fontSize: 8,
		          		alignment: 'center',
		          		
		          	},
		          	{
						text: 'REMARKS',
						fillColor: '#F8CBAD',
						fontSize: 8,
						alignment: 'center'
					},
		          	'',
		          	{
		          		columns:[
		          			{
		          				image: ("ifEmpty " + troubleClassification).includes('DIMENSIONAL') ? checked : unChecked,
			          			fit: [10, 10],
			          			// margin: [2, 6, 0, 6],
			          			width: 10
		          			},
		          			{
		          				text: 'DIMENSIONAL',
				          		fontSize: 8,
				          		alignment: 'center',
				          		// margin: [0, 5, 0, 0],
				          		width: '*'
		          			}
	          			]
		          	}
		          ],
		          [
		          	{
		          		text: defectsArr[7] ? defectsArr[7] : '',
		          		fontSize: 8,
		          		alignment: 'center',
		          		
		          	},
		          	{
		          		text: defectsQty[7] ? defectsQty[7] : '',
		          		fontSize: 8,
		          		alignment: 'center',
		          		
		          	},
	          		{
						text: remarks,
						fontSize: 8,
						alignment: 'center',
						rowSpan: 3
					},
		          	{
		          		text: 'RANK B - Minor Defects, No Major effect in function',
		          		fontSize: 8,
		          		alignment: 'center',
		          		rowSpan: 3
		          	},
		          	{
		          		columns: [
		          			{
		          				image: ("ifEmpty " + troubleClassification).includes('FUNCTIONAL') ? checked : unChecked,
			          			fit: [10, 10],
			          			// margin: [2, 6, 0, 6],
			          			width: 10
		          			},
		          			{
		          				text: 'FUNCTIONAL',
				          		fontSize: 8,
				          		alignment: 'center',
				          		// margin: [0, 6, 0, 0],
				          		width: '*'
		          			}
	          			]
		          	}
		          ],
		          [
		          	{
		          		text: defectsArr[8] ? defectsArr[8] : '',
		          		fontSize: 8,
		          		alignment: 'center',
		          		
		          	},
		          	{
		          		text: defectsQty[8] ? defectsQty[8] : '',
		          		fontSize: 8,
		          		alignment: 'center',
		          		
		          	},
		          	'',
		          	'',
		          	{
		          		columns: [
		          			{
		          				image: ("ifEmpty " + troubleClassification).includes('GREEN PURCHASING STD.') ? checked : unChecked, 
			          			fit: [10, 10],
			          			// margin: [2, 6, 0, 0],
			          			width: 10
		          			},
		          			{
		          				text: 'GREEN PURCHASING STD',
				          		fontSize: 8,
				          		alignment: 'center',
				          		width: '*'
		          			}
	          			]
		          	}
		          ],
		          [
		          	{
		          		text: 'TOTAL DEFECT QTY',
		          		fillColor: '#F8CBAD',
		          		fontSize: 8,
		          		alignment: 'center'
		          	},
		          	{
		          		text: totalDefectsQuantity,
		          		fontSize: 8,
		          		alignment: 'center'
		          	},
		          	'',
		          	'',
		          	''
		          ]
		        ]
		      },
		      margin: [0, 0, 0, 2]
		    },
		    {
		    	table: {
		    		widths: [100, '*', '*', '*'],
		    		body: [
		    			[
			    			{
			    				text: 'MAT MATERIAL DISPOSITION',
				    			fontSize: 9,
				    			bold: true,
				    			fillColor: '#B4C6E7',
			          			alignment: 'center',
			          			margin: [0, 2, 0, 2],
			          			colSpan: 4
			    			},
			    			'',
			    			'',
			    			''
		    			],
		    			[
		    				{
		    					text: 'AFFECTED LOT / INVOICE NUMBER',
		    					fontSize: 8,
		    					italics: true,
		    					fillColor: '#F8CBAD',
		    					alignment: 'center',
		    					colSpan: 2
		    				},
		    				'',
		    				{
		    					text: 'DEFECTIVE MATERIAL',
		    					fontSize: 8,
		    					italics: true,
		    					fillColor: '#F8CBAD',
		    					alignment: 'center',		    					
		    				},
		    				{
		    					text: 'NG MATERIAL DISPOSAL',
		    					fontSize: 8,
		    					italics: true,
		    					fillColor: '#F8CBAD',
		    					alignment: 'center',		    					
		    				}
		    			],
		    			[
		    				{
				          		columns: [
				          			{
				          				image: isAffectedLotRTV == 1 ? checked : unChecked, 
					          			fit: [10, 10],
					          			margin: [2, 0, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'RTV',
						          		fontSize: 8,
						          		width: '*'
				          			}
			          			]
				          	},
				          	{
				          		
				          		columns: [
				          			{
				          				image: affectedLotInvoiceNumber == "SUPPLIER SORTING / REWORK" ? checked : unChecked,
					          			fit: [10, 10],
					          			margin: [2, 0, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'SUPPLIER SORTING / REWORK',
						          		fontSize: 8,
						          		width: '*'
				          			}
			          			]
					          	
				          	},
				          	{
				          		columns: [
				          			{
				          				image: deffectiveMaterial == "REPLACEMENT" ? checked : unChecked, 
					          			fit: [10, 10],
					          			margin: [2, 0, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'REPLACEMENT',
						          		fontSize: 8,
						          		width: '*'
				          			}
			          			]
				          	},
				          	{
				          		columns: [
				          			{
				          				image: ngMaterialDisposal == "MAT DISPOSAL FEE BY SUPPLIER" ? checked : unChecked, 
					          			fit: [10, 10],
					          			margin: [2, 0, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'MAT Disposal Fee by Supplier',
						          		fontSize: 8,
						          		width: '*'
				          			}
			          			]
				          	}
		    			],
		    			[
		    				{
				          		columns: [
				          			{
				          				image: isAffectedLotOkToUse == 1 ? checked : unChecked, 
					          			fit: [10, 10],
					          			margin: [2, 6, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'OK TO USE',
						          		fontSize: 8,
						          		margin: [0, 5, 0, 0],
						          		width: '*'
				          			}
			          			]
				          	},
				          	{
				          		
				          		columns: [
				          			{
				          				image: affectedLotInvoiceNumber == "MAT SORTING / REWORK (WITH CHARGE SHEET)" ? checked : unChecked, 
					          			fit: [10, 10],
					          			margin: [2, 6, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'MAT SORTING / REWORK (WITH CHARGE SHEET)',
						          		fontSize: 8,
						          		width: '*'
				          			}
			          			]
					          	
				          	},
				          	{
				          		columns: [
				          			{
				          				image: deffectiveMaterial == "REFUND / DEBIT" ? checked : unChecked, 
					          			fit: [10, 10],
					          			margin: [2, 6, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'REFUND / DEBIT',
						          		fontSize: 8,
						          		margin: [0, 5, 0, 0],
						          		width: '*'
				          			}
			          			]
				          	},
				          	{
				          		columns: [
				          			{
				          				image: ngMaterialDisposal == "RTV" ? checked : unChecked, 
					          			fit: [10, 10],
					          			margin: [2, 6, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'RTV',
						          		fontSize: 8,
						          		margin: [0, 5, 0, 0],
						          		width: '*'
				          			}
			          			]
				          	}
		    			],
		    			[
		    				'',
		    				'',
		    				{
		    					text: 'NG AMOUNT: $' + (ngAmount ? ngAmount : ''),
		    					fontSize: 8,
		    				},
		    				''
		    			]
		    		]
		    	},
		    	margin: [0, 0, 0, 2]
		    },
			//<======================================SIGNATORIES========================================>
		    {
		    	table: {
		    		widths: ['*', '*', '*', '*', '*', '*', '*', '*'],
					body: isIncludeTechSuppMngrSuppier == 1 ?
						[
							[
								{	
									image: preparedBy == "Jane Dagli" ?
														qcLeadGirlSign_Jane
													:
														qcLeadGirlSign_sheryl,
									fit: [40, 40],
									margin: [2, 6, 0, 0]
								},		    				
								{		    					
									image: iqcSrSpvrApprove || approvedByIQCSrSupervisor ?
										approvedByIQCSrSupervisor == 'Rose de Leon'
										? iqcSrSpvrRoseSign
										: qcEngineerAljadeSign		
									:
										noSignature, 
									fit: [40, 40],
									margin: [2, 6, 0, 0]
								},
								{		    					
									image: qcSectionHeadApprove || approvedByQCSectionHead ?
												qcSectionHeadLoidaSign
											:
												noSignature, 
									fit: [40, 40],
									margin: [2, 6, 0, 0]
								},
								{		    					
									image: qcAsstMngrApprove || approvedByQCAsstManager ?
												approvedByQCAsstManager == 'Cristy Bautista' 
												? qcAsstmngrCristySign
												: qcAsstmngrGeorgeSign
											:
												noSignature,
									fit: [40, 40],
									margin: [2, 6, 0, 0]
								},
								{		    					
									image:  techSuppMngrApprove || approvedByTechSuppMngr ?
												suppTechMngrFernanSign
											:
												noSignature,
									fit: [40, 40],
									margin: [2, 6, 0, 0]
								},
								{		    					
									image: asstFactMngrApprove || approvedByAsstFactMngr ?
												asstFactMngrSign
											:
												noSignature,
									fit: [40, 40],
									margin: [2, 6, 0, 0]
								},
								{		    					
									image: factMngrApprove || approvedByFactMngr ?
												approvedByFactMngr == "Yasushi Hizakae" ?
													presSignHizakae
												:
													factMngrSign
											:
												noSignature,
									fit: [40, 40],
									margin: [2, 6, 0, 0]
								},
								{		    					
									image: noSignature, 
									fit: [40, 40],
									margin: [2, 6, 0, 0]
								}
							],
							[
								{	
									text: preparedBy == "Sheryl Caballes" ?
												"S. Caballes"
											:
												"J. Dagli",
									fontSize: 8
								},
								{	
									text: approvedByIQCSrSupervisor == 'Rose de Leon'
									? 'Ms. R. de Leon'
									: 'Mr. A. Florida',
									fontSize: 8
								},
								{	
									text: 'Ms. L. Gabutin',
									fontSize: 8
								},
								{	
									text: approvedByQCAsstManager == 'Cristy Bautista'
									? 'Ms. C. Bautista'
									: 'Mr. G. Reyes',
									fontSize: 8
								},
								{	
									text: deffectRank == 'A' ? 'Mr. F. Gonzales' : '',
									fontSize: 8
								},
								{	
									text: deffectRank == 'A'
									? 
										asstFactMngrApprove || approvedByAsstFactMngr
										? "Ms. G. Abogado"
										: ""
									: '',
									fontSize: 8
								},
								{	
									text: deffectRank == 'A' ?
												localStorage.getItem("position") == "President" ?
													approvedByFactMngr != null && approvedByFactMngr != '' ?
														approvedByFactMngr == "Yasushi Hizakae" ?
															"Mr. Y. Hizakae"
														:
															"Mr. D. Goto"
													:
														"Mr. Y. Hizakae"
												:
													approvedByFactMngr != null && approvedByFactMngr != '' ?
														approvedByFactMngr == "Yasushi Hizakae" ?
															"Mr. Y. Hizakae"
														:
															"Mr. D. Goto"
													:
														"Mr. D. Goto"
											:
												"",
									fontSize: 8
								},
								{
									text: ''
								}
							],
							[
								{	
									text: 'QC Leadgirl',
									fontSize: 7
								},
								{	
									text: approvedByIQCSrSupervisor == 'Rose de Leon'
									? 'IQC Supervisor'
									: 'QC Engineer',
									fontSize: 7
								},
								{	
									text: 'QC Section Head',
									fontSize: 7
								},
								{	
									text: 'QC Asst. Mngr.',
									fontSize: 7
								},
								{	
									text: deffectRank == 'A' ? 'Supp. Control Mngr.' : '',
									fontSize: 7
								},
								{	
									text: deffectRank == 'A' 
									? 
										asstFactMngrApprove || approvedByAsstFactMngr
										? 'Asst. Fact. Mngr.'
										: ''
									: '',
									fontSize: 7
								},
								{	
									text: deffectRank == 'A' ?
												localStorage.getItem("position") == "President" ?
													approvedByFactMngr != null && approvedByFactMngr != '' ?
														approvedByFactMngr == "Yasushi Hizakae" ?
															"President"
														:
															"Factory Manager"
													:
														"President"
												:
													approvedByFactMngr != null && approvedByFactMngr != '' ?
														approvedByFactMngr == "Yasushi Hizakae" ?
															"President"
														:
															"Factory Manager"
													:
														"Factory Manager"
											:
												"",
									fontSize: 7
								},
								{	
									text: ''
								}
							],
							[
								{
									text: 'PREPARED BY',
									fontSize: 8,
									fillColor: '#C6E0B4',
									alignment: 'center'
								},
								{
									text: 'CHECKED BY',
									fontSize: 8,
									fillColor: '#C6E0B4',
									colSpan: 2,
									alignment: 'center'
								},
								'',
								{
									text: 'APPROVED BY',
									fontSize: 8,
									fillColor: '#C6E0B4',
									colSpan: 5,
									alignment: 'center'
								},
								'',
								'',
								'',
								''
							]
						]
					:
						[
							[
								{	
									image: preparedBy == "Jane Dagli" ?
														qcLeadGirlSign_Jane
													:
														qcLeadGirlSign_sheryl,
									fit: [40, 40],
									margin: [9, 3, 0, 0]
								},		    				
								{		    					
									image: iqcSrSpvrApprove || approvedByIQCSrSupervisor ?
										approvedByIQCSrSupervisor == 'Rose de Leon'
										? iqcSrSpvrRoseSign
										: qcEngineerAljadeSign		
									:
										noSignature, 
									fit: [40, 40],
									margin: [9, 3, 0, 0]
								},
								{		    					
									image: qcSectionHeadApprove || approvedByQCSectionHead ?
												qcSectionHeadLoidaSign
											:
												noSignature, 
									fit: [40, 40],
									margin: [9, 3, 0, 0]
								},
								{		    					
									image: qcAsstMngrApprove || approvedByQCAsstManager ?
												approvedByQCAsstManager == 'Cristy Bautista' 
												? qcAsstmngrCristySign
												: qcAsstmngrGeorgeSign
											:
												noSignature,
									fit: [40, 40],
									margin: [9, 3, 0, 0]
								},
								{		    					
									image: asstFactMngrApprove || approvedByAsstFactMngr ?
												asstFactMngrSign
											:
												noSignature,
									fit: [40, 40],
									margin: [9, 3, 0, 0]
								},
								{		    					
									image: factMngrApprove || approvedByFactMngr ?
												approvedByFactMngr == "Yasushi Hizakae" ?
													presSignHizakae
												:
													factMngrSign
											:
												noSignature,
									fit: [40, 40],
									margin: [9, 3, 0, 0]
								},
								{		    					
									image: noSignature, 
									fit: [40, 40],
									margin: [9, 3, 0, 0]
								},
								{		    					
									image: noSignature, 
									fit: [40, 40],
									margin: [9, 3, 0, 0]
								}
							],
							[
								{	
									text: preparedBy == "Sheryl Caballes" ?
												"S. Caballes"
											:
												"J. Dagli",
									fontSize: 8
								},
								{	
									text: approvedByIQCSrSupervisor == 'Rose de Leon'
									? 'Ms. R. de Leon'
									: 'Mr. A. Florida',
									fontSize: 8
								},
								{	
									text: 'Ms. L. Gabutin',
									fontSize: 8
								},
								{	
									text: approvedByQCAsstManager == 'Cristy Bautista'
									? 'Ms. C. Bautista'
									: 'Mr. G. Reyes',
									fontSize: 8
								},
								{	
									text: deffectRank == 'A' 
									? 
										asstFactMngrApprove || approvedByAsstFactMngr
										? "Ms. G. Abogado"
										: ""
									: "",
									fontSize: 8
								},
								{	
									text: deffectRank == 'A' ?
												localStorage.getItem("position") == "President" ?
													approvedByFactMngr != null && approvedByFactMngr != '' ?
														approvedByFactMngr == "Yasushi Hizakae" ?
															"Mr. Y. Hizakae"
														:
															"Mr. D. Goto"
													:
														"Mr. Y. Hizakae"
												:
													approvedByFactMngr != null && approvedByFactMngr != '' ?
														approvedByFactMngr == "Yasushi Hizakae" ?
															"Mr. Y. Hizakae"
														:
															"Mr. D. Goto"
													:
														"Mr. D. Goto"
											:
												"",
									fontSize: 8
								},
								{
									text: ''
								},
								{
									text: ''
								}
							],
							[
								{	
									text: 'QC Leadgirl',
									fontSize: 7
								},
								{	
									text: approvedByIQCSrSupervisor == 'Rose de Leon'
									? 'IQC Supervisor'
									: 'QC Engineer',
									fontSize: 7
								},
								{	
									text: 'QC Section Head',
									fontSize: 7
								},
								{	
									text: 'QC Asst. Mngr.',
									fontSize: 7
								},
								{	
									text: deffectRank == 'A'
									? 
										asstFactMngrApprove || approvedByAsstFactMngr 
										? 'Asst. Fact. Mngr.'
										: ""
									: '',
									fontSize: 7
								},
								{	
									text: deffectRank == 'A' ?
												localStorage.getItem("position") == "President" ?
													approvedByFactMngr != null && approvedByFactMngr != '' ?
														approvedByFactMngr == "Yasushi Hizakae" ?
															"President"
														:
															"Factory Manager"
													:
														"President"
												:
													approvedByFactMngr != null && approvedByFactMngr != '' ?
														approvedByFactMngr == "Yasushi Hizakae" ?
															"President"
														:
															"Factory Manager"
													:
														"Factory Manager"
											:
												"",
									fontSize: 7
								},
								{	
									text: ''
								},
								{	
									text: ''
								}
							],
							[
								{
									text: 'PREPARED BY',
									fontSize: 8,
									fillColor: '#C6E0B4',
									alignment: 'center'
								},
								{
									text: 'CHECKED BY',
									fontSize: 8,
									fillColor: '#C6E0B4',
									colSpan: 2,
									alignment: 'center'
								},
								'',
								{
									text: 'APPROVED BY',
									fontSize: 8,
									fillColor: '#C6E0B4',
									colSpan: 5,
									alignment: 'center'
								},
								'',
								'',
								'',
								''
							]
						]
		    	},
		    	margin: [0, 0, 0, 2],
		    },
		    {
		      	table: {
		    		widths: ['*','*','*','*','*','*','*','*'],
		    		body: [
		    			[
			    			{
			    				text: 'SUPPLIER PORTION - MATERIAL DISPOSITION',
			    				fontSize: 9,
			    				bold: true,
			    				alignment: 'center',
			    				fillColor: '#B4C6E7',
			    				margin: [0, 2, 0, 2],
			    				colSpan: 8,
			    			},
			    			'',
			    			'',
			    			'',
			    			'',
			    			'',
			    			'',
			    			''
		    			],
		    			[
		    				{
			    				text: 'SUPPLIER CONFIRMATION',
			    				fontSize: 9,
			    				bold: true,
			    				alignment: 'center',
			    				fillColor: '#F8CBAD',
			    				colSpan: 8,
			    			},
			    			'',
			    			'',
			    			'',
			    			'',
			    			'',
			    			'',
			    			''
		    			],
		    			[
		    				{
		    					text: 'AFFECTED LOT / INVOICE NUMBER',
			    				fontSize: 8,
			    				alignment: 'center',
			    				italics: true,
			    				fillColor: '#F8CBAD',
			    				colSpan: 4,
		    				},
		    				'',
		    				'',
		    				'',
		    				{
		    					text: 'DEFECTIVE MATERIAL',
			    				fontSize: 8,
			    				alignment: 'center',
			    				italics: true,
			    				fillColor: '#F8CBAD',
			    				colSpan: 2,
		    				},
		    				'',
		    				{
		    					text: 'NG MATERIAL DISPOSAL',
			    				fontSize: 8,
			    				alignment: 'center',
			    				italics: true,
			    				fillColor: '#F8CBAD',
			    				colSpan: 2,
		    				},
		    				''
		    			],
		    			[
			    			{
			    				columns: [
				          			{
				          				image: unChecked, 
					          			fit: [10, 10],
					          			// margin: [2, 6, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'ACCEPT',
						          		fontSize: 8,
						          		// margin: [0, 5, 0, 0],
						          		width: '*'
				          			}
				          		],
				          		colSpan: 4
			    			},
			    			'',
			    			'',
			    			'',
			    			{
			    				columns: [
				          			{
				          				image: unChecked, 
					          			fit: [10, 10],
					          			// margin: [2, 6, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'ACCEPT',
						          		fontSize: 8,
						          		// margin: [0, 5, 0, 0],
						          		width: '*'
				          			}
				          		],
				          		colSpan: 2
			    			},
			    			'',
			    			{
			    				columns: [
				          			{
				          				image: unChecked, 
					          			fit: [10, 10],
					          			// margin: [2, 6, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'ACCEPT',
						          		fontSize: 8,
						          		// margin: [0, 5, 0, 0],
						          		width: '*'
				          			}
				          		],
				          		colSpan: 2
			    			},
			    			''
		    			],
		    			[
			    			{
			    				columns: [
				          			{
				          				image: unChecked, 
					          			fit: [10, 10],
					          			// margin: [2, 6, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'NOT ACCEPT, SELECT BELOW',
						          		fontSize: 8,
						          		// margin: [0, 5, 0, 0],
						          		width: '*'
				          			}
				          		],
				          		colSpan: 4
			    			},
			    			'',
			    			'',
			    			'',
			    			{
			    				columns: [
				          			{
				          				image: unChecked, 
					          			fit: [10, 10],
					          			// margin: [2, 6, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'NOT ACCEPT, SELECT BELOW',
						          		fontSize: 8,
						          		// margin: [0, 5, 0, 0],
						          		width: '*'
				          			}
				          		],
				          		colSpan: 2
			    			},
			    			'',
			    			{
			    				columns: [
				          			{
				          				image: unChecked, 
					          			fit: [10, 10],
					          			// margin: [2, 6, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'NOT ACCEPT, SELECT BELOW',
						          		fontSize: 8,
						          		// margin: [0, 5, 0, 0],
						          		width: '*'
				          			}
				          		],
				          		colSpan: 2
			    			},
			    			''
		    			],
		    			[	
		    				{
			    				columns: [
				    				{
				    					image: unChecked,
				    					fit: [10, 10],
				    					width: 10,
				    					margin: [10, 0, 0, 0]
				    				},
				    				{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'RTV',
						          		fontSize: 8,
						          		width: 50,
						          		margin: [10, 0, 0, 0]
				          			},
				          			{
				    					image: unChecked,
				    					fit: [10, 10],
				    					width: 10,
				    					margin: [10, 0, 0, 0]
				    				},
				    				{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'SUPPLIER SORTING / REWORK',
						          		fontSize: 8,
						          		margin: [10, 0, 0, 0]
				          			},
			    				],
			    				colSpan: 4
		    				},
		    				'',
			    			'',
			    			'',
			    			{
			    				columns: [
				          			{
				          				image: unChecked, 
					          			fit: [10, 10],
					          			margin: [10, 0, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'REPLACEMENT',
						          		fontSize: 8,
						          		margin: [10, 0, 0, 0],
						          		width: '*'
				          			}
				          		],
				          		colSpan: 2
			    			},
			    			'',
			    			{
			    				columns: [
				          			{
				          				image: unChecked, 
					          			fit: [10, 10],
					          			margin: [10, 0, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'MAT DISPOSAL FEE BY SUPPLIER',
						          		fontSize: 8,
						          		margin: [10, 0, 0, 0],
						          		width: '*'
				          			}
				          		],
				          		colSpan: 2
			    			},
			    			''
		    			],
		    			[	
		    				{
			    				columns: [
				    				{
				    					image: unChecked,
				    					fit: [10, 10],
				    					width: 10,
				    					margin: [10, 0, 0, 0]
				    				},
				    				{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'OK TO USE',
						          		fontSize: 8,
						          		width: 50,
						          		margin: [10, 0, 0, 0]
				          			},
				          			{
				    					image: unChecked,
				    					fit: [10, 10],
				    					width: 10,
				    					margin: [10, 0, 0, 0]
				    				},
				    				{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'MAT SORTING / REWORK (WITH CHARGE SHEET)',
						          		fontSize: 8,
						          		margin: [10, 0, 0, 0],
						          		width: '*'
				          			},
			    				],
			    				colSpan: 4
		    				},
		    				'',
			    			'',
			    			'',
			    			{
			    				columns: [
				          			{
				          				image: unChecked, 
					          			fit: [10, 10],
					          			margin: [10, 0, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'REFUND / DEBIT',
						          		fontSize: 8,
						          		margin: [10, 0, 0, 0],
						          		width: '*'
				          			}
				          		],
				          		colSpan: 2
			    			},
			    			'',
			    			{
			    				columns: [
				          			{
				          				image: unChecked, 
					          			fit: [10, 10],
					          			margin: [10, 0, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'RTV',
						          		fontSize: 8,
						          		margin: [10, 0, 0, 0],
						          		width: '*'
				          			}
				          		],
				          		colSpan: 2
			    			},
			    			''
		    			],
		    			[
			    			{
			    				text: 'REMARKS:',
			    				fontSize: 9,
			    				bold: true,
			    				colSpan: 5,
			    				fillColor: '#F8CBAD',
			    				margin: [0, 2, 0, 2],
			    			},
			    			'',
			    			'',
			    			'',
			    			'',
			    			{
								text: "SUPPLIER'S APPROVAL",
								fontSize: 9,
								bold: true,
								alignment: 'center',
								fillColor: '#B4C6E7',
								margin: [0, 2, 0, 2],
								colSpan: 3
							},
			    			'',
			    			'',
		    			],
		    			[
			    			{
			    				text: '.',
			    				color: 'white',
			    				fontSize: 8,
			    				colSpan: 5,
			    			},
			    			'',
			    			'',
			    			'',
			    			'',
			    			{
								text: 'PREPARED',
								fontSize: 8,
								alignment: 'center',
								fillColor: '#C6E0B4'
							},
							{
								text: 'CHECKED',
								fontSize: 8,
								alignment: 'center',
								fillColor: '#C6E0B4'
							},
							{
								text: 'APPROVED',
								fontSize: 8,
								alignment: 'center',
								fillColor: '#C6E0B4'
							},
		    			],
		    			[
			    			{
			    				text: '.',
			    				color: 'white',
			    				fontSize: 8,
			    				colSpan: 5,
			    			},
			    			'',
			    			'',
			    			'',
			    			'',
			    			{
			    				text: '',
			    				rowSpan: 2,
			    			},
			    			{
			    				text: '',
			    				rowSpan: 2,
			    			},
			    			{
			    				text: '',
			    				rowSpan: 2,
			    			},
		    			],
		    			[
			    			{
			    				text: '.',
			    				color: 'white',
			    				fontSize: 8,
			    				colSpan: 5
			    			},
			    			'',
			    			'',
			    			'',
			    			'',
			    			'',
			    			'',
			    			''
		    			],
		    			[
			    			{
			    				text: '.',
			    				color: 'white',
			    				fontSize: 8,
			    				colSpan: 5
			    			},
			    			'',
			    			'',
			    			'',
			    			'',
			    			'',
			    			'',
			    			''
		    			],
		    			[
							{
								text: "MAT'S APPROVAL",
								fontSize: 9,
								bold: true,
								alignment: 'center',
								fillColor: '#B4C6E7',
								margin: [0, 2, 0, 2],
								colSpan: 8
							},
							'',
							'',
							'',
							'',
							'',
							'',
							''
						],
						[
							{
								text: 'PREPARED',
								fontSize: 8,
								alignment: 'center',
								fillColor: '#C6E0B4'
							},
							{
								text: 'CHECKED',
								fontSize: 8,
								alignment: 'center',
								fillColor: '#C6E0B4'
							},
							{
								text: 'APPROVED',
								fontSize: 8,
								alignment: 'center',
								fillColor: '#C6E0B4'
							},
							{
								text: '',
								fontSize: 8,
								alignment: 'center',
								fillColor: '#C6E0B4'
							},
							{
								text: '',
								fontSize: 8,
								alignment: 'center',
								fillColor: '#C6E0B4'
							},
							{
								text: '',
								fontSize: 8,
								alignment: 'center',
								fillColor: '#C6E0B4'
							},
							{
								text: '',
								fontSize: 8,
								alignment: 'center',
								fillColor: '#C6E0B4'
							},
							{
								text: '',
								fontSize: 8,
								alignment: 'center',
								fillColor: '#C6E0B4'
							}
						],
						[
							{		    					
		          				image: noSignature,
			          			fit: [30, 30],
			          			margin: [2, 6, 0, 0]
		    				},
		    				{		    					
		          				image: noSignature,
			          			fit: [30, 30],
			          			margin: [2, 6, 0, 0]
		    				},
		    				{		    					
		          				image: noSignature,
			          			fit: [30, 30],
			          			margin: [2, 6, 0, 0]
		    				},
		    				{		    					
		          				image: noSignature,
			          			fit: [30, 30],
			          			margin: [2, 6, 0, 0]
		    				},
		    				{		    					
		          				image: noSignature,
			          			fit: [30, 30],
			          			margin: [2, 6, 0, 0]
		    				},
		    				{		    					
		          				image: noSignature,
			          			fit: [30, 30],
			          			margin: [2, 6, 0, 0]
		    				},
		    				'',
		    				''
						],
						[
							{
								text: 'QC Engineer',
								fontSize: '8',
							},
							{
								text: 'QC Section Head',
								fontSize: '8',
							},
							{
								text: 'QC Asst. Mngr.',
								fontSize: '8',
							},
							'',
							'',
							'',
							'',
							''
						]
		    		]					
		    	}		    		    	
		    },
		    //ILLUSTRATIONS STARTS HERE
		    illustrationsData.length > 0 ?
		    	illustrationsPDFMakeScript
		    :
		    	''
		  ]
		};
		pdfMake.createPdf(docDefinition).open();
	}

	//FOR GENERATING THE APPROVED 'SUPPLIERS MATERIAL DISPOSITION' 
	const generateApprovedSupMatDispoPDF = async () => {

		let illustrationsPDFMakeTableRowScript = [
			[
		  		  	{
		  		  		text: 'ILLUSTRATIONS',
		  		  		bold: true,
		  		  		fontSize: 9,
		  		  		alignment: 'center',
		  		  		fillColor: '#FFFF65',
		  		  		colSpan: 2,
		  		  	},
		  		  	''
		  	]
		 ]
		for(let i = 0; i < illustrationsData.length; i++){
			illustrationsPDFMakeTableRowScript.push(
			  [
	  		  	{
    				image: await getBase64ImageFromURL(process.env.REACT_APP_SQAR_API + '/api/image/' + illustrationsData[i].image),
    				width: 270,
    				alignment: 'center'
    			},
    			{
    				stack: [
    					{
    						text: illustrationsData[i].title,
		    				fontSize: 10,
		    				bold: true,
		    				margin: [0, 3, 0, 5]
    					},
    					{
			  		  		text: illustrationsData[i].description,
		    				fontSize: 9
			  		  	}
    				],
    				unbreakable: true 				
    			}
	  		  ]
			)
		}
		let illustrationsPDFMakeScript = [
			{
		      pageBreak: 'before',
			  columns: [
		        {
	          		image: maLogoJpeg, 
	          		fit: [50, 50],
	          		margin: [5, 0, 0, 0]
	          	}, 
		        {
	          		image: assuredlogo, 
	          		fit: [50, 50], alignment: 'right'
	          	}
		      ]
			},
			{
		      table: {
		        widths: ['*', 100, 75],
		        body: [
		          [ 
		          	{
		          		text: 'SUPPLIER QUALITY ABNORMALITY REPORT (SQAR)', 
		          		bold: true, 
		          		fontSize: 10,
		          		fillColor: '#9966FF', 
		          		alignment: 'center',
		          		margin: [0, 2, 0, 2]
		          	}, 
		          	{
		          		text: 'CONTROL NO.',
		          		fontSize: 10, 
		          		fillColor: '#F8CBAD', 
		          		alignment: 'right',
		          		margin: [0, 2, 0, 2]
		          	}, 
		          	{
		          		text: sqarControlNo,
		          		fontSize: 10,
		          		alignment: 'center',
		          		margin: [0, 2, 0, 2]
		          	}
		          ]
		        ]
		      },		      
		      margin: [0, 5, 0, 2]
		    },
			{
			  table: {			  	
			  	widths: ['*', '*'],
			  	body: illustrationsPDFMakeTableRowScript,
			  	dontBreakRows: true
			  }
			}
		]

		pdfMake.vfs = pdfFonts.pdfMake.vfs;
		var docDefinition = {
		  pageSize: 'A4',
		  pageMargins: [15, 20],
		  fontSizes: 11,
		  content: [
			{
			  columns: [
		        {
					image: await getBase64ImageFromURL(maLogoFormno),
					fit: [108, 50],
	          		margin: [5, 0, 0, 0]
	          	}, 
		        {
	          		image: assuredlogo, 
	          		fit: [50, 50], alignment: 'right'
	          	}
		      ]
			},
		    {
		      table: {
		        widths: ['*', 100, 75],
		        body: [
		          [ 
		          	{
		          		text: 'SUPPLIER QUALITY ABNORMALITY REPORT (SQAR)', 
		          		bold: true, 
		          		fontSize: 10,
		          		fillColor: '#9966FF', 
		          		alignment: 'center',
		          		margin: [0, 2, 0, 2]
		          	}, 
		          	{
		          		text: 'CONTROL NO.',
		          		fontSize: 10, 
		          		fillColor: '#F8CBAD', 
		          		alignment: 'right',
		          		margin: [0, 2, 0, 2]
		          	}, 
		          	{
		          		text: sqarControlNo,
		          		fontSize: 10,
		          		alignment: 'center',
		          		margin: [0, 2, 0, 2]
		          	}
		          ]
		        ]
		      },		      
		      margin: [0, 5, 0, 2]
		    },
		    {
				table: {
				  widths: [ '*', 90, 90, '*', '*'],
				  body: [
					[
						{
							text: 'SUPPLIER NAME', 
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						},
						{
							text: 'No. of Issued SQAR (Month)',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center'
						},
						{
							text: 'PART NAME',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						},
						{
							text: 'PART NUMBER',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						},
						{
							text: 'DATE SENT',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						}
					],
					[
						{
							text: supplier,
							fontSize: 8,
							alignment: 'center',
					  },
						{
							text: noIssuedSqarMonth,
							fontSize: 8,
							alignment: 'center',
					  },
						{
							text: partName,
							fontSize: 8,
							alignment: 'center',
					  },
						{
							text: partNo,
							fontSize: 8,
							alignment: 'center',
					  },
						{
							text: dateSent ? convertDate(dateSent) : '',
							fontSize: 8,
							alignment: 'center',
						}
					],
					[
						{
							text: 'MAKER NAME', 
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						},
						{
							text: 'With SQAR Response (Month)',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center'
						},
						{
							text: 'AFFECTED QUANTITY',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						},
						{
							text: 'APPLICABLE MODEL',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						},
						{
							text: 'RESPONSE DUE DATE (MATERIAL DISPOSITION)',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						}
					],
					[
						{
							text: makerName,
							fontSize: 8,
							alignment: 'center',
						},
						{
							text: withSqarResponseMonth,
							fontSize: 8,
							alignment: 'center',
						},
						{
							text: affectedQuantity,
							fontSize: 8,
							alignment: 'center',
						},
						{
							text: applicableModel,
							fontSize: 8,
							alignment: 'center',
						},
						{
							text: responseDueDateMD ? convertDate(responseDueDateMD) : '',
							fontSize: 8,
							alignment: 'center',
						},
					],
					[
						{
							text: 'ATTENTION TO', 
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						},
						{
							text: 'DEFECT OCCURRENCE',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center'
						},
						{
							text: 'INPUT QUANTITY',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						},
						{
							text: 'RECEIVED DATE',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						},
						{
							text: 'RESPONSE DUE DATE (RCCA)',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						}
					],
					[
						{
							text: contactPerson,
							fontSize: 8,
							alignment: 'center',
						},
						{
							text: defectOccurence,
							fontSize: 8,
							alignment: 'center',
						},
						{
							text: inputQuantity,
							fontSize: 8,
							alignment: 'center',
						},
						{
							text: (dateReceived) ? convertDate(dateReceived) : "",
							fontSize: 8,
							alignment: 'center',
						},
						{
							text: responseDueDateRCAA ? convertDate(responseDueDateRCAA) : '',
							fontSize: 8,
							alignment: 'center',
						},
					],
					[
						{
							text: 'DEFECTS', 
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						},
						{
							text: 'REJECT QTY',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						},
						{
							text: 'SAMPLE SIZE',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						},
						{
							text: 'INVOICE / D.R.',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						},
						{
							text: 'LOT NO(S)',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						}
					],
					[
						{
							text: defectsArr[0] ? defectsArr[0] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
							text: defectsQty[0] ? defectsQty[0] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
							text: sampleSize,
							fontSize: 8,
							alignment: 'center',
						},
						{
							text: invoice,
							fontSize: 8,
							alignment: 'center',
						},
						{
							text: lotNos,
							fontSize: 8,
							alignment: 'center',
							rowSpan: 3
						},
					],
					[
						{
							text: defectsArr[1] ? defectsArr[1] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
							text: defectsQty[1] ? defectsQty[1] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
							text: 'REJECT RATE (%)',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center'
						},
						{
							text: 'D.I. NO.',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center'
						},
						''
					],
					[
						{
							text: defectsArr[2] ? defectsArr[2] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
							text: defectsQty[2] ? defectsQty[2] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
							text: parseFloat(rejectRate).toFixed(2),
							fontSize: 8,
							alignment: 'center'
						},
						{
							text: defectInfoNo,
							fontSize: 8,
							alignment: 'center'
						},
						''
					],
					[
						{
							text: defectsArr[3] ? defectsArr[3] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
							text: defectsQty[3] ? defectsQty[3] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
							text: 'DEFECT RANK',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center'
						},
						{
							text: 'RANKING CRITERIA',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center'
						},
						{
							text: 'TROUBLE CLASSIFICATION',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center'
						}
					],
					[
						{
							text: defectsArr[4] ? defectsArr[4] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
							text: defectsQty[4] ? defectsQty[4] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
							text: deffectRank,
							color: 'blue',
							bold: true,
							fontSize: 20,
							alignment: 'center',
							// margin: [0, 0, 0, 0],
							rowSpan: 2
						},
						{
							text: 'RANK A - Major Defects, Not Following UL Requirements',
							fontSize: 8,
							alignment: 'center',
							rowSpan: 3
						},
						{
							columns: [
								{
									image: ("ifEmpty " + troubleClassification).includes('DOCUMENTS/LABELS') ? checked : unChecked, 
									fit: [10, 10],
									// margin: [0, 0, 6, 0],
									width: 10
								},
								{
									text: 'DOCUMENTS / LABELS',
									fontSize: 8,
									alignment: 'center',
									width: '*'
								}
							],
						}
					],
					[
						{
							text: defectsArr[5] ? defectsArr[5] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
							text: defectsQty[5] ? defectsQty[5] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						'',
						'',
						{
							columns: [
								{
									image: ("ifEmpty " + troubleClassification).includes('PHYSICAL PROPERTIES') ? checked : unChecked,
									fit: [10, 10],
									// margin: [2, 0, 0, 0],
									width: 10
								},
								{
									text: 'PHYSICAL PROPERTIES',
									fontSize: 8,
									alignment: 'center',
									width: '*'
								}
							]
					  }
					],
					[
						{
							text: defectsArr[6] ? defectsArr[6] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
							text: defectsQty[6] ? defectsQty[6] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
						  text: 'REMARKS',
						  fillColor: '#F8CBAD',
						  fontSize: 8,
						  alignment: 'center'
					  },
						'',
						{
							columns:[
								{
									image: ("ifEmpty " + troubleClassification).includes('DIMENSIONAL') ? checked : unChecked,
									fit: [10, 10],
									// margin: [2, 6, 0, 6],
									width: 10
								},
								{
									text: 'DIMENSIONAL',
									fontSize: 8,
									alignment: 'center',
									// margin: [0, 5, 0, 0],
									width: '*'
								}
							]
						}
					],
					[
						{
							text: defectsArr[7] ? defectsArr[7] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
							text: defectsQty[7] ? defectsQty[7] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
						  text: remarks,
						  fontSize: 8,
						  alignment: 'center',
						  rowSpan: 3
					  },
						{
							text: 'RANK B - Minor Defects, No Major effect in function',
							fontSize: 8,
							alignment: 'center',
							rowSpan: 3
						},
						{
							columns: [
								{
									image: ("ifEmpty " + troubleClassification).includes('FUNCTIONAL') ? checked : unChecked,
									fit: [10, 10],
									// margin: [2, 6, 0, 6],
									width: 10
								},
								{
									text: 'FUNCTIONAL',
									fontSize: 8,
									alignment: 'center',
									// margin: [0, 6, 0, 0],
									width: '*'
								}
							]
						}
					],
					[
						{
							text: defectsArr[8] ? defectsArr[8] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
							text: defectsQty[8] ? defectsQty[8] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						'',
						'',
						{
							columns: [
								{
									image: ("ifEmpty " + troubleClassification).includes('GREEN PURCHASING STD.') ? checked : unChecked, 
									fit: [10, 10],
									// margin: [2, 6, 0, 0],
									width: 10
								},
								{
									text: 'GREEN PURCHASING STD',
									fontSize: 8,
									alignment: 'center',
									width: '*'
								}
							]
						}
					],
					[
						{
							text: 'TOTAL DEFECT QTY',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center'
						},
						{
							text: totalDefectsQuantity,
							fontSize: 8,
							alignment: 'center'
						},
						'',
						'',
						''
					]
				  ]
				},
				margin: [0, 0, 0, 2]
			},
		    {
		    	table: {
		    		widths: [100, '*', '*', '*'],
		    		body: [
		    			[
			    			{
			    				text: 'MAT MATERIAL DISPOSITION',
				    			fontSize: 9,
				    			bold: true,
				    			fillColor: '#B4C6E7',
			          			alignment: 'center',
			          			margin: [0, 2, 0, 2],
			          			colSpan: 4
			    			},
			    			'',
			    			'',
			    			''
		    			],
		    			[
		    				{
		    					text: 'AFFECTED LOT / INVOICE NUMBER',
		    					fontSize: 8,
		    					italics: true,
		    					fillColor: '#F8CBAD',
		    					alignment: 'center',
		    					colSpan: 2
		    				},
		    				'',
		    				{
		    					text: 'DEFECTIVE MATERIAL',
		    					fontSize: 8,
		    					italics: true,
		    					fillColor: '#F8CBAD',
		    					alignment: 'center',		    					
		    				},
		    				{
		    					text: 'NG MATERIAL DISPOSAL',
		    					fontSize: 8,
		    					italics: true,
		    					fillColor: '#F8CBAD',
		    					alignment: 'center',		    					
		    				}
		    			],
		    			[
		    				{
				          		columns: [
				          			{
				          				image: isAffectedLotRTV == 1 ? checked : unChecked, 
					          			fit: [10, 10],
					          			margin: [2, 0, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'RTV',
						          		fontSize: 8,
						          		width: '*'
				          			}
			          			]
				          	},
				          	{
				          		
				          		columns: [
				          			{
				          				image: affectedLotInvoiceNumber == "SUPPLIER SORTING / REWORK" ? checked : unChecked,
					          			fit: [10, 10],
					          			margin: [2, 0, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'SUPPLIER SORTING / REWORK',
						          		fontSize: 8,
						          		width: '*'
				          			}
			          			]
					          	
				          	},
				          	{
				          		columns: [
				          			{
				          				image: deffectiveMaterial == "REPLACEMENT" ? checked : unChecked, 
					          			fit: [10, 10],
					          			margin: [2, 0, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'REPLACEMENT',
						          		fontSize: 8,
						          		width: '*'
				          			}
			          			]
				          	},
				          	{
				          		columns: [
				          			{
				          				image: ngMaterialDisposal == "MAT DISPOSAL FEE BY SUPPLIER" ? checked : unChecked, 
					          			fit: [10, 10],
					          			margin: [2, 0, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'MAT Disposal Fee by Supplier',
						          		fontSize: 8,
						          		width: '*'
				          			}
			          			]
				          	}
		    			],
		    			[
		    				{
				          		columns: [
				          			{
				          				image: isAffectedLotOkToUse == 1 ? checked : unChecked, 
					          			fit: [10, 10],
					          			margin: [2, 6, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'OK TO USE',
						          		fontSize: 8,
						          		margin: [0, 5, 0, 0],
						          		width: '*'
				          			}
			          			]
				          	},
				          	{
				          		
				          		columns: [
				          			{
				          				image: affectedLotInvoiceNumber == "MAT SORTING / REWORK (WITH CHARGE SHEET)" ? checked : unChecked, 
					          			fit: [10, 10],
					          			margin: [2, 6, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'MAT SORTING / REWORK (WITH CHARGE SHEET)',
						          		fontSize: 8,
						          		width: '*'
				          			}
			          			]
					          	
				          	},
				          	{
				          		columns: [
				          			{
				          				image: deffectiveMaterial == "REFUND / DEBIT" ? checked : unChecked, 
					          			fit: [10, 10],
					          			margin: [2, 6, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'REFUND / DEBIT',
						          		fontSize: 8,
						          		margin: [0, 5, 0, 0],
						          		width: '*'
				          			}
			          			]
				          	},
				          	{
				          		columns: [
				          			{
				          				image: ngMaterialDisposal == "RTV" ? checked : unChecked, 
					          			fit: [10, 10],
					          			margin: [2, 6, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'RTV',
						          		fontSize: 8,
						          		margin: [0, 5, 0, 0],
						          		width: '*'
				          			}
			          			]
				          	}
		    			],
		    			[
		    				'',
		    				'',
		    				{
		    					text: 'NG AMOUNT: $' + (ngAmount ? ngAmount : ''),
		    					fontSize: 8,
		    				},
		    				''
		    			]
		    		]
		    	},
		    	margin: [0, 0, 0, 2]
		    },
			//SIGNATORIES
			{
		    	table: {
		    		widths: ['*', '*', '*', '*', '*', '*', '*', '*'],
					body: isIncludeTechSuppMngrSuppier == 1 ?
						[
							[
								{	
									image: preparedBy == "Jane Dagli" ?
														qcLeadGirlSign_Jane
													:
														qcLeadGirlSign_sheryl,
									fit: [40, 40],
									margin: [2, 6, 0, 0]
								},		    				
								{		    					
									image: iqcSrSpvrApprove || approvedByIQCSrSupervisor ?
										approvedByIQCSrSupervisor == 'Rose de Leon'
										? iqcSrSpvrRoseSign
										: qcEngineerAljadeSign
											:
												noSignature, 
									fit: [40, 40],
									margin: [2, 6, 0, 0]
								},
								{		    					
									image: qcSectionHeadApprove || approvedByQCSectionHead ?
												qcSectionHeadLoidaSign
											:
												noSignature, 
									fit: [40, 40],
									margin: [2, 6, 0, 0]
								},
								{		    					
									image: qcAsstMngrApprove || approvedByQCAsstManager ?
												approvedByQCAsstManager == 'Cristy Bautista' 
												? qcAsstmngrCristySign
												: qcAsstmngrGeorgeSign
											:
												noSignature,
									fit: [40, 40],
									margin: [2, 6, 0, 0]
								},
								{		    					
									image:  techSuppMngrApprove || approvedByTechSuppMngr ?
												suppTechMngrFernanSign
											:
												noSignature,
									fit: [40, 40],
									margin: [2, 6, 0, 0]
								},
								{		    					
									image: asstFactMngrApprove || approvedByAsstFactMngr ?
												asstFactMngrSign
											:
												noSignature,
									fit: [40, 40],
									margin: [2, 6, 0, 0]
								},
								{		    					
									image: factMngrApprove || approvedByFactMngr ?
												approvedByFactMngr == "Yasushi Hizakae" ?
													presSignHizakae
												:
													factMngrSign
											:
												noSignature,
									fit: [40, 40],
									margin: [2, 6, 0, 0]
								},
								{		    					
									image: noSignature, 
									fit: [40, 40],
									margin: [2, 6, 0, 0]
								}
							],
							[
								{	
									text: preparedBy == "Sheryl Caballes" ?
												"S. Caballes"
											:
												"J. Dagli",
									fontSize: 8
								},
								{	
									text: approvedByIQCSrSupervisor == 'Rose de Leon'
									? 'Ms. R. de Leon'
									: 'Mr. A. Florida',
									fontSize: 8
								},
								{	
									text: 'Ms. L. Gabutin',
									fontSize: 8
								},
								{	
									text: approvedByQCAsstManager == 'Cristy Bautista'
									? 'Ms. C. Bautista'
									: 'Mr. G. Reyes',
									fontSize: 8
								},
								{	
									text: deffectRank == 'A' ? 'Mr. F. Gonzales' : '',
									fontSize: 8
								},
								{	
									text: deffectRank == 'A' 
									? 
										asstFactMngrApprove || approvedByAsstFactMngr
										? "Ms. G. Abogado"
										: ''
									: '',
									fontSize: 8
								},
								{	
									text: deffectRank == 'A' ?
												localStorage.getItem("position") == "President" ?
													approvedByFactMngr != null && approvedByFactMngr != '' ?
														approvedByFactMngr == "Yasushi Hizakae" ?
															"Mr. Y. Hizakae"
														:
															"Mr. D. Goto"
													:
														"Mr. Y. Hizakae"
												:
													approvedByFactMngr != null && approvedByFactMngr != '' ?
														approvedByFactMngr == "Yasushi Hizakae" ?
															"Mr. Y. Hizakae"
														:
															"Mr. D. Goto"
													:
														"Mr. D. Goto"
											:
												"",
									fontSize: 8
								},
								{
									text: ''
								}
							],
							[
								{	
									text: 'QC Leadgirl',
									fontSize: 7
								},
								{	
									text: approvedByIQCSrSupervisor == 'Rose de Leon'
									? 'IQC Supervisor'
									: 'QC Engineer',
									fontSize: 7
								},
								{	
									text: 'QC Section Head',
									fontSize: 7
								},
								{	
									text: 'QC Asst. Mngr.',
									fontSize: 7
								},
								{	
									text: deffectRank == 'A' ? 'Supp. Control Mngr.' : '',
									fontSize: 7
								},
								{	
									text: deffectRank == 'A' 
									?
										asstFactMngrApprove || approvedByAsstFactMngr 
										? 'Asst. Fact. Mngr.'
										: ''
									: '',
									fontSize: 7
								},
								{	
									text: deffectRank == 'A' ?
												localStorage.getItem("position") == "President" ?
													approvedByFactMngr != null && approvedByFactMngr != '' ?
														approvedByFactMngr == "Yasushi Hizakae" ?
															"President"
														:
															"Factory Manager"
													:
														"President"
												:
													approvedByFactMngr != null && approvedByFactMngr != '' ?
														approvedByFactMngr == "Yasushi Hizakae" ?
															"President"
														:
															"Factory Manager"
													:
														"Factory Manager"
											:
												"",
									fontSize: 7
								},
								{	
									text: ''
								}
							],
							[
								{
									text: 'PREPARED BY',
									fontSize: 8,
									fillColor: '#C6E0B4',
									alignment: 'center'
								},
								{
									text: 'CHECKED BY',
									fontSize: 8,
									fillColor: '#C6E0B4',
									colSpan: 2,
									alignment: 'center'
								},
								'',
								{
									text: 'APPROVED BY',
									fontSize: 8,
									fillColor: '#C6E0B4',
									colSpan: 5,
									alignment: 'center'
								},
								'',
								'',
								'',
								''
							]
						]
					:
						[
							[
								{	
									image: preparedBy == "Jane Dagli" ?
														qcLeadGirlSign_Jane
													:
														qcLeadGirlSign_sheryl,
									fit: [40, 40],
									margin: [9, 3, 0, 0]
								},		    				
								{		    					
									image: iqcSrSpvrApprove || approvedByIQCSrSupervisor ?
										approvedByIQCSrSupervisor == 'Rose de Leon'
										? iqcSrSpvrRoseSign
										: qcEngineerAljadeSign
									:
										noSignature, 
									fit: [40, 40],
									margin: [9, 3, 0, 0]
								},
								{		    					
									image: qcSectionHeadApprove || approvedByQCSectionHead ?
												qcSectionHeadLoidaSign
											:
												noSignature, 
									fit: [40, 40],
									margin: [9, 3, 0, 0]
								},
								{		    					
									image: qcAsstMngrApprove || approvedByQCAsstManager ?
												approvedByQCAsstManager == 'Cristy Bautista' 
												? qcAsstmngrCristySign
												: qcAsstmngrGeorgeSign
											:
												noSignature,
									fit: [40, 40],
									margin: [9, 3, 0, 0]
								},
								{		    					
									image: asstFactMngrApprove || approvedByAsstFactMngr ?
												asstFactMngrSign
											:
												noSignature,
									fit: [40, 40],
									margin: [9, 3, 0, 0]
								},
								{		    					
									image: factMngrApprove || approvedByFactMngr ?
												approvedByFactMngr == "Yasushi Hizakae" ?
													presSignHizakae
												:
													factMngrSign
											:
												noSignature,
									fit: [40, 40],
									margin: [9, 3, 0, 0]
								},
								{		    					
									image: noSignature, 
									fit: [40, 40],
									margin: [9, 3, 0, 0]
								},
								{		    					
									image: noSignature, 
									fit: [40, 40],
									margin: [9, 3, 0, 0]
								}
							],
							[
								{	
									text: preparedBy == "Sheryl Caballes" ?
												"S. Caballes"
											:
												"J. Dagli",
									fontSize: 8
								},
								{	
									text: approvedByIQCSrSupervisor == 'Rose de Leon'
									? 'Ms. R. de Leon'
									: 'Mr. A. Florida',
									fontSize: 8
								},
								{	
									text: 'Ms. L. Gabutin',
									fontSize: 8
								},
								{	
									text: approvedByQCAsstManager == 'Cristy Bautista'
									? 'Ms. C. Bautista'
									: 'Mr. G. Reyes',
									fontSize: 8
								},
								{	
									text: deffectRank == 'A' 
									? 
										asstFactMngrApprove || approvedByAsstFactMngr 
										? "Ms. G. Abogado"
										: ''
									: '',
									fontSize: 8
								},
								{	
									text: deffectRank == 'A' ?
												localStorage.getItem("position") == "President" ?
													approvedByFactMngr != null && approvedByFactMngr != '' ?
														approvedByFactMngr == "Yasushi Hizakae" ?
															"Mr. Y. Hizakae"
														:
															"Mr. D. Goto"
													:
														"Mr. Y. Hizakae"
												:
													approvedByFactMngr != null && approvedByFactMngr != '' ?
														approvedByFactMngr == "Yasushi Hizakae" ?
															"Mr. Y. Hizakae"
														:
															"Mr. D. Goto"
													:
														"Mr. D. Goto"
											:
												"",
									fontSize: 8
								},
								{
									text: ''
								},
								{
									text: ''
								}
							],
							[
								{	
									text: 'QC Leadgirl',
									fontSize: 7
								},
								{	
									text: approvedByIQCSrSupervisor == 'Rose de Leon'
									? 'IQC Supervisor'
									: 'QC Engineer',
									fontSize: 7
								},
								{	
									text: 'QC Section Head',
									fontSize: 7
								},
								{	
									text: 'QC Asst. Mngr.',
									fontSize: 7
								},
								{	
									text: deffectRank == 'A'
									? 
										asstFactMngrApprove || approvedByAsstFactMngr 
										? 'Asst. Fact. Mngr.'
										: ''
									: '',
									fontSize: 7
								},
								{	
									text: deffectRank == 'A' ?
												localStorage.getItem("position") == "President" ?
													approvedByFactMngr != null && approvedByFactMngr != '' ?
														approvedByFactMngr == "Yasushi Hizakae" ?
															"President"
														:
															"Factory Manager"
													:
														"President"
												:
													approvedByFactMngr != null && approvedByFactMngr != '' ?
														approvedByFactMngr == "Yasushi Hizakae" ?
															"President"
														:
															"Factory Manager"
													:
														"Factory Manager"
											:
												"",
									fontSize: 7
								},
								{	
									text: ''
								},
								{	
									text: ''
								}
							],
							[
								{
									text: 'PREPARED BY',
									fontSize: 8,
									fillColor: '#C6E0B4',
									alignment: 'center'
								},
								{
									text: 'CHECKED BY',
									fontSize: 8,
									fillColor: '#C6E0B4',
									colSpan: 2,
									alignment: 'center'
								},
								'',
								{
									text: 'APPROVED BY',
									fontSize: 8,
									fillColor: '#C6E0B4',
									colSpan: 5,
									alignment: 'center'
								},
								'',
								'',
								'',
								''
							]
						]
		    	},
		    	margin: [0, 0, 0, 2],
		    },
			//SUPPLIER MATERIAL DISPOSITION
		    {	
		    	table: {
		    		widths: ['*', '*', '*', '*', '*', '*', '*', '*'],
		    		body: [
		    			[
			    			{
			    				text: 'SUPPLIER PORTION - MATERIAL DISPOSITION',
			    				fontSize: 9,
			    				bold: true,
			    				alignment: 'center',
			    				fillColor: '#B4C6E7',
			    				margin: [0, 2, 0, 2],
			    				colSpan: 8,

			    			},
			    			'',
			    			'',
			    			'',
			    			'',
			    			'',
			    			'',
			    			'',
		    			],
		    			[
			    			{
			    				image: await getBase64ImageFromURL(process.env.REACT_APP_SQAR_API + '/api/image/' + imageName),
			    				width: 555,
								height: 160,
			    				alignment: 'center',
			    				colSpan: 8
			    			},			    			
			    			'',
			    			'',
			    			'',
			    			'',
			    			'',
			    			'',
			    			'',
		    			]   			    			
		    		],	    		
		    	},
		    },
		    {
		    	table: {
		    		widths: ['*', '*', '*', '*', '*', '*', '*', '*'],
		    		body: [
		    			[
		    				{
			    				text: "MAT'S APPROVAL",
			    				fontSize: 9,
			    				bold: true,
			    				alignment: 'center',
			    				fillColor: '#B4C6E7',
			    				margin: [0, 2, 0, 2],
			    				colSpan: 8,

			    			},
		    				'',
		    				'',
		    				'',
		    				'',
		    				'',
		    				'',
		    				'',
		    			],
		    			[
		    				{
			    				text: "PREPARED",
			    				fontSize: 8,
			    				alignment: 'center',
			    				fillColor: '#C6E0B4'
			    			},
		    				{
			    				text: "CHECKED",
			    				fontSize: 8,
			    				alignment: 'center',
			    				fillColor: '#C6E0B4'
			    			},
		    				{
			    				text: "APPROVED",
			    				fontSize: 8,
			    				alignment: 'center',
			    				fillColor: '#C6E0B4'
			    			},
		    				{
			    				text: "",
			    				fontSize: 8,
			    				alignment: 'center',
			    				fillColor: '#C6E0B4'
			    			},
		    				{
			    				text: "",
			    				fontSize: 8,
			    				alignment: 'center',
			    				fillColor: '#C6E0B4'
			    			},
		    				{
			    				text: "",
			    				fontSize: 8,
			    				alignment: 'center',
			    				fillColor: '#C6E0B4'
			    			},
		    				{
			    				text: "",
			    				fontSize: 8,
			    				alignment: 'center',
			    				fillColor: '#C6E0B4'
			    			},
		    				{
			    				text: "",
			    				fontSize: 8,
			    				alignment: 'center',
			    				fillColor: '#C6E0B4'
			    			},
		    			],
						//SIGNATURES
		    			[
		    				{
		    					image: (supMatDispoApprovedBy_IQCSrSupervisor || supMatDispo_isApprovedBy_IQCSrSupervisor) ?
									supMatDispoApprovedBy_IQCSrSupervisor == 'Rose de Leon'
									? iqcSrSpvrRoseSign
									: qcEngineerAljadeSign
								:
									noSignature,
								margin: [9, 0, 0, 0],
		    					fit: [40, 40],

		    				},
		    				{
		    					image: (supMatDispoApprovedBy_QCSectionHead || supMatDispo_isApprovedBy_QCSectionHead) ?
		    							qcSectionHeadLoidaSign
		    						:
		    							noSignature,
								margin: [9, 0, 0, 0],
		    					fit: [40, 40],

		    				},
		    				{
		    					image: (supMatDispoApprovedBy_QCAsstManager || supMatDispo_isApprovedBy_QCAsstManager) ?
										supMatDispoApprovedBy_QCAsstManager == 'Cristy Bautista'
		    							? qcAsstmngrCristySign
										: qcAsstmngrGeorgeSign
		    						:
		    							noSignature,
								margin: [9, 0, 0, 0],
		    					fit: [40, 40],

		    				},
		    				'',
		    				'',
		    				'',
		    				'',
		    				'',
		    			],
		    			[
		    				{
		    					text: supMatDispoApprovedBy_IQCSrSupervisor == 'Rose de Leon'
								? 'IQC Supvr'
								: 'QC Engr.',
		    					fontSize: 8,
		    					alignment: 'center'
		    				},
		    				{
		    					text: 'QC Section Head',
		    					fontSize: 8,
		    					alignment: 'center'
		    				},
		    				{
		    					text: 'QC Asst. Mngr.',
		    					fontSize: 8,
		    					alignment: 'center'
		    				},
		    				'',
		    				'',
		    				'',
		    				'',
		    				''
		    			]
		    		]
		    	},
		    	margin: [0, 2, 0, 2]
		    },
		    //ILLUSTRATIONS STARTS HERE
		    illustrationsData.length > 0 ?
		    	illustrationsPDFMakeScript
		    :
		    	''
		  ]
		};
		pdfMake.createPdf(docDefinition).open();
	}

	//FOR GENERATING PDF INCLUDING SUPPLIER RCCA, MAT COMMENTS/REQUEST, MAT APPROVAL
	const generateSQARSupMatDispoWithSupRCCA = async () => {
		let illustrationsPDFMakeTableRowScript = [
			[
	  		  	{
	  		  		text: 'ILLUSTRATIONS',
	  		  		bold: true,
	  		  		fontSize: 9,
	  		  		alignment: 'center',
	  		  		fillColor: '#FFFF65',
	  		  		colSpan: 2,
	  		  	},
	  		  	''
		  	]
		 ]
		for(let i = 0; i < illustrationsData.length; i++){
			illustrationsPDFMakeTableRowScript.push(
			  [
	  		  	{
    				image: await getBase64ImageFromURL(process.env.REACT_APP_SQAR_API + '/api/image/' + illustrationsData[i].image),
    				width: 270,
    				alignment: 'center'
    			},
    			{
    				stack: [
    					{
    						text: illustrationsData[i].title,
		    				fontSize: 10,
		    				bold: true,
		    				margin: [0, 3, 0, 5]
    					},
    					{
			  		  		text: illustrationsData[i].description,
		    				fontSize: 9
			  		  	}
    				],
    				unbreakable: true 				
    			}
	  		  ]
			)
		}
		let illustrationsPDFMakeScript = [
			{
		    //   pageBreak: 'before',
			  columns: [
		        {
	          		image: maLogoJpeg, 
	          		fit: [50, 50],
	          		margin: [5, 0, 0, 0]
	          	}, 
		        {
	          		image: assuredlogo, 
	          		fit: [50, 50], alignment: 'right'
	          	}
		      ]
			},
			{
		      table: {
		        widths: ['*', 100, 75],
		        body: [
		          [ 
		          	{
		          		text: 'SUPPLIER QUALITY ABNORMALITY REPORT (SQAR)', 
		          		bold: true, 
		          		fontSize: 10,
		          		fillColor: '#9966FF', 
		          		alignment: 'center',
		          		margin: [0, 2, 0, 2]
		          	}, 
		          	{
		          		text: 'CONTROL NO.',
		          		fontSize: 10, 
		          		fillColor: '#F8CBAD', 
		          		alignment: 'right',
		          		margin: [0, 2, 0, 2]
		          	}, 
		          	{
		          		text: sqarControlNo,
		          		fontSize: 10,
		          		alignment: 'center',
		          		margin: [0, 2, 0, 2]
		          	}
		          ]
		        ]
		      },	      
		      margin: [0, 5, 0, 2]
		    },
			{
			  table: {			  	
			  	widths: ['*', '*'],
			  	body: illustrationsPDFMakeTableRowScript,
			  	dontBreakRows: true
			  },
			  pageBreak: 'after'
			}
		]

		let rccaPDFMakeTableRowScript = [
			[
    			{
		    		text: 'SUPPLIER PORTION - ROOT CAUSE & CORRECTIVE ACTION',
		    		fontSize: 9,
	    			bold: true,
	    			fillColor: '#B4C6E7',
          			alignment: 'center',
          			margin: [0, 2, 0, 2]
		    	}
		    ]
		]
		for(let i = 0; i < rccaImagesNames.length; i++){
			//get image dimension for image sizing
			let rccaImage = new Image()
			rccaImage.src = process.env.REACT_APP_SQAR_API + '/api/image/' + rccaImagesNames[i].image
			
			let iHeight = rccaImage.height
			let iWidth = rccaImage.width

			if(i == 0){
				if(i == rccaImagesNames.length -1){
					rccaPDFMakeTableRowScript.push(
						[
					    	(iHeight / iWidth >= 1.018018018018018) ? 
							    {
							    	image: await getBase64ImageFromURL(process.env.REACT_APP_SQAR_API + '/api/image/' + rccaImagesNames[i].image),
							    	width: 555,
							    	height: 560,
							    	alignment: 'center'						    
							    }
							:
								{
							    	image: await getBase64ImageFromURL(process.env.REACT_APP_SQAR_API + '/api/image/' + rccaImagesNames[i].image),
							    	width: 555,
							    	alignment: 'center'						    
							    }
					    ]
				    )
				}else{
					rccaPDFMakeTableRowScript.push(
						[
							(iHeight / iWidth >= 1.261261261261261) ? 
								{
							    	image: await getBase64ImageFromURL(process.env.REACT_APP_SQAR_API + '/api/image/' + rccaImagesNames[i].image),
							    	width: 555,
							    	height: 700,
							    	alignment: 'center'						    
							    }
							:
								{
							    	image: await getBase64ImageFromURL(process.env.REACT_APP_SQAR_API + '/api/image/' + rccaImagesNames[i].image),
							    	width: 555,
							    	alignment: 'center'						    
							    }
						]
					)
				}
			}else{
				if(i == rccaImagesNames.length -1){
					rccaPDFMakeTableRowScript.push(
						[
					    	(iHeight / iWidth >= 1.117117117117117) ? 
							    {
							    	image: await getBase64ImageFromURL(process.env.REACT_APP_SQAR_API + '/api/image/' + rccaImagesNames[i].image),
							    	width: 555,
							    	height: 620,
							    	alignment: 'center'						    
							    }
							:
								{
							    	image: await getBase64ImageFromURL(process.env.REACT_APP_SQAR_API + '/api/image/' + rccaImagesNames[i].image),
							    	width: 555,
							    	alignment: 'center'						    
							    }
					    ]
				    )
				}else{
					rccaPDFMakeTableRowScript.push(
						[
							(iHeight / iWidth >= 1.279279279279279) ? 
								{
							    	image: await getBase64ImageFromURL(process.env.REACT_APP_SQAR_API + '/api/image/' + rccaImagesNames[i].image),
							    	width: 555,
							    	height: 710,
							    	alignment: 'center'						    
							    }
							:
								{
							    	image: await getBase64ImageFromURL(process.env.REACT_APP_SQAR_API + '/api/image/' + rccaImagesNames[i].image),
							    	width: 555,
							    	alignment: 'center'						    
							    }
						]
					)
				}
			}
		}

		pdfMake.vfs = pdfFonts.pdfMake.vfs;
		var docDefinition = {
		  pageSize: 'A4',
		  pageMargins: [15, 20],
		  fontSizes: 11,
		  content: [
			//HEADER
			{
			  columns: [
		        {
					image: await getBase64ImageFromURL(maLogoFormno),
					fit: [108, 50],
	          		margin: [5, 0, 0, 0]
	          	}, 
		        {
	          		image: assuredlogo, 
	          		fit: [50, 50], alignment: 'right'
	          	}
		      ]
			},
		    {
		      table: {
		        widths: ['*', 100, 75],
		        body: [
		          [ 
		          	{
		          		text: 'SUPPLIER QUALITY ABNORMALITY REPORT (SQAR)', 
		          		bold: true, 
		          		fontSize: 10,
		          		fillColor: '#9966FF', 
		          		alignment: 'center',
		          		margin: [0, 2, 0, 2]
		          	}, 
		          	{
		          		text: 'CONTROL NO.',
		          		fontSize: 10, 
		          		fillColor: '#F8CBAD', 
		          		alignment: 'right',
		          		margin: [0, 2, 0, 2]
		          	}, 
		          	{
		          		text: sqarControlNo,
		          		fontSize: 10,
		          		alignment: 'center',
		          		margin: [0, 2, 0, 2]
		          	}
		          ]
		        ]
		      },		      
		      margin: [0, 5, 0, 2]
		    },
			//CONTENTS
			{
				table: {
				  widths: [ '*', 90, 90, '*', '*'],
				  body: [
					[
						{
							text: 'SUPPLIER NAME', 
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						},
						{
							text: 'No. of Issued SQAR (Month)',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center'
						},
						{
							text: 'PART NAME',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						},
						{
							text: 'PART NUMBER',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						},
						{
							text: 'DATE SENT',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						}
					],
					[
						{
							text: supplier,
							fontSize: 8,
							alignment: 'center',
					  },
						{
							text: noIssuedSqarMonth,
							fontSize: 8,
							alignment: 'center',
					  },
						{
							text: partName,
							fontSize: 8,
							alignment: 'center',
					  },
						{
							text: partNo,
							fontSize: 8,
							alignment: 'center',
					  },
						{
							text: dateSent ? convertDate(dateSent) : '',
							fontSize: 8,
							alignment: 'center',
						}
					],
					[
						{
							text: 'MAKER NAME', 
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						},
						{
							text: 'With SQAR Response (Month)',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center'
						},
						{
							text: 'AFFECTED QUANTITY',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						},
						{
							text: 'APPLICABLE MODEL',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						},
						{
							text: 'RESPONSE DUE DATE (MATERIAL DISPOSITION)',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						}
					],
					[
						{
							text: makerName,
							fontSize: 8,
							alignment: 'center',
						},
						{
							text: withSqarResponseMonth,
							fontSize: 8,
							alignment: 'center',
						},
						{
							text: affectedQuantity,
							fontSize: 8,
							alignment: 'center',
						},
						{
							text: applicableModel,
							fontSize: 8,
							alignment: 'center',
						},
						{
							text: responseDueDateMD ? convertDate(responseDueDateMD) : '',
							fontSize: 8,
							alignment: 'center',
						},
					],
					[
						{
							text: 'ATTENTION TO', 
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						},
						{
							text: 'DEFECT OCCURRENCE',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center'
						},
						{
							text: 'INPUT QUANTITY',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						},
						{
							text: 'RECEIVED DATE',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						},
						{
							text: 'RESPONSE DUE DATE (RCCA)',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						}
					],
					[
						{
							text: contactPerson,
							fontSize: 8,
							alignment: 'center',
						},
						{
							text: defectOccurence,
							fontSize: 8,
							alignment: 'center',
						},
						{
							text: inputQuantity,
							fontSize: 8,
							alignment: 'center',
						},
						{
							text: (dateReceived) ? convertDate(dateReceived) : "",
							fontSize: 8,
							alignment: 'center',
						},
						{
							text: responseDueDateRCAA ? convertDate(responseDueDateRCAA) : '',
							fontSize: 8,
							alignment: 'center',
						},
					],
					[
						{
							text: 'DEFECTS', 
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						},
						{
							text: 'REJECT QTY',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						},
						{
							text: 'SAMPLE SIZE',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						},
						{
							text: 'INVOICE / D.R.',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						},
						{
							text: 'LOT NO(S)',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center',
							// margin: [0, 3, 0, 0]
						}
					],
					[
						{
							text: defectsArr[0] ? defectsArr[0] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
							text: defectsQty[0] ? defectsQty[0] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
							text: sampleSize,
							fontSize: 8,
							alignment: 'center',
						},
						{
							text: invoice,
							fontSize: 8,
							alignment: 'center',
						},
						{
							text: lotNos,
							fontSize: 8,
							alignment: 'center',
							rowSpan: 3
						},
					],
					[
						{
							text: defectsArr[1] ? defectsArr[1] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
							text: defectsQty[1] ? defectsQty[1] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
							text: 'REJECT RATE (%)',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center'
						},
						{
							text: 'D.I. NO.',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center'
						},
						''
					],
					[
						{
							text: defectsArr[2] ? defectsArr[2] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
							text: defectsQty[2] ? defectsQty[2] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
							text: parseFloat(rejectRate).toFixed(2),
							fontSize: 8,
							alignment: 'center'
						},
						{
							text: defectInfoNo,
							fontSize: 8,
							alignment: 'center'
						},
						''
					],
					[
						{
							text: defectsArr[3] ? defectsArr[3] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
							text: defectsQty[3] ? defectsQty[3] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
							text: 'DEFECT RANK',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center'
						},
						{
							text: 'RANKING CRITERIA',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center'
						},
						{
							text: 'TROUBLE CLASSIFICATION',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center'
						}
					],
					[
						{
							text: defectsArr[4] ? defectsArr[4] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
							text: defectsQty[4] ? defectsQty[4] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
							text: deffectRank,
							color: 'blue',
							bold: true,
							fontSize: 20,
							alignment: 'center',
							// margin: [0, 0, 0, 0],
							rowSpan: 2
						},
						{
							text: 'RANK A - Major Defects, Not Following UL Requirements',
							fontSize: 8,
							alignment: 'center',
							rowSpan: 3
						},
						{
							columns: [
								{
									image: ("ifEmpty " + troubleClassification).includes('DOCUMENTS/LABELS') ? checked : unChecked, 
									fit: [10, 10],
									// margin: [0, 0, 6, 0],
									width: 10
								},
								{
									text: 'DOCUMENTS / LABELS',
									fontSize: 8,
									alignment: 'center',
									width: '*'
								}
							],
						}
					],
					[
						{
							text: defectsArr[5] ? defectsArr[5] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
							text: defectsQty[5] ? defectsQty[5] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						'',
						'',
						{
							columns: [
								{
									image: ("ifEmpty " + troubleClassification).includes('PHYSICAL PROPERTIES') ? checked : unChecked,
									fit: [10, 10],
									// margin: [2, 0, 0, 0],
									width: 10
								},
								{
									text: 'PHYSICAL PROPERTIES',
									fontSize: 8,
									alignment: 'center',
									width: '*'
								}
							]
					  }
					],
					[
						{
							text: defectsArr[6] ? defectsArr[6] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
							text: defectsQty[6] ? defectsQty[6] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
						  text: 'REMARKS',
						  fillColor: '#F8CBAD',
						  fontSize: 8,
						  alignment: 'center'
					  },
						'',
						{
							columns:[
								{
									image: ("ifEmpty " + troubleClassification).includes('DIMENSIONAL') ? checked : unChecked,
									fit: [10, 10],
									// margin: [2, 6, 0, 6],
									width: 10
								},
								{
									text: 'DIMENSIONAL',
									fontSize: 8,
									alignment: 'center',
									// margin: [0, 5, 0, 0],
									width: '*'
								}
							]
						}
					],
					[
						{
							text: defectsArr[7] ? defectsArr[7] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
							text: defectsQty[7] ? defectsQty[7] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
						  text: remarks,
						  fontSize: 8,
						  alignment: 'center',
						  rowSpan: 3
					  },
						{
							text: 'RANK B - Minor Defects, No Major effect in function',
							fontSize: 8,
							alignment: 'center',
							rowSpan: 3
						},
						{
							columns: [
								{
									image: ("ifEmpty " + troubleClassification).includes('FUNCTIONAL') ? checked : unChecked,
									fit: [10, 10],
									// margin: [2, 6, 0, 6],
									width: 10
								},
								{
									text: 'FUNCTIONAL',
									fontSize: 8,
									alignment: 'center',
									// margin: [0, 6, 0, 0],
									width: '*'
								}
							]
						}
					],
					[
						{
							text: defectsArr[8] ? defectsArr[8] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						{
							text: defectsQty[8] ? defectsQty[8] : '',
							fontSize: 8,
							alignment: 'center',
							
						},
						'',
						'',
						{
							columns: [
								{
									image: ("ifEmpty " + troubleClassification).includes('GREEN PURCHASING STD.') ? checked : unChecked, 
									fit: [10, 10],
									// margin: [2, 6, 0, 0],
									width: 10
								},
								{
									text: 'GREEN PURCHASING STD',
									fontSize: 8,
									alignment: 'center',
									width: '*'
								}
							]
						}
					],
					[
						{
							text: 'TOTAL DEFECT QTY',
							fillColor: '#F8CBAD',
							fontSize: 8,
							alignment: 'center'
						},
						{
							text: totalDefectsQuantity,
							fontSize: 8,
							alignment: 'center'
						},
						'',
						'',
						''
					]
				  ]
				},
				margin: [0, 0, 0, 2]
			},
		    {
		    	table: {
		    		widths: [100, '*', '*', '*'],
		    		body: [
		    			[
			    			{
			    				text: 'MAT MATERIAL DISPOSITION',
				    			fontSize: 9,
				    			bold: true,
				    			fillColor: '#B4C6E7',
			          			alignment: 'center',
			          			margin: [0, 2, 0, 2],
			          			colSpan: 4
			    			},
			    			'',
			    			'',
			    			''
		    			],
		    			[
		    				{
		    					text: 'AFFECTED LOT / INVOICE NUMBER',
		    					fontSize: 8,
		    					italics: true,
		    					fillColor: '#F8CBAD',
		    					alignment: 'center',
		    					colSpan: 2
		    				},
		    				'',
		    				{
		    					text: 'DEFECTIVE MATERIAL',
		    					fontSize: 8,
		    					italics: true,
		    					fillColor: '#F8CBAD',
		    					alignment: 'center',		    					
		    				},
		    				{
		    					text: 'NG MATERIAL DISPOSAL',
		    					fontSize: 8,
		    					italics: true,
		    					fillColor: '#F8CBAD',
		    					alignment: 'center',		    					
		    				}
		    			],
		    			[
		    				{
				          		columns: [
				          			{
				          				image: isAffectedLotRTV == 1 ? checked : unChecked, 
					          			fit: [10, 10],
					          			margin: [2, 0, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'RTV',
						          		fontSize: 8,
						          		width: '*'
				          			}
			          			]
				          	},
				          	{
				          		
				          		columns: [
				          			{
				          				image: affectedLotInvoiceNumber == "SUPPLIER SORTING / REWORK" ? checked : unChecked,
					          			fit: [10, 10],
					          			margin: [2, 0, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'SUPPLIER SORTING / REWORK',
						          		fontSize: 8,
						          		width: '*'
				          			}
			          			]
					          	
				          	},
				          	{
				          		columns: [
				          			{
				          				image: deffectiveMaterial == "REPLACEMENT" ? checked : unChecked, 
					          			fit: [10, 10],
					          			margin: [2, 0, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'REPLACEMENT',
						          		fontSize: 8,
						          		width: '*'
				          			}
			          			]
				          	},
				          	{
				          		columns: [
				          			{
				          				image: ngMaterialDisposal == "MAT DISPOSAL FEE BY SUPPLIER" ? checked : unChecked, 
					          			fit: [10, 10],
					          			margin: [2, 0, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'MAT Disposal Fee by Supplier',
						          		fontSize: 8,
						          		width: '*'
				          			}
			          			]
				          	}
		    			],
		    			[
		    				{
				          		columns: [
				          			{
				          				image: isAffectedLotOkToUse == 1 ? checked : unChecked, 
					          			fit: [10, 10],
					          			margin: [2, 6, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'OK TO USE',
						          		fontSize: 8,
						          		margin: [0, 5, 0, 0],
						          		width: '*'
				          			}
			          			]
				          	},
				          	{
				          		
				          		columns: [
				          			{
				          				image: affectedLotInvoiceNumber == "MAT SORTING / REWORK (WITH CHARGE SHEET)" ? checked : unChecked, 
					          			fit: [10, 10],
					          			margin: [2, 6, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'MAT SORTING / REWORK (WITH CHARGE SHEET)',
						          		fontSize: 8,
						          		width: '*'
				          			}
			          			]
					          	
				          	},
				          	{
				          		columns: [
				          			{
				          				image: deffectiveMaterial == "REFUND / DEBIT" ? checked : unChecked, 
					          			fit: [10, 10],
					          			margin: [2, 6, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'REFUND / DEBIT',
						          		fontSize: 8,
						          		margin: [0, 5, 0, 0],
						          		width: '*'
				          			}
			          			]
				          	},
				          	{
				          		columns: [
				          			{
				          				image: ngMaterialDisposal == "RTV" ? checked : unChecked, 
					          			fit: [10, 10],
					          			margin: [2, 6, 0, 0],
					          			width: 10
				          			},
				          			{
				          				text: '',
				          				width: 7
				          			},
				          			{
				          				text: 'RTV',
						          		fontSize: 8,
						          		margin: [0, 5, 0, 0],
						          		width: '*'
				          			}
			          			]
				          	}
		    			],
		    			[
		    				'',
		    				'',
		    				{
		    					text: 'NG AMOUNT: $' + (ngAmount ? ngAmount : ''),
		    					fontSize: 8,
		    				},
		    				''
		    			]
		    		]
		    	},
		    	margin: [0, 0, 0, 2]
		    },
			//SIGNATORIES
			{
		    	table: {
		    		widths: ['*', '*', '*', '*', '*', '*', '*', '*'],
					body: isIncludeTechSuppMngrSuppier == 1 ?
						[
							[
								{	
									image: preparedBy == "Jane Dagli" ?
														qcLeadGirlSign_Jane
													:
														qcLeadGirlSign_sheryl,
									fit: [50, 50],
									margin: [2, 6, 0, 0]
								},		    				
								{		    					
									image: iqcSrSpvrApprove || approvedByIQCSrSupervisor ?
										approvedByIQCSrSupervisor == 'Rose de Leon'
										? iqcSrSpvrRoseSign
										: qcEngineerAljadeSign
									:
										noSignature, 
									fit: [50, 50],
									margin: [2, 6, 0, 0]
								},
								{		    					
									image: qcSectionHeadApprove || approvedByQCSectionHead ?
												qcSectionHeadLoidaSign
											:
												noSignature, 
									fit: [50, 50],
									margin: [2, 6, 0, 0]
								},
								{		    					
									image: qcAsstMngrApprove || approvedByQCAsstManager ?
												approvedByQCAsstManager == 'Cristy Bautista' 
												? qcAsstmngrCristySign
												: qcAsstmngrGeorgeSign
											:
												noSignature,
									fit: [50, 50],
									margin: [2, 6, 0, 0]
								},
								{		    					
									image:  techSuppMngrApprove || approvedByTechSuppMngr ?
												suppTechMngrFernanSign
											:
												noSignature,
									fit: [50, 50],
									margin: [2, 6, 0, 0]
								},
								{		    					
									image: asstFactMngrApprove || approvedByAsstFactMngr ?
												asstFactMngrSign
											:
												noSignature,
									fit: [50, 50],
									margin: [2, 6, 0, 0]
								},
								{		    					
									image: factMngrApprove || approvedByFactMngr ?
												approvedByFactMngr == "Yasushi Hizakae" ?
													presSignHizakae
												:
													factMngrSign
											:
												noSignature,
									fit: [50, 50],
									margin: [2, 6, 0, 0]
								},
								{		    					
									image: noSignature, 
									fit: [50, 50],
									margin: [2, 6, 0, 0]
								}
							],
							[
								{	
									text: preparedBy == "Sheryl Caballes" ?
												"S. Caballes"
											:
												"J. Dagli",
									fontSize: 8
								},
								{	
									text: approvedByIQCSrSupervisor == 'Rose de Leon'
									? 'Ms. R. de Leon'
									: 'Mr. A. Florida',
									fontSize: 8
								},
								{	
									text: 'Ms. L. Gabutin',
									fontSize: 8
								},
								{	
									text: approvedByQCAsstManager == 'Cristy Bautista'
									? 'Ms. C. Bautista'
									: 'Mr. G. Reyes',
									fontSize: 8
								},
								{	
									text: deffectRank == 'A' ? 'Mr. F. Gonzales' : '',
									fontSize: 8
								},
								{	
									text: deffectRank == 'A' 
									? 
										asstFactMngrApprove || approvedByAsstFactMngr
										? "Ms. G. Abogado"
										: ""
									: '',
									fontSize: 8
								},
								{	
									text: deffectRank == 'A' ?
												localStorage.getItem("position") == "President" ?
													approvedByFactMngr != null && approvedByFactMngr != '' ?
														approvedByFactMngr == "Yasushi Hizakae" ?
															"Mr. Y. Hizakae"
														:
															"Mr. D. Goto"
													:
														"Mr. Y. Hizakae"
												:
													approvedByFactMngr != null && approvedByFactMngr != '' ?
														approvedByFactMngr == "Yasushi Hizakae" ?
															"Mr. Y. Hizakae"
														:
															"Mr. D. Goto"
													:
														"Mr. D. Goto"
											:
												"",
									fontSize: 8
								},
								{
									text: ''
								}
							],
							[
								{	
									text: 'QC Leadgirl',
									fontSize: 7
								},
								{	
									text: approvedByIQCSrSupervisor == 'Rose de Leon'
									? 'IQC Supervisor'
									: 'QC Engineer',
									fontSize: 7
								},
								{	
									text: 'QC Section Head',
									fontSize: 7
								},
								{	
									text: 'QC Asst. Mngr.',
									fontSize: 7
								},
								{	
									text: deffectRank == 'A' ? 'Supp. Control Mngr.' : '',
									fontSize: 7
								},
								{	
									text: deffectRank == 'A' 
									? 
										asstFactMngrApprove || approvedByAsstFactMngr 
										? 'Asst. Fact. Mngr.'
										: ""
									: '',
									fontSize: 7
								},
								{	
									text: deffectRank == 'A' ?
												localStorage.getItem("position") == "President" ?
													approvedByFactMngr != null && approvedByFactMngr != '' ?
														approvedByFactMngr == "Yasushi Hizakae" ?
															"President"
														:
															"Factory Manager"
													:
														"President"
												:
													approvedByFactMngr != null && approvedByFactMngr != '' ?
														approvedByFactMngr == "Yasushi Hizakae" ?
															"President"
														:
															"Factory Manager"
													:
														"Factory Manager"
											:
												"",
									fontSize: 7
								},
								{	
									text: ''
								}
							],
							[
								{
									text: 'PREPARED BY',
									fontSize: 8,
									fillColor: '#C6E0B4',
									alignment: 'center'
								},
								{
									text: 'CHECKED BY',
									fontSize: 8,
									fillColor: '#C6E0B4',
									colSpan: 2,
									alignment: 'center'
								},
								'',
								{
									text: 'APPROVED BY',
									fontSize: 8,
									fillColor: '#C6E0B4',
									colSpan: 5,
									alignment: 'center'
								},
								'',
								'',
								'',
								''
							]
						]
					:
						[
							[
								{	
									image: preparedBy == "Jane Dagli" ?
														qcLeadGirlSign_Jane
													:
														qcLeadGirlSign_sheryl,
									fit: [50, 50],
									margin: [2, 6, 0, 0]
								},		    				
								{		    					
									image: iqcSrSpvrApprove || approvedByIQCSrSupervisor ?
										approvedByIQCSrSupervisor == 'Rose de Leon'
										? iqcSrSpvrRoseSign
										: qcEngineerAljadeSign
									:
										noSignature, 
									fit: [50, 50],
									margin: [2, 6, 0, 0]
								},
								{		    					
									image: qcSectionHeadApprove || approvedByQCSectionHead ?
												qcSectionHeadLoidaSign
											:
												noSignature, 
									fit: [50, 50],
									margin: [2, 6, 0, 0]
								},
								{		    					
									image: qcAsstMngrApprove || approvedByQCAsstManager ?
												approvedByQCAsstManager == 'Cristy Bautista' 
												? qcAsstmngrCristySign
												: qcAsstmngrGeorgeSign
											:
												noSignature,
									fit: [50, 50],
									margin: [2, 6, 0, 0]
								},
								{		    					
									image: asstFactMngrApprove || approvedByAsstFactMngr ?
												asstFactMngrSign
											:
												noSignature,
									fit: [50, 50],
									margin: [2, 6, 0, 0]
								},
								{		    					
									image: factMngrApprove || approvedByFactMngr ?
												approvedByFactMngr == "Yasushi Hizakae" ?
													presSignHizakae
												:
													factMngrSign
											:
												noSignature,
									fit: [50, 50],
									margin: [2, 6, 0, 0]
								},
								{		    					
									image: noSignature, 
									fit: [50, 50],
									margin: [2, 6, 0, 0]
								},
								{		    					
									image: noSignature, 
									fit: [50, 50],
									margin: [2, 6, 0, 0]
								}
							],
							[
								{	
									text: preparedBy == "Sheryl Caballes" ?
												"S. Caballes"
											:
												"J. Dagli",
									fontSize: 8
								},
								{	
									text: approvedByIQCSrSupervisor == 'Rose de Leon' ? 'Ms. R. de Leon' : 'Mr. A. Florida',
									fontSize: 8
								},
								{	
									text: 'Ms. L. Gabutin',
									fontSize: 8
								},
								{	
									text: approvedByQCAsstManager == 'Cristy Bautista'
									? 'Ms. C. Bautista'
									: 'Mr. G. Reyes',
									fontSize: 8
								},
								{	
									text: deffectRank == 'A'
									? 
										asstFactMngrApprove || approvedByAsstFactMngr
										? "Ms. G. Abogado"
										: ""
									: '',
									fontSize: 8
								},
								{	
									text: deffectRank == 'A' ?
												localStorage.getItem("position") == "President" ?
													approvedByFactMngr != null && approvedByFactMngr != '' ?
														approvedByFactMngr == "Yasushi Hizakae" ?
															"Mr. Y. Hizakae"
														:
															"Mr. D. Goto"
													:
														"Mr. Y. Hizakae"
												:
													approvedByFactMngr != null && approvedByFactMngr != '' ?
														approvedByFactMngr == "Yasushi Hizakae" ?
															"Mr. Y. Hizakae"
														:
															"Mr. D. Goto"
													:
														"Mr. D. Goto"
											:
												"",
									fontSize: 8
								},
								{
									text: ''
								},
								{
									text: ''
								}
							],
							[
								{	
									text: 'QC Leadgirl',
									fontSize: 7
								},
								{	
									text: approvedByIQCSrSupervisor == 'Rose de Leon' ? 'IQC Supervisor' : 'QC Engineer',
									fontSize: 7
								},
								{	
									text: 'QC Section Head',
									fontSize: 7
								},
								{	
									text: 'QC Asst. Mngr.',
									fontSize: 7
								},
								{	
									text: deffectRank == 'A' 
									?
										asstFactMngrApprove || approvedByAsstFactMngr
										? 'Asst. Fact. Mngr.'
										: ""
									: '',
									fontSize: 7
								},
								{	
									text: deffectRank == 'A' ?
												localStorage.getItem("position") == "President" ?
													approvedByFactMngr != null && approvedByFactMngr != '' ?
														approvedByFactMngr == "Yasushi Hizakae" ?
															"President"
														:
															"Factory Manager"
													:
														"President"
												:
													approvedByFactMngr != null && approvedByFactMngr != '' ?
														approvedByFactMngr == "Yasushi Hizakae" ?
															"President"
														:
															"Factory Manager"
													:
														"Factory Manager"
											:
												"",
									fontSize: 7
								},
								{	
									text: ''
								},
								{	
									text: ''
								}
							],
							[
								{
									text: 'PREPARED BY',
									fontSize: 8,
									fillColor: '#C6E0B4',
									alignment: 'center'
								},
								{
									text: 'CHECKED BY',
									fontSize: 8,
									fillColor: '#C6E0B4',
									colSpan: 2,
									alignment: 'center'
								},
								'',
								{
									text: 'APPROVED BY',
									fontSize: 8,
									fillColor: '#C6E0B4',
									colSpan: 5,
									alignment: 'center'
								},
								'',
								'',
								'',
								''
							]
						]
		    	},
		    	margin: [0, 0, 0, 2],
				// pageBreak: 'after'
		    },
			//SUPPLIER MATERIAL DISPOSITION
			{	
		    	table: {
		    		widths: ['*', '*', '*', '*', '*', '*', '*', '*'],
		    		body: [
		    			[
			    			{
			    				text: 'SUPPLIER PORTION - MATERIAL DISPOSITION',
			    				fontSize: 9,
			    				bold: true,
			    				alignment: 'center',
			    				fillColor: '#B4C6E7',
			    				margin: [0, 2, 0, 2],
			    				colSpan: 8,

			    			},
			    			'',
			    			'',
			    			'',
			    			'',
			    			'',
			    			'',
			    			'',
		    			],
		    			[
			    			{
			    				image: await getBase64ImageFromURL(process.env.REACT_APP_SQAR_API + '/api/image/' + imageName),
			    				width: 555,
								height: 160,
			    				alignment: 'center',
			    				colSpan: 8
			    			},			    			
			    			'',
			    			'',
			    			'',
			    			'',
			    			'',
			    			'',
			    			'',
		    			]   			    			
		    		],	    		
		    	},
		    },
			//SIGNATORIES
		    {
		    	table: {
		    		widths: ['*', '*', '*', '*', '*', '*', '*', '*'],
		    		body: [
		    			[
		    				{
			    				text: "MAT'S APPROVAL",
			    				fontSize: 9,
			    				bold: true,
			    				alignment: 'center',
			    				fillColor: '#B4C6E7',
			    				margin: [0, 2, 0, 2],
			    				colSpan: 8,

			    			},
		    				'',
		    				'',
		    				'',
		    				'',
		    				'',
		    				'',
		    				'',
		    			],
		    			[
		    				{
			    				text: "PREPARED",
			    				fontSize: 8,
			    				alignment: 'center',
			    				fillColor: '#C6E0B4'
			    			},
		    				{
			    				text: "CHECKED",
			    				fontSize: 8,
			    				alignment: 'center',
			    				fillColor: '#C6E0B4'
			    			},
		    				{
			    				text: "APPROVED",
			    				fontSize: 8,
			    				alignment: 'center',
			    				fillColor: '#C6E0B4'
			    			},
		    				{
			    				text: "",
			    				fontSize: 8,
			    				alignment: 'center',
			    				fillColor: '#C6E0B4'
			    			},
		    				{
			    				text: "",
			    				fontSize: 8,
			    				alignment: 'center',
			    				fillColor: '#C6E0B4'
			    			},
		    				{
			    				text: "",
			    				fontSize: 8,
			    				alignment: 'center',
			    				fillColor: '#C6E0B4'
			    			},
		    				{
			    				text: "",
			    				fontSize: 8,
			    				alignment: 'center',
			    				fillColor: '#C6E0B4'
			    			},
		    				{
			    				text: "",
			    				fontSize: 8,
			    				alignment: 'center',
			    				fillColor: '#C6E0B4'
			    			},
		    			],
		    			[
		    				{
		    					image: (supMatDispoApprovedBy_IQCSrSupervisor || supMatDispo_isApprovedBy_IQCSrSupervisor) ?
									supMatDispoApprovedBy_IQCSrSupervisor == 'Rose de Leon'
									? iqcSrSpvrRoseSign
									: qcEngineerAljadeSign
								:
									noSignature,
								margin: [9, 0, 0, 0],
		    					fit: [40, 40],

		    				},
		    				{
		    					image: (supMatDispoApprovedBy_QCSectionHead || supMatDispo_isApprovedBy_QCSectionHead) ?
		    							qcSectionHeadLoidaSign
		    						:
		    							noSignature,
								margin: [9, 0, 0, 0],
		    					fit: [40, 40],

		    				},
		    				{
		    					image: (supMatDispoApprovedBy_QCAsstManager || supMatDispo_isApprovedBy_QCAsstManager) ?
										supMatDispoApprovedBy_QCAsstManager == 'Cristy Bautista'
										? qcAsstmngrCristySign
										: qcAsstmngrGeorgeSign
		    						:
		    							noSignature,
								margin: [9, 0, 0, 0],
		    					fit: [40, 40],

		    				},
		    				'',
		    				'',
		    				'',
		    				'',
		    				'',
		    			],
		    			[
		    				{
		    					text: supMatDispoApprovedBy_IQCSrSupervisor == 'Rose de Leon' ? 'IQC Supvr.' : 'QC Engr.',
		    					fontSize: 8,
		    					alignment: 'center'
		    				},
		    				{
		    					text: 'QC Section Head',
		    					fontSize: 8,
		    					alignment: 'center'
		    				},
		    				{
		    					text: 'QC Asst. Mngr.',
		    					fontSize: 8,
		    					alignment: 'center'
		    				},
		    				'',
		    				'',
		    				'',
		    				'',
		    				''
		    			]
		    		]
		    	},
		    	margin: [0, 2, 0, 2],
				pageBreak: 'after'
		    },
			//ILLUSTRATIONS STARTS HERE
		    illustrationsData.length > 0 ?
		    	illustrationsPDFMakeScript
		    :
		    	'',
			//ROOT CAUSE AND CORRECTIVE ACTION
		    {
			  columns: [
		        {
	          		image: maLogoJpeg, 
	          		fit: [50, 50],
	          		margin: [5, 0, 0, 0]
	          	}, 
		        {
	          		image: assuredlogo, 
	          		fit: [50, 50], alignment: 'right'
	          	}
		      ]
			},			
		    {
		    	table: {
		        widths: ['*', 100, 75],
		        body: [
		          [ 
		          	{
		          		text: 'SUPPLIER QUALITY ABNORMALITY REPORT (SQAR)', 
		          		bold: true, 
		          		fontSize: 10,
		          		fillColor: '#9966FF', 
		          		alignment: 'center',
		          		margin: [0, 2, 0, 2]
		          	}, 
		          	{
		          		text: 'CONTROL NO.',
		          		fontSize: 10, 
		          		fillColor: '#F8CBAD', 
		          		alignment: 'right',
		          		margin: [0, 2, 0, 2]
		          	}, 
		          	{
		          		text: sqarControlNo,
		          		fontSize: 10,
		          		alignment: 'center',
		          		margin: [0, 2, 0, 2]
		          	}
		          ]
		        ]
		      },		      
		      margin: [0, 5, 0, 2]
		    },
		    {
		    	table: {
		    		widths: ['*'],
		    		body: rccaPDFMakeTableRowScript		    	
		    	}
		    },
		    {
	    		table: {
	    			widths: ['*'],
	    			body: [
	    				[
	    					{
	    						table: {
	    							widths: ['*', 60],
	    							body: [
	    								[
	    									{
				    							text: 'MAT IQA COMMENTS/REQUEST:',
					    						fontSize: 9,
								    			bold: true,						    			
							          			alignment: 'center',
							          			fillColor: '#B4C6E7',
							          			colSpan: 2,
							          			margin: [0, 2, 0, 2],
							          			border: [false, false, false, true]
				    						},
				    						''
	    								],
	    								[
	    									{
	    										text: matCommentsRequest,
	    										fontSize: 8,
	    										rowSpan: 2,
	    										border: [false, true, true, false]
	    									},
	    									{
	    										text: 'DISPOSITION',
	    										fontSize: 8,
	    										bold: true,
	    										alignment: 'center',
	    										fillColor: '#B4C6E7',
	    										border: [true, true, false, true]
	    									}
	    								],
	    								[
	    									'',
	    									{
	    										stack: [
	    											{
	    												columns: [
		    												{
		    													image: isRccaDispositionApproved == 1 ? checked : unChecked,
		    													fit: [10, 10],
		    													width: 15
		    												},
		    												{
		    													text: 'Approve',
		    													fontSize: 8

		    												}
	    												],
	    												margin: [0, 0, 0, 2]
	    											},
	    											{
	    												columns: [
		    												{
		    													image: isRccaDispositionApproved == 0 ? checked : unChecked,
		    													fit: [10, 10],
		    													width: 15
		    												},
		    												{
		    													text: 'Disapprove',
		    													fontSize: 8
		    												}
	    												]
	    											}
	    										],
	    										border: [true, true, false, false]
	    									}
	    								]
	    							]
	    						},
	    						layout: {
							    	paddingLeft: function(i, node) { return 5; },
							    	paddingRight: function(i, node) { return 5; },
							    	paddingTop: function(i, node) { return 3; },
							    	paddingBottom: function(i, node) { return 3; }
								}
	    					}	    					
	    				],
	    				[
	    					{
	    						table: {
	    							// width: [50, 50, 50, 55, 40, 60, 40, 60, 40, 60, 60],
	    							width: ['*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*'],
	    							body: [
	    								[
		    								{
		    									text: 'CONFIRMATION',
		    									fontSize: 8,
		    									bold: true,
		    									alignment: 'center',
		    									fillColor: '#B4C6E7',
		    									border: [false, false, true, false],
		    									colSpan: 4
		    								},
		    								'',
		    								'',
		    								'',		    								
		    								{
		    									text: 'EFFECTIVENESS OF CORRECTIVE ACTION',
		    									fontSize: 8,
		    									bold: true,
		    									alignment: 'center',
		    									fillColor: '#B4C6E7',
		    									border: [true, false, false, false],
		    									colSpan: 7
		    								},
											'',
		    								'',
		    								'',
		    								'',
		    								'',
		    								'',
	    								],
	    								[
	    									{
	    										text: 'PREPARED',
	    										fontSize: 8,
	    										bold: true,
	    										alignment: 'center',
	    										margin: [0, 4, 0, 0],
	    										border: [false, true, true, false],
	    										fillColor: '#C6E0B4'
	    									},
	    									{
	    										text: 'CHECKED',
	    										fontSize: 8,
	    										bold: true,
	    										alignment: 'center',
	    										margin: [0, 4, 0, 0],
	    										border: [false, true, true, false],
	    										fillColor: '#C6E0B4'
	    									},
	    									{
	    										text: 'APPROVED',
	    										fontSize: 8,
	    										bold: true,
	    										alignment: 'center',
	    										margin: [0, 4, 0, 0],
	    										border: [false, true, true, false],
	    										fillColor: '#C6E0B4'
	    									},
	    									{
	    										text: 'RESPONSE DATE',
	    										fontSize: 8,
	    										bold: true,
	    										alignment: 'center',
	    										fillColor: '#F8CBAD',
	    										border: [false, true, true, false],
	    									},
	    									{
	    										text: '1st Delivery',
	    										fontSize: 8,
	    										bold: true,
	    										fillColor: '#FFF2CC',
	    										alignment: 'center',
	    										margin: [0, 4, 0, 0],
	    										border: [false, true, true, false],	    										
	    										colSpan: 2
	    									},
	    									'',
	    									{
	    										text: '2st Delivery',
	    										fontSize: 8,
	    										bold: true,
	    										fillColor: '#FFF2CC',
	    										alignment: 'center',
	    										margin: [0, 4, 0, 0],
	    										border: [false, true, true, false],
	    										colSpan: 2
	    									},
	    									'',
	    									{
	    										text: '3st Delivery',
	    										fontSize: 8,
	    										bold: true,
	    										fillColor: '#FFF2CC',
	    										alignment: 'center',
	    										margin: [0, 4, 0, 0],
	    										border: [false, true, true, false],
	    										colSpan: 2
	    									},
	    									'',
	    									{
	    										text: 'SQAR STATUS',
	    										fontSize: 8,
	    										bold: true,
	    										alignment: 'center',
	    										margin: [0, 4, 0, 0],
	    										fillColor: '#F8CBAD',
	    										border: [false, true, false, false],
	    									}
	    								],
	    								[
	    									{
	    										image: (supRCCAApprovedBy_IQCSrSupervisor || supRCCA_isApprovedBy_IQCSrSupervisor) ? 
													supRCCAApprovedBy_IQCSrSupervisor == 'Rose de Leon'
													? iqcSrSpvrRoseSign
													: qcEngineerAljadeSign
	    										:
	    											noSignature,
	    										fit: [40, 40],
	    										border: [false, true, true, false],
	    										rowSpan: 2
	    									},
	    									{
	    										image: (supRCCAApprovedBy_QCSectionHead || supRCCA_isApprovedBy_QCSectionHead) ?
	    											qcSectionHeadLoidaSign
	    										:
	    											noSignature,
	    										fit: [40, 40],
	    										border: [false, true, true, false],
	    										rowSpan: 2
	    									},
	    									{
	    										image: (supRCCAApprovedBy_QCAsstManager || supRCCA_isApprovedBy_QCAsstManager) ?
													supRCCAApprovedBy_QCAsstManager == 'Cristy Bautista'
	    											? qcAsstmngrCristySign
	    											: qcAsstmngrGeorgeSign
	    										:
	    											noSignature,
	    										fit: [40, 40],
	    										border: [false, true, true, false],
	    										rowSpan: 2
	    									},
	    									{
	    										text: rccaResponseDate ? 
							        				rccaResponseDate.slice(5, 10) + '-' + rccaResponseDate.slice(0, 4) 
							        			: 
							        				'',
							        			fontSize: 8,
							        			alignment: 'center',
							        			rowSpan: 2,
							        			border: [false, true, true, false],
	    									},
	    									{
	    										text: firstDeliveryCheck.date ?
	        										firstDeliveryCheck.date.slice(5, 10) + '-' + firstDeliveryCheck.date.slice(0, 4)
							        			: 
							        				'',
							        			fontSize: 8,
							        			alignment: 'center',
							        			border: [false, true, true, false],
	    									},
	    									{
	    										text: firstDeliveryCheck.quantity ? 
	        										  	firstDeliveryCheck.quantity
	        										:
	        											'',
	        									fontSize: 8,
							        			alignment: 'center',
							        			border: [false, true, true, false],
	    									},
	    									{
	    										text: secondDeliveryCheck.date ?
	        										secondDeliveryCheck.date.slice(5, 10) + '-' + secondDeliveryCheck.date.slice(0, 4)
							        			: 
							        				'',
							        			fontSize: 8,
							        			alignment: 'center',
							        			border: [false, true, true, false],
	    									},
	    									{
	    										text: secondDeliveryCheck.quantity ? 
	        											secondDeliveryCheck.quantity
	        										:
	        											'',
	        									fontSize: 8,
							        			alignment: 'center',
							        			border: [false, true, true, false],
	    									},
	    									{
	    										text: thirdDeliveryCheck.date ?
	        										thirdDeliveryCheck.date.slice(5, 10) + '-' + thirdDeliveryCheck.date.slice(0, 4)
							        			: 
							        				'',
							        			fontSize: 8,
							        			alignment: 'center',
							        			border: [false, true, true, false],
	    									},
	    									{
	    										text: thirdDeliveryCheck.quantity ? 
	        											thirdDeliveryCheck.quantity
	        										:
	        											'',
	        									fontSize: 8,
							        			alignment: 'center',
							        			border: [false, true, true, false],
	    									},
	    									{
	    										image: sqarStatus == 14 ? closedIcon : noSignature2,
												fit:[50,50],
												margin: [0, 7, 0, 0],
							        			alignment: 'center',
							        			rowSpan: 2,
							        			border: [false, true, false, false],
	    									},
	    								],
	    								[
	    									'',
	    									'',
	    									'',
	    									'',
	    									{
	    										text: 'CHECKED BY:',
	    										fontSize: 7,
							        			alignment: 'left',
							        			fillColor: '#C6E0B4',
							        			border: [true, true, true, false],
	    									},
	    									{
	    										image: firstDeliveryCheck.isOk == true ?
													firstDeliveryCheck.approver == 'Rose de Leon'
													? iqcSrSpvrRoseSign2
													: qcEngineerAljadeSign
												:
													noSignature2,
	    										fit: [35, 30],
	    										border: [true, true, true, false]
	    									},
	    									{
	    										text: 'CHECKED BY:',
	    										fontSize: 7,
							        			alignment: 'left',
							        			fillColor: '#C6E0B4',
							        			border: [true, true, true, false],
	    									},	    									
	    									{
	    										image: secondDeliveryCheck.isOk == true ?
													secondDeliveryCheck.approver == 'Rose de Leon'
													? iqcSrSpvrRoseSign2
													: qcEngineerAljadeSign
												:
													noSignature2,
	    										fit: [35, 30],
	    										border: [true, true, true, false]
	    									},
	    									{
	    										text: 'CHECKED BY:',
	    										fontSize: 7,
							        			alignment: 'left',
							        			fillColor: '#C6E0B4',
							        			border: [true, true, true, false],
	    									},	    									
	    									{
	    										image: thirdDeliveryCheck.isOk == true ?
													thirdDeliveryCheck.approver == 'Rose de Leon'
													? iqcSrSpvrRoseSign2
													: qcEngineerAljadeSign
												:
													noSignature2,
	    										fit: [35, 30],
	    										border: [true, true, true, false]
	    									},
	    									'',
	    								]
	    							]
	    						}
	    					}
	    				]	    				
	    			],
					dontBreakRows: true,
	    		},
	    		layout: {
			    	paddingLeft: function(i, node) { return 0; },
			    	paddingRight: function(i, node) { return 0; },
			    	paddingTop: function(i, node) { return 0; },
			    	paddingBottom: function(i, node) { return 0; }
				},
	    		margin: [0, 2, 0, 0]
	    	},
		  ]
		};
		pdfMake.createPdf(docDefinition).open();
	}

	const updateRccaReceiveDate = () => {
		console.log(rccaResponseDate)
		Swal.fire({
			title: 'Select a date',
			html: '<input style="text-align: center; width: 50%; height: 40px;" type="date" id="rcca-swal-date" />',
			confirmButtonColor: '#3C5393',
			showCancelButton: true,
		})
		.then((userResponse) => {
			let date = document.getElementById('rcca-swal-date').value
			if(date && userResponse.isConfirmed){
				fetch(`${process.env.REACT_APP_SQAR_API}/api/sqar/update-rcca-response-date`, {
					method: 'POST',
					headers: {'Content-Type': 'application/json'},
					body: JSON.stringify({
						sqarControlNo: sqarControlNo,
						rccaDateReceived: date
					})
				})
				.then(response => response.json())
				.then(result => {
					setRccaResponseDate(date + "00:00:00.000")
					if(result == 1){
						Swal.fire({
							title: 'Success',
							text: 'RCCA receive date successfully updated.',
							icon: 'success',
						})
					}else{
						Swal.fire({
							title: 'Error',
							text: 'Something went wrong. Updating RCCA receive date failed.',
							icon: 'error',
						})
					}
				})		
			}
		})
	}

	const approveRccaDisposition = () => {
		fetch(`${process.env.REACT_APP_SQAR_API}/api/sqar/approve-rcca-disposition`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				sqarControlNo: sqarControlNo
			})
		})
		.then((response) => response.json())
		.then((result) => {
			if(result == 1){
				Swal.fire({
					title: 'Success',
					icon: 'success',
					confirmButtonColor: '#3C5393',
					text: 'Disposition successfully approved'
				})
				setIsRccaDispositionApproved(true)
			}else{
				Swal.fire({
					title: 'Error',
					icon: 'error',
					confirmButtonColor: '#3C5393',
					text: 'Something went wrong.'
				})
			}
		})
	}

	const disapproveRccaDisposition = () => {
		fetch(`${process.env.REACT_APP_SQAR_API}/api/sqar/disapprove-rcca-disposition`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				sqarControlNo: sqarControlNo
			})
		})
		.then((response) => response.json())
		.then((result) => {
			if(result == 1){
				Swal.fire({
					title: 'Success',
					icon: 'success',
					confirmButtonColor: '#3C5393',
					text: 'Disposition successfully disapproved'
				})
				setIsRccaDispositionApproved(false)
			}else{
				Swal.fire({
					title: 'Error',
					icon: 'error',
					confirmButtonColor: '#3C5393',
					text: 'Something went wrong.'
				})
			}
		})
	}

	const saveMatRCCAComments = () => {
		fetch(`${process.env.REACT_APP_SQAR_API}/api/sqar/save-mat-rcca-comments`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				matRccaComments: matComments,
				sqarControlNo: sqarControlNo
			})
		})
		.then((response) => response.json())
		.then((result) => {
			if(result == 1){
				Swal.fire({
					title: 'Success',
					icon: 'success',
					confirmButtonColor: "#3C5393",
					text: "Comments successfully saved."
				})
				setMatCommentsRequest(matComments)
			}else{
				Swal.fire({
					title: 'Error',
					icon: 'error',
					confirmButtonColor: "#3C5393",
					text: "Something went wrong."
				})
			}
		})
	}

	const searchForSQARProcess = (event) => {
		event.preventDefault()
		setSearchKeyword(event.target.value)
		constructUI(true)
	}

	const nextPage = () => {
		setCurrentPage(currentPage + 1)
	}

	const prevPage = () => {
		setCurrentPage(currentPage - 1)
	}

	const constructUI = (isSearch) => {
		let searchedItems = [];
		let sqarData2 = sqarData
		let newSearchKeyword = document.getElementById('searchBox').value

		if(isSearch && newSearchKeyword != ""){
			for (let i = 0; i < sqarData.length; i++){
				if((sqarData[i].SQAR_Invoice_DR || '').toLowerCase().includes(newSearchKeyword.toLowerCase()) 
					|| (sqarData[i].SQAR_Control_Number || '').toLowerCase().includes(newSearchKeyword.toLowerCase())
					|| (sqarData[i].SQAR_Supplier_Name || '').toLowerCase().includes(newSearchKeyword.toLowerCase())
					|| (sqarData[i].SQAR_Part_Number || '').toLowerCase().includes(newSearchKeyword.toLowerCase())
					|| (sqarData[i].SQAR_Part_Name || '').toLowerCase().includes(newSearchKeyword.toLowerCase())
					|| (sqarData[i].SQAR_Maker_Name || '').toLowerCase().includes(newSearchKeyword.toLowerCase())
					|| (sqarData[i].SQAR_Model || '').toLowerCase().includes(newSearchKeyword.toLowerCase())
					|| (sqarData[i].SQAR_Status || '').toLowerCase().includes(newSearchKeyword.toLowerCase())
					|| (sqarData[i].SQAR_MatCodeBoxSeqId || '').toLowerCase().includes(newSearchKeyword.toLowerCase())
					|| (sqarData[i].SQAR_General_Status || '').toLowerCase().includes(newSearchKeyword.toLowerCase())
					|| (sqarData[i].SQAR_Status || '').toLowerCase().includes(newSearchKeyword.toLowerCase())
					|| (sqarData[i].SQAR_PO_Number || '').toLowerCase().includes(newSearchKeyword.toLowerCase())){
					searchedItems.push(sqarData[i])
				}
			}
			sqarData2 = searchedItems
		}

		//filter data using the substatus
		// console.log(selectedSubStatus)

		currentPageItems = []
		//SET 100 ITEMS ON TABLE
		if(sqarData2){
			//If the current page reach the last page that does not have 100 items
			if(currentPage * 100 > sqarData2.length){
				for(let i = currentPage * 100 - 100; i < (currentPage * 100) - (currentPage * 100 - sqarData2.length) ; i++){
					if(i == (currentPage * 100) - (currentPage * 100 - sqarData2.length)){
						break;
					}
					currentPageItems.push(sqarData2[i])
				}
			}else{
				for(let i = currentPage * 100 - 100; i < currentPage * 100 ; i++){
					if(i == currentPage * 100){
						break;
					}
					currentPageItems.push(sqarData2[i])
				}
			}			
		}
		setSearchedItemsLength(currentPageItems.length)
		let rowNo = 0
		// let status;
		setSqars(									
			currentPageItems.map(item => {
				rowNo++
				return(
					(viewTable) ?
						<tr key={item.ROW_NUMBER}>
							<td>{rowNo}</td>
							<td>
								<OverlayTrigger
							      placement="right"
							      delay={{ show: 250, hide: 400 }}
							      overlay={renderTooltipOpenSQAR}
							    >
							     <Button
								 className="process-sqar-button" 
							     onClick={() => {
									getData(item)
									console.log(item)
								}}
							     >
							     	<i className="fa-solid fa-eye"></i>
							     </Button>
							    </OverlayTrigger>									
							</td>
							<td style={{textAlign: 'center'}}>
								<Badge
								style={{fontSize: 13, marginTop: 5}}														
								bg={item.SQAR_General_Status == "CLOSED" ? "danger" : item.SQAR_General_Status == "CANCELLED" ? "secondary" : "success"}
								>
									{item.SQAR_General_Status == "CLOSED" ? "Closed" : item.SQAR_General_Status == "CANCELLED" ? "Cancelled" : "Open"}
								</Badge>
							</td>							
							<td><b>{item.SQAR_Control_Number}</b></td>
							<td>{item.SQAR_Prepared_By}</td>
							<td>{convertDate(item.SQAR_Prepared_Date.slice(0, 10))}</td>
							<td>{item.SQAR_PO_Number}</td>
							<td>{item.SQAR_Invoice_DR}</td>
							<td>{item.SQAR_Part_Name}</td>
							<td>{item.SQAR_Part_Number}</td>
							<td>{item.SQAR_Supplier_Name}</td>
							<td>{item.SQAR_Maker_Name}</td>
							<td>{item.SQAR_Rejected_Rate.slice(0, 4)}</td>
							<td>{convertDate(item.SQAR_Received_Date.slice(0, 10))}</td>
							<td>{item.SQAR_MatCodeBoxSeqId}</td>
							<td>{item.SQAR_Status}</td>
						</tr>
					:
						<Col xs={12} sm={6} md={4} lg={4} xl={3} className="mt-3 rounded" key={item.ROW_NUMBER}>
							<Card className="h-100 border shadow-sm">
							  <Card.Header className="py-2">
							  	<div style={{color: 'white', textAlign: 'center'}}><b>{item.SQAR_Control_Number}</b></div>							  	
							  </Card.Header>
						      <Card.Body>
						      	<div><b>Sub Status:</b> &nbsp; {item.SQAR_Status}</div>
						      	<div><b>Prepared By:</b> &nbsp; {item.SQAR_Prepared_By}</div>
						      	<div><b>Date Prepared:</b> &nbsp; {convertDate(item.SQAR_Prepared_Date.slice(0, 10))}</div>
								<div><b>PO Number:</b>  &nbsp; {item.SQAR_PO_Number}</div>
								<div><b>Invoice No:</b>  &nbsp; {item.SQAR_Invoice_DR}</div>
								<div><b>Part Name:</b>  &nbsp; {item.SQAR_Part_Name}</div>
								<div><b>Part No:</b>  &nbsp; {item.SQAR_Part_Number}</div>
								<div><b>Supplier Name:</b>  &nbsp; {item.SQAR_Supplier_Name}</div>
								<div><b>Maker Name:</b>  &nbsp; {item.SQAR_Maker_Name}</div>
								<div><b>Reject Rate:</b>  &nbsp; {item.SQAR_Rejected_Rate.slice(0, 4)}</div>
								<div><b>Date Received:</b> &nbsp; {convertDate(item.SQAR_Received_Date.slice(0, 10))}</div>
								<div><b>MATCodeBox:</b> &nbsp; {item.SQAR_MatCodeBoxSeqId}</div>	        
						      </Card.Body>
						      <Card.Footer className="card-footer py-2">				
								<Badge
								style={{fontSize: 15, marginTop: 5}}														
								bg={item.SQAR_General_Status == "CLOSED" ? "danger" : item.SQAR_General_Status == "CANCELLED" ? "secondary" : "success"}
								>
									{item.SQAR_General_Status == "CLOSED" ? "Closed" : item.SQAR_General_Status == "CANCELLED" ? "Cancelled" : "Open"}
								</Badge>	
								<Button className="process-sqar-button align-right"
										onClick={() => getData(item)}>
								<i className="fa-solid fa-eye"></i> View
								</Button>
						      </Card.Footer>
						    </Card>
						</Col>
				)
			})
		)
		setIsLoading(false)		
	}

	useEffect(() => {
		setTimeout(() => {
			constructUI(false)
		})	
	}, [sqarWaiter, sqarData, currentPage, viewTable])


	//expose function to parent
	useImperativeHandle(ref, () => {
		return { 
			getData
		}
	})

	return(
		<>
			{
				isUploadingSupportDocs ?
					<UploadSpinner/>
				:
					<></>
			}
			<Modal
		        show={showSqarForm}
		        onHide={handleClose}
		        size="xl"
		        aria-labelledby="example-custom-modal-styling-title"
		      >
		        <Modal.Header closeButton>
		        </Modal.Header>
		        <Modal.Body>
					{	
						//Modal Pagination				
						!dateSent ? 
							<div className="prevNextButton">
									<Button	
									disabled
									className="ma-blue-button align-right mb-2">
										<i className="fa-solid fa-arrow-right"></i>
									</Button>				        		
								<div className="align-right rounded ma-blue-background mx-1"
									style={{height: 38, width: 38, textAlign: 'center', verticalAlign: 'middle', fontSize: 20, color: 'white'}}
									>
									<div className='mt-1'>{modalPage}</div>
								</div>
									<Button
									disabled
									className="ma-blue-button align-right mb-2">
										<i className="fa-solid fa-arrow-left"></i>
									</Button>		        		    		
							</div>
						:
							<div className="prevNextButton">
								{modalPage == 1 ?
									<Button
									className="ma-blue-button align-right mb-2" 
									onClick={()=>modalPageSwitch(1)}>
										<i className="fa-solid fa-arrow-right"></i>
									</Button>
								:
									modalPage == 2 ?
										<Button
										className="ma-blue-button align-right mb-2" 
										onClick={()=>modalPageSwitch(1)}>
											<i className="fa-solid fa-arrow-right"></i>
										</Button>
									:
										modalPage == 3 ?
											<Button							        		
											className="ma-blue-button align-right mb-2"
											onClick	={() => modalPageSwitch(1)}>
												<i className="fa-solid fa-arrow-right"></i>
											</Button>
										:
											<Button						        		
											className="ma-blue-button align-right mb-2"
											disabled>
												<i className="fa-solid fa-arrow-right"></i>
											</Button>
								}
								<div className="align-right rounded ma-blue-background mx-1"
									style={{height: 38, width: 38, textAlign: 'center', verticalAlign: 'middle', fontSize: 20, color: 'white'}}
									>
									<div className='mt-1'>{modalPage}</div>
								</div>
								{modalPage == 2 ?
									<Button
									className="ma-blue-button align-right mb-2" 
									onClick={()=>modalPageSwitch(-1)}>
										<i className="fa-solid fa-arrow-left"></i>
									</Button>
								:
									modalPage == 3 ?
										<Button
										className="ma-blue-button align-right mb-2" 
										onClick={()=>modalPageSwitch(-1)}>
											<i className="fa-solid fa-arrow-left"></i>
										</Button>
									:
										modalPage == 4 ?
											<Button
											className="ma-blue-button align-right mb-2" 
											onClick={()=>modalPageSwitch(-1)}>
												<i className="fa-solid fa-arrow-left"></i>
											</Button>
										:
											<Button
											disabled
											className="ma-blue-button align-right mb-2">
												<i className="fa-solid fa-arrow-left"></i>
											</Button>
								}		        		    		
							</div>
					}		        		 		        	
		        	<div id="sqarFirstPage" style={{display: modalPage == 1 ? 'block' : 'none'}}>
						{/* FIRST PAGE HEADER */}
			        	<Table bordered>
			        		<tbody>
			        			<tr>
			        				<td colSpan="6">
										<img src={maLogo} width="100" height="100" className="img-fluid"/>
										<span style={{position: 'absolute', top: 116, marginLeft: 10}}>QR-QA-121004R14</span>
			        					<img src={assuredlogo} width="100" height="100" className="img-fluid align-right"/>
			        				</td>
			        			</tr>
			        			<tr>
			        				<td colSpan="4" style={{backgroundColor: '#9966FF', fontSize: 25}}><center><b className="text-white">SUPPLIER QUALITY ABNORMALITY REPORT (SQAR)</b></center></td>		        				
			        				<td style={{backgroundColor: '#F8CBAD', textAlign: 'right', verticalAlign: 'middle'}}><b>CONTROL NO.</b></td>
			        				<td 
			        					style={{verticalAlign: 'middle', textAlign: 'center'}}>	
			        					{sqarControlNo}       				
			        				</td>      					        				
			        			</tr>
			        		</tbody>
			        	</Table>

						{/* SQAR DATA */}
			        	<Table bordered>
			        		<tbody>		        			
			        			<tr>
			        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>SUPPLIER NAME</b></td>
			        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>No. of Issued<br/>SQAR (Month)</b></td>		        				
			        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>PART NAME</b></td>
			        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>PART NUMBER</b></td>
			        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>DATE SENT</b></td>
			        			</tr>
			        			<tr>
			        				<td>
			        					<center>{supplier}</center>
			        				</td>
			        				<td>
			        					<center>{noIssuedSqarMonth}</center>
			        				</td>
			        				<td>
			        					<center>{partName}</center>
			        				</td>
			        				<td>
			        					<center>{partNo}</center>
			        				</td>
									{/* {
										dateSent ?
											<td id='dateSent' style={{textAlign: 'center', verticalAlign: 'middle'}}>{dateSent ? convertDate(dateSent) : ''}</td>
										:
											<td>
												{
													(sqarStatus > 1) ?
														<input style={{textAlign: 'center', width: '100%', height: '100%', border: 'none'}} type="date" onChange={(e) => console.log(convertDate(e.target.value))}></input>
													:
														""
												}
											</td>
									} */}
			        				<td id='dateSent' style={{textAlign: 'center', verticalAlign: 'middle'}}>{dateSent ? convertDate(dateSent) : ''}</td>									
			        			</tr>
			        			<tr>
			        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>MAKER NAME</b></td>
			        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>With SQAR<br/>Response (Month)</b></td>
			        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>AFFECTED QUANTITY</b></td>
			        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>APPLICABLE MODEL</b></td>
			        				<td style={{backgroundColor: 'rgb(248, 203, 173)', textAlign: 'center', verticalAlign: 'middle'}}><b>RESPONSE DUE DATE <br/> (MATERIAL DISPOSITION)</b></td>
			        			</tr>
			        			<tr>
			        				<td><center>{makerName}</center></td>
			        				<td><center>{withSqarResponseMonth}</center></td>
			        				<td><center>{affectedQuantity}</center></td>
			        				<td><center>{applicableModel}</center></td>
			        				<td id='supMatDispoDueDate' style={{textAlign: 'center', verticalAlign: 'middle'}}>{responseDueDateMD ? convertDate(responseDueDateMD) : ''}</td>
			        			</tr>
			        			<tr>
			        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>ATTENTION TO</b></td>
			        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>DEFECT <br/>OCCURRENCE</b></td>
			        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>INPUT QUANTITY</b></td>
			        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>RECEIVED DATE</b></td>
			        				<td style={{backgroundColor: 'rgb(248, 203, 173)', textAlign: 'center', verticalAlign: 'middle'}}><b>RESPONSE DUE DATE<br/>(RCCA)</b></td>
			        			</tr>
			        			<tr>
			        				<td><center>{contactPerson}</center></td>
			        				<td><center>{defectOccurence}</center></td>
			        				<td><center>{inputQuantity}</center></td>
			        				<td><center>{(dateReceived) ? convertDate(dateReceived) : dateReceived}</center></td>
			        				<td id='responseDueDateRCAA' style={{textAlign: 'center', verticalAlign: 'middle'}}>{responseDueDateRCAA ? convertDate(responseDueDateRCAA) : ''}</td>
			        			</tr>
			        			<tr>
			        				<td style={{backgroundColor: 'rgb(248, 203, 173)', textAlign: 'center'}}><b>DEFECT(S)</b></td>
			        				<td style={{backgroundColor: 'rgb(248, 203, 173)', textAlign: 'center'}}><b>REJECT QTY</b></td>
			        				<td style={{backgroundColor: 'rgb(248, 203, 173)', textAlign: 'center'}}><b>SAMPLE SIZE</b></td>
			        				<td style={{backgroundColor: 'rgb(248, 203, 173)', textAlign: 'center'}}><b>INVOICE / D.R.</b></td>
			        				<td style={{backgroundColor: 'rgb(248, 203, 173)', textAlign: 'center'}}><b>LOT NO(S)</b></td>
			        			</tr>
			        			<tr>
			        				<td id="defectOne" style={{verticalAlign: 'middle', textAlign: 'center'}}></td>
			        				<td id="rejectQty1" style={{verticalAlign: 'middle', textAlign: 'center'}}></td>
			        				<td style={{textAlign: 'center', verticalAlign: 'middle'}}>
			        					{sampleSize}
			        				</td>
			        				<td style={{textAlign: 'center', verticalAlign: 'middle'}}>
			        					{invoice}
			        				</td>
			        				<td rowSpan="3" style={{width: 250, textAlign: 'center'}}>
										{lotNos}
									</td>
			        			</tr>
			        			<tr>
			        				<td id="defectTwo" style={{verticalAlign: 'middle', textAlign: 'center'}}></td>
			        				<td id="rejectQty2" style={{textAlign: 'center', verticalAlign: 'middle'}}></td>
			        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>REJECT RATE (%)</b></td>
			        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>D.I. NO.</b></td>
			        			</tr>
			        			<tr>
			        				<td id="defectThree" style={{verticalAlign: 'middle', textAlign: 'center'}}></td>
			        				<td id="rejectQty3" style={{verticalAlign: 'middle', textAlign: 'center'}}></td>
			        				<td style={{textAlign: 'center', verticalAlign: 'middle', fontWeight: 'bold'}}>
			        					{parseFloat(rejectRate).toFixed(2)}
			        				</td>
			        				<td style={{textAlign: 'center', verticalAlign: 'middle'}}>
			        					{defectInfoNo}
			        				</td>
			        			</tr>
			        			<tr>
			        				<td id="defectFour" style={{verticalAlign: 'middle', textAlign: 'center'}}></td>
			        				<td id="rejectQty4" style={{verticalAlign: 'middle', textAlign: 'center'}}></td>
			        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}} ><b>DEFECT RANK</b></td>
			        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>RANKING CRITERIA</b></td>		
			        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>TROUBLE CLASSIFICATION</b></td>		
			        			</tr>
			        			<tr>
			        				<td id="defectFive" style={{verticalAlign: 'middle', textAlign: 'center'}}></td>
			        				<td id="rejectQty5" style={{verticalAlign: 'middle', textAlign: 'center'}}></td>
			        				<td style={{fontSize: 20, fontWeight: 'bold', color: 'blue', textAlign: 'center', verticalAlign: 'middle'}}>
			        					{deffectRank}
									</td>
									<td rowSpan="3" style={{}}>
										<b>RANK A</b> - Major Defects,<br/>Not Following UL Requirements
									</td>
									<td>
										<Form.Check
								            type='checkbox'
								            checked={("ifEmpty " + troubleClassification).includes("DOCUMENTS/LABELS")}
								            readOnly
								            id='documentLabelscheckbox'
								            label='DOCUMENTS/LABELS'
								          />
									 </td>
			        			</tr>
			        			<tr>
			        				<td id="defectSix" style={{verticalAlign: 'middle', textAlign: 'center'}}></td>
			        				<td id="rejectQty6" style={{verticalAlign: 'middle', textAlign: 'center'}}></td>
			        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>REMARKS</b></td>
			        				<td>
										<Form.Check 
								            type='checkbox'
								            checked={("ifEmpty " + troubleClassification).includes("PHYSICAL PROPERTIES")}
								            readOnly
								            id='physicalPropertiesCheckbox'
								            label='PHYSICAL PROPERTIES'
								          />
									 </td>
			        			</tr>
			        			<tr>
			        				<td id="defectSeven" style={{verticalAlign: 'middle', textAlign: 'center'}}></td>
			        				<td id="rejectQty7" style={{verticalAlign: 'middle', textAlign: 'center'}}></td>
			        				<td rowSpan="4">
			        					<textarea 
			        						rows="6"
			        						id="remarks"
			        						value={remarks ? remarks : ""}
			        						readOnly
			        						style={{width: '100%', height: '100%', border: 'none'}}
			        					>
			        					</textarea>
			        				</td>
			        				<td>
										<Form.Check 
								            type='checkbox'
								            checked={("ifEmpty " + troubleClassification).includes("DIMENSIONAL")}
								            readOnly
								            id='dimensionalCheckbox'
								            label='DIMENSIONAL'
								          />
									 </td>	        				
			        			</tr>
			        			<tr>
			        				<td id="defectEight" style={{verticalAlign: 'middle', textAlign: 'center'}}></td>
			        				<td id="rejectQty8" style={{verticalAlign: 'middle', textAlign: 'center'}}></td>
			        				<td rowSpan="3" style={{}}>
			        					<b>RANK B</b> - Minor Defects,<br/> No Major effect in function
			        				</td>
			        				<td>
										<Form.Check 
								            type='checkbox'
								            checked={("ifEmpty " + troubleClassification).includes("FUNCTIONAL")}
								            readOnly
								            id='functionalCheckbox'
								            label='FUNCTIONAL'
								          />
									 </td>
			        			</tr>
			        			<tr>
			        				<td id="defectNine" style={{verticalAlign: 'middle', textAlign: 'center'}}></td>
			        				<td id="rejectQty9" style={{verticalAlign: 'middle', textAlign: 'center'}}></td>
			        				<td>
										<Form.Check 
								            type='checkbox'
								            checked={("ifEmpty " + troubleClassification).includes("GREEN PURCHASING STD.")}
								            readOnly
								            id='greenPurchasingCheckbox'
								            label='GREEN PURCHASING STD.'
								          />
									 </td>
			        			</tr>
			        			<tr>
			        				<td style={{backgroundColor: 'rgb(248, 203, 173)', verticalAlign: 'middle', textAlign: 'center'}}><b>TOTAL DEFECT QTY</b></td>
			        				<td id="totalRejectQty" style={{textAlign: 'center', verticalAlign: 'middle'}}></td>
			        				<td></td>
			        			</tr>
			        			<tr>
			        				<td colSpan="5"></td>
			        			</tr>
			        			<tr>
			        				<td colSpan="5" style={{textAlign: 'center', backgroundColor: 'rgb(180, 198, 231)'}}><b>MAT MATERIAL DISPOSITION</b></td>
			        			</tr>
			        			<tr>
			        				<td colSpan="3" style={{backgroundColor: 'rgb(248, 203, 173)', fontStyle: 'italic', textAlign: 'center'}}><b>AFFECTED LOT / INVOICE NUMBER</b></td>
			        				<td style={{backgroundColor: 'rgb(248, 203, 173)', fontStyle: 'italic', textAlign: 'center'}}><b>DEFECTIVE MATERIAL</b></td>
			        				<td style={{backgroundColor: 'rgb(248, 203, 173)', fontStyle: 'italic', textAlign: 'center'}}><b>NG MATERIAL DISPOSAL</b></td>
			        			</tr>
			        			<tr>
			        				<td>
			        					<Form.Check 
								            type='checkbox'
								            checked={isAffectedLotRTV == 1}
								            readOnly
								            id='rtvCheckbox'
								            label='RTV'
								          />
			        				</td>
			        				<td colSpan="2">
			        					<Form.Check 
								            type='checkbox'
								            checked={affectedLotInvoiceNumber == "SUPPLIER SORTING / REWORK"}
								            readOnly	
								            id='supplierSortingCheckBox'
								            label='SUPPLIER SORTING / REWORK'
								          />
			        				</td>
			        				<td>
			        					<Form.Check 
								            type='checkbox'
								            checked={deffectiveMaterial == "REPLACEMENT"}
								            readOnly
								            id='replacementCheckbox'
								            label='REPLACEMENT'
								          />
			        				</td>
			        				<td>
			        					<Form.Check 
								            type='checkbox'
								            checked={ngMaterialDisposal == "MAT DISPOSAL FEE BY SUPPLIER"}
								            readOnly
								            id='matDisposalCheckbox'
								            label='MAT Disposal Fee by Supplier'
								          />
			        				</td>
			        			</tr>
			        			<tr>
			        				<td>
			        					<Form.Check 
								            type='checkbox'
								            checked={isAffectedLotOkToUse == 1}
								            readOnly	
								            id='okToUseCheckbox'
								            label='OK TO USE'
								          />
			        				</td>
			        				<td colSpan="2">
			        					<Form.Check 
								            type='checkbox'
								            checked={affectedLotInvoiceNumber == "MAT SORTING / REWORK (WITH CHARGE SHEET)"}
								            readOnly	
								            id='matSortingCheckbox'
								            label='MAT SORTING / REWORK (WITH CHARGE SHEET)'
								          />
			        				</td>
			        				<td>
			        					<Form.Check 
								            type='checkbox'
								            checked={deffectiveMaterial == "REFUND / DEBIT"}
								            readOnly
								            id='refundDebitCheckbox'
								            label='REFUND / DEBIT'
								          />
			        				</td>
			        				<td>
			        					<Form.Check 
								            type='checkbox'
								            checked={ngMaterialDisposal == "RTV"}
								            readOnly
								            id='ngMatRTVCheckbox'
								            label='RTV'
								          />
			        				</td>
			        			</tr>
			        			<tr>
			        				<td></td>
			        				<td colSpan="2"></td>
			        				<td>NG AMOUNT: $&nbsp;
			        					{ngAmount}
			        				</td>
			        				<td></td>
			        			</tr>        			
			        		</tbody>
			        	</Table>
						
						{/* SIGNATORIES */}
			        	<Table bordered>
			        		<tbody>
			        			<tr>
			        				<td style={{height: 100, width: 130, textAlign: 'center', verticalAlign: 'middle'}}>
			        					<img 
			        						src={
			        								preparedBy == "Jane Dagli" ?
			        									qcLeadGirlSign_Jane
			        								:
			        									qcLeadGirlSign_sheryl
			        							} 
			        						className="img-fluid"
			        					/>
			        				</td>
			        				<td style={{height: 100, width: 130, textAlign: 'center', verticalAlign: 'middle'}}>
			        					{
			        						(iqcSrSpvrApprove || approvedByIQCSrSupervisor) ?
												approvedByIQCSrSupervisor == 'Rose de Leon' ?
			        								<img src={iqcSrSpvrRoseSign} className="img-fluid"/>
												:
													<img src={qcEngineerAljadeSign} className="img-fluid" />
			        						:
			        							(localStorage.getItem("position") == "QC Engineer" 
												&& generalStatus == "OPEN") ?
				        							<Button className="ma-green-button"
				        								onClick={() => approve(1)}
				        							><i className="fa-solid fa-thumbs-up"></i> Approve</Button>
			        							:
				        							<Button className="ma-green-button"
					        								disabled
					        						><i className="fa-solid fa-thumbs-up"></i> Approve</Button>
			        					}
			        				</td>
			        				<td style={{height: 100, width: 130, textAlign: 'center', verticalAlign: 'middle'}}>
			        					{
			        						(qcSectionHeadApprove || approvedByQCSectionHead) ?
			        							<img src={qcSectionHeadLoidaSign} className="img-fluid"/>
			        						:
			        							(localStorage.getItem("position") == "QC Section Head" 
												&& approvedByIQCSrSupervisor
												&& generalStatus == "OPEN") ?
				        							<Button className="ma-green-button"
				        								onClick={() => approve(2)}
				        							><i className="fa-solid fa-thumbs-up"></i> Approve</Button>
				        						:
				        							<Button className="ma-green-button"
				        								disabled
				        							><i className="fa-solid fa-thumbs-up"></i> Approve</Button>

			        					}
			        				</td>
			        				<td style={{height: 100, width: 130, textAlign: 'center', verticalAlign: 'middle'}}>
			        					{
			        						(qcAsstMngrApprove || approvedByQCAsstManager) ?
												approvedByQCAsstManager  == 'Cristy Bautista' ?
			        								<img src={qcAsstmngrCristySign} className="img-fluid"/>
												:
			        								<img src={qcAsstmngrGeorgeSign} className="img-fluid"/>
			        						:
			        							(localStorage.getItem("position") == "QC Assistant Manager"
												&& localStorage.getItem("fullName") == "George Reyes"
												&& approvedByQCSectionHead
												&& generalStatus == "OPEN") ?
				        							<Button className="ma-green-button"
				        								onClick={() => approve(3)}
				        							><i className="fa-solid fa-thumbs-up"></i> Approve</Button>
			        							:
				        							<Button className="ma-green-button"
					        								disabled
					        						><i className="fa-solid fa-thumbs-up"></i> Approve</Button>
				        				}		
			        				</td>
									{
										isIncludeTechSuppMngrSuppier == 1 ?
											<td style={{height: 100, width: 130, textAlign: 'center', verticalAlign: 'middle'}}>
												{
													deffectRank == 'A' ?		        						
														(techSuppMngrApprove || approvedByTechSuppMngr) ?
															<img src={suppTechMngrFernanSign} className="img-fluid"/>
														:
															(localStorage.getItem("position") == "Supplier Technical Manager" 
															&& approvedByQCAsstManager
															&& generalStatus == "OPEN") ?
																<Button className="ma-green-button"
																	onClick={() => approve(4)}
																><i className="fa-solid fa-thumbs-up"></i> Approve</Button>
															:
																<Button className="ma-green-button"
																		disabled
																><i className="fa-solid fa-thumbs-up"></i> Approve</Button>
													:
														""
												}
											</td>
										:
												<></>
									}			        				
			        				<td style={{height: 100, width: 130, textAlign: 'center', verticalAlign: 'middle'}}>
			        					{
			        						deffectRank == 'A' ?
				        						(asstFactMngrApprove || approvedByAsstFactMngr) ?
				        							<img src={asstFactMngrSign} className="img-fluid"/>
				        						:
				        							// (localStorage.getItem("position") == "Assistant Factory Manager"
													// && generalStatus == "OPEN") ?
													// 	isIncludeTechSuppMngrSuppier == 1 ?
													// 		approvedByTechSuppMngr ?
													// 			<Button className="ma-green-button"
													// 				onClick={() => approve(5)}
													// 			><i className="fa-solid fa-thumbs-up"></i> Approve</Button>
													// 		:
													// 			<Button className="ma-green-button"
													// 			disabled
													// 			><i className="fa-solid fa-thumbs-up"></i> Approve</Button>
													// 	:
													// 		approvedByQCAsstManager ?
													// 			<Button className="ma-green-button"
													// 			onClick={() => approve(5)}
													// 			><i className="fa-solid fa-thumbs-up"></i> Approve</Button>
													// 		:
													// 			<Button className="ma-green-button"
													// 			disabled
													// 			><i className="fa-solid fa-thumbs-up"></i> Approve</Button>
					        						// :
					        						// 	<Button className="ma-green-button"
						        					// 			disabled
						        					// 	><i className="fa-solid fa-thumbs-up"></i> Approve</Button>
													null
						        			:
						        				""
			        					}
			        				</td>
			        				<td style={{height: 100, width: 130, textAlign: 'center', verticalAlign: 'middle'}}>
			        					{
			        						deffectRank == 'A' ?
				        						(factMngrApprove || approvedByFactMngr ) ?
				        							approvedByFactMngr == "Yasushi Hizakae" ?
				        								<img src={presSignHizakae} className="img-fluid"/>
				        							:
				        								<img src={factMngrSign} className="img-fluid"/>
				        						:
				        							((localStorage.getItem("position") == "Factory Manager"
				        								|| localStorage.getItem("position") == "President") 
														&& generalStatus == "OPEN") ?
														
															isIncludeTechSuppMngrSuppier == 1 ?
																approvedByTechSuppMngr ?
																	<Button className="ma-green-button"
																		onClick={() => approve(6)}
																	><i className="fa-solid fa-thumbs-up"></i> Approve</Button>
																:
																	<Button className="ma-green-button"
																	disabled
																	><i className="fa-solid fa-thumbs-up"></i> Approve</Button>
															:
																approvedByQCAsstManager ?
																	<Button className="ma-green-button"
																	onClick={() => approve(6)}
																	><i className="fa-solid fa-thumbs-up"></i> Approve</Button>
																:
																	<Button className="ma-green-button"
																	disabled
																	><i className="fa-solid fa-thumbs-up"></i> Approve</Button>
					        						:
					        							<Button className="ma-green-button"
						        								disabled
						        						><i className="fa-solid fa-thumbs-up"></i> Approve</Button>
						        			:
						        				""
			        					}
			        				</td>
									{
										isIncludeTechSuppMngrSuppier == 1 ?
											<></>
										:
											<td style={{height: 100, width: 130, textAlign: 'center', verticalAlign: 'middle'}}></td>	
									}
			        			</tr>
			        			<tr>
			        				<td>{
			        						preparedBy == "Jane Dagli" ?			        							
			        							"J. Dagli"
			        						:
			        							"S. Caballes"
			        					}
			        				</td>
			        				<td>
										{
											approvedByIQCSrSupervisor == 'Rose de Leon'
											? 'Ms. R. de Leon'
											: 'Mr. A. Florida'
										}
									</td>
			        				<td>Ms. L. Gabutin</td>
			        				<td>
										{
											approvedByQCAsstManager == 'Cristy Bautista' ? 'Cristy Bautista' : 'George Reyes'
										}
									</td>
			        				{isIncludeTechSuppMngrSuppier == 1 ? <td>{deffectRank == 'A' ? "Mr. F. Gonzales" : ""}</td> : <></>}
			        				<td>
										{
											deffectRank == 'A'
											? 
												(asstFactMngrApprove || approvedByAsstFactMngr)
												? "Asst. Factory Manager"
												: ""
											: ""
										}
									</td>
			        				<td>
			        					{
			        						deffectRank == 'A' ?
			        							localStorage.getItem("position") == "President" ?
			        								approvedByFactMngr != null && approvedByFactMngr != '' ?
			        									approvedByFactMngr == "Yasushi Hizakae" ?
			        										"Mr. Y. Hizakae"
			        									:
			        										"Mr. D. Goto"
			        								:
			        									"Mr. Y. Hizakae"
			        							:
			        								approvedByFactMngr != null && approvedByFactMngr != '' ?
			        									approvedByFactMngr == "Yasushi Hizakae" ?
			        										"Mr. Y. Hizakae"
			        									:
			        										"Mr. D. Goto"
			        								:
			        									"Mr. D. Goto"
			        						:
			        							""
			        					}
			        				</td>
									{isIncludeTechSuppMngrSuppier == 1 ? <></> : <td></td>}
			        			</tr>
			        			<tr>
			        				<td>QC Leadgirl</td>
			        				{/*<td>QC Supervisor.</td>*/}
			        				<td>
										{
											approvedByIQCSrSupervisor == 'Rose de Leon'
											? 'IQC Supervisor'
											: 'QC Engineer'
										}
									</td>
			        				<td>QC Section Head</td>
			        				<td>QC Assistant Manager</td>
			        				{isIncludeTechSuppMngrSuppier == 1 ? <td>{deffectRank == 'A' ? "Supp. Control Manager" : ""}</td> : <></>}
			        				<td>
										{
											deffectRank == 'A'
											? 
												(asstFactMngrApprove || approvedByAsstFactMngr)
												? "Asst. Factory Manager"
												: ""
											: ""
										}
									</td>
			        				<td>
			        				{
			        					deffectRank == 'A' ?
			        							localStorage.getItem("position") == "President" ?
			        								approvedByFactMngr != null && approvedByFactMngr != '' ?
			        									approvedByFactMngr == "Yasushi Hizakae" ?
			        										"President"
			        									:
			        										"Factory Manager"
			        								:
			        									"President"
			        							:
			        								approvedByFactMngr != null && approvedByFactMngr != '' ?
			        									approvedByFactMngr == "Yasushi Hizakae" ?
			        										"President"
			        									:
			        										"Factory Manager"
			        								:
			        									"Factory Manager"
			        						:
			        							""
			        				}
			        				</td>
									{isIncludeTechSuppMngrSuppier == 1 ? <></> : <td></td>}
			        			</tr>
			        			<tr style={{textAlign: 'center', backgroundColor: '#C6E0B4'}}>
			        				<td>PREPARED BY</td>
			        				<td>CHECKED BY</td>
			        				<td colSpan='5'>APPROVED BY</td>
			        			</tr>
			        		</tbody>
			        	</Table>

			        	{
			        		isIllustrationsLoading ?
			        			<LoadingButton/>
			        		:
			        			viewIllustrations ?
			        				<>
			        				<Button className='ma-blue-button mb-2' onClick={() => setViewIllustrations(false)}><i className="fa-solid fa-eye-slash"></i> Hide Illustrations</Button>
			        				<Table bordered>
						        		<tbody>
						        			<tr>
						        				<td colSpan='2' style={{textAlign: 'center', fontWeight: 'bold', backgroundColor: '#FFFF65'}}>ILLUSTRATIONS</td>
						        			</tr>
						        			{
						        				illustrationScript.length > 0 ? 
						        					illustrationScript
						        				:
						        					<tr colSpan='2'><td style={{textAlign:'center'}}><i className="fa-solid fa-not-equal"></i> NO ATTACHED ILLUSTRATIONS</td></tr>
						        			}
						        		</tbody>
						        	</Table>						        	
						        	</>
					        	:
			        				<Button className='ma-blue-button' onClick={() => setViewIllustrations(true)}><i className="fa-solid fa-eye"></i> See Illustrations</Button>
			        	}			        	

			        	<Table>
			        		<tbody>
			        			<tr bordered="false">
			        				<td>										
			        				{
			        					// ((approvedByAsstFactMngr || asstFactMngrApprove) &&
			        					(approvedByFactMngr || factMngrApprove)

			        					||

			        					((deffectRank == 'B')
			        					&& (approvedByIQCSrSupervisor || iqcSrSpvrApprove)
			        					&& (approvedByQCSectionHead || qcSectionHeadApprove)
			        					&& (approvedByQCAsstManager || qcAsstMngrApprove)) ?
				        					<Button 
				        						className="align-right ma-blue-button"
				        						onClick={() => generatePDF()}
				        					>
				        						<i className="fa-solid fa-file-export"></i> EXPORT PDF
				        					</Button>
				        				:
				        					<Button 
				        						className="align-right ma-blue-button"
				        						disabled
				        					>
				        						<i className="fa-solid fa-file-export"></i> EXPORT PDF
				        					</Button>
			        				}
			        					<div className="align-right">&nbsp;</div>
					        			<Button className="align-right ma-gray-button"
					        					onClick={handleClose}
					        			>
					        				<i className="fa-solid fa-xmark"></i> CLOSE
					        			</Button>
										<div className="align-right">&nbsp;</div>
										{
											((localStorage.getItem('position') == "IQC Sr. Supervisor" 
											|| localStorage.getItem('position') == "QC Section Head"
											) && generalStatus == "OPEN" ) ?
												<Button 
												className="ma-red-button align-right"
												onClick={() => setCancelSqarModalShow(true)}
												>
													<i className="fa-sharp fa-solid fa-ban"></i> CANCEL SQAR
												</Button>
											:
												<></>
										}										
					        		</td>
			        			</tr>
			        		</tbody>
			        	</Table>
		        	</div>

		        	<div id="sqarSecondPage" style={{display: modalPage == 2 ? 'block' : 'none'}}>
		        		<Table bordered>
			        		<tbody>
			        			<tr>
			        				<td colSpan="6"><img src={maLogo} width="100" height="100" className="img-fluid ma-navbar-logo"/>
			        					<img src={assuredlogo} width="100" height="100" className="img-fluid align-right"/>
			        				</td>
			        			</tr>
			        			<tr>
			        				<td colSpan="4" style={{backgroundColor: '#9966FF', fontSize: 25}}><center><b>SUPPLIER QUALITY ABNORMALITY REPORT (SQAR)</b></center></td>		        				
			        				<td style={{backgroundColor: '#F8CBAD', textAlign: 'right', verticalAlign: 'middle'}}><b>CONTROL NO.</b></td>
			        				<td 
			        					style={{verticalAlign: 'middle', textAlign: 'center'}}>	
			        					{sqarControlNo}
			        				</td>      					        				
			        			</tr>
			        		</tbody>
			        	</Table>

			        	<Table bordered>
			        		<tbody>
			        			<tr>
			        				<td colSpan="8" style={{textAlign: 'center', backgroundColor: 'rgb(180, 198, 231)'}}>
			        					<b>SUPPLIER PORTION - MATERIAL DISPOSITION</b>
			        				</td>			        			
			        			</tr>
			        			<tr style={{height: 200}}>
			        				<td colSpan="8" style={{textAlign: 'center', verticalAlign: 'middle'}}>
			        					<center>
			        					 {selectedImage != null || imageName != null ?
									        <div>
									        	{
									        		imageName != null ?
									        			<a style={{cursor: 'zoom-in'}} href={`${process.env.REACT_APP_SQAR_API}/api/image/` + imageName} target="_blank">
									        			<img alt="not found" width={"100%"} className="img-fluid" src={`${process.env.REACT_APP_SQAR_API}/api/image/` + imageName} />
									        			</a>
									        		:
									        			<a style={{cursor: 'zoom-in'}} href={URL.createObjectURL(selectedImage)} target="_blank">
									        			<img alt="not found" width={"100%"} className="img-fluid" src={URL.createObjectURL(selectedImage)} />
									        			</a>
									        	}
									        </div>
									      :
									      	<Form.Group controlId="formFile" className="mb-3">
										        <Form.Label><i className="fa-solid fa-photo-film"></i> Attach Supplier Material Disposition</Form.Label>
										        <Form.Control 
										        style={{width: 400}} 
										        type="file"
										        disabled={!((localStorage.getItem('position') == 'IQC Sr. Supervisor' 
												|| localStorage.getItem('position') == 'QC Engineer' 
												|| localStorage.getItem('position') == 'QC Section Head' 
												|| localStorage.getItem('position') == 'QC Assistant Manager')
												&& generalStatus == "OPEN")}
										        onChange={(event) => {
										          setSelectedImage(event.target.files[0]);
										        }}
												/>
										    </Form.Group>
									      }
									    </center>
			        				</td>
			        			</tr>			        			
	        					{
	        					selectedImage && (
	        					<tr>
	        						<td colSpan="8">
	        					 		<div>
	        					 			<Button
	        					 			className="ma-blue-button align-right"
	        					 			onClick={() => saveSupplierReplyPhoto()}
	        					 			>
	        					 				<i className="fa-solid fa-upload"></i> SAVE PHOTO
	        					 			</Button>

		        					 		<Button 
		        					 		className="ma-red-button align-right mx-1" onClick={()=>setSelectedImage(null)}>
		        					 			<i className="fa-solid fa-eraser"></i> REMOVE PHOTO
		        					 		</Button>				        					 		
	        					 		</div>	        					 	
	        					 	</td>
	        					</tr>
	        					)
	        					}
	        					{imageName != null && (
	        					<>
	        					<tr>
	        						<td colSpan="8" style={{textAlign: 'center', backgroundColor: 'rgb(180, 198, 231)'}}><b>MAT'S APPROVAL</b></td>
	        					</tr>
	        					<tr style={{backgroundColor: '#C6E0B4'}}>
	        						<td style={{textAlign: 'center', verticalAlign: 'middle'}}>
			        					PREPARED
			        				</td>
			        				<td style={{textAlign: 'center', verticalAlign: 'middle'}}>
			        					CHECKED
			        				</td>
			        				<td style={{textAlign: 'center', verticalAlign: 'middle'}}>
			        					APPROVED
			        				</td>
			        				<td style={{textAlign: 'center', verticalAlign: 'middle'}}></td>
			        				<td style={{textAlign: 'center', verticalAlign: 'middle'}}></td>
			        				<td style={{TextAlign: 'center', verticalAlign: 'middle'}}></td>
			        				<td style={{TextAlign: 'center', verticalAlign: 'middle'}}></td>
			        				<td style={{TextAlign: 'center', verticalAlign: 'middle'}}></td>
	        					</tr>	
	        					<tr>
	        						<td style={{height: 75, width: 130, textAlign: 'center', verticalAlign: 'middle'}}>
			        					{
			        						(supMatDispo_isApprovedBy_IQCSrSupervisor || supMatDispoApprovedBy_IQCSrSupervisor) ?
												supMatDispoApprovedBy_IQCSrSupervisor == "Rose de Leon"
			        							? <img src={iqcSrSpvrRoseSign} className="img-fluid"/>
												: <img src={qcEngineerAljadeSign} className="img-fluid"/>
			        						:
			        							(
												(localStorage.getItem("position") == "IQC Sr. Supervisor"
												|| localStorage.getItem("position") == "QC Engineer"
												) && generalStatus == "OPEN") ?
				        							<Button className="ma-green-button"
				        								onClick={() => approveSupMatDisposal(1)}
				        							><i className="fa-solid fa-thumbs-up"></i> Approve</Button>
			        							:
				        							<Button className="ma-green-button"
					        								disabled
					        						><i className="fa-solid fa-thumbs-up"></i> Approve</Button>
			        					}
			        				</td>
			        				<td style={{height: 75, width: 130, textAlign: 'center', verticalAlign: 'middle'}}>
			        					{
			        						(supMatDispoApprovedBy_QCSectionHead || supMatDispo_isApprovedBy_QCSectionHead) ?
			        							<img src={qcSectionHeadLoidaSign} className="img-fluid"/>
			        						:
			        							(localStorage.getItem("position") == "QC Section Head" 
												&& supMatDispoApprovedBy_IQCSrSupervisor
												&& generalStatus == "OPEN") ?
				        							<Button className="ma-green-button"
				        								onClick={() => approveSupMatDisposal(2)}
				        							><i className="fa-solid fa-thumbs-up"></i> Approve</Button>
			        							:
				        							<Button className="ma-green-button"
					        								disabled
					        						><i className="fa-solid fa-thumbs-up"></i> Approve</Button>
			        					}
			        				</td>
			        				<td style={{height: 75, width: 130, textAlign: 'center', verticalAlign: 'middle'}}>
			        					{
			        						(supMatDispoApprovedBy_QCAsstManager || supMatDispo_isApprovedBy_QCAsstManager) ?
												supMatDispoApprovedBy_QCAsstManager == 'Cristy Bautista'
			        							? <img src={qcAsstmngrCristySign} className="img-fluid"/>
			        							: <img src={qcAsstmngrGeorgeSign} className="img-fluid"/>
			        						:
			        							(localStorage.getItem("position") == "QC Assistant Manager"
												&& localStorage.getItem("fullName") == "George Reyes" 
												&& supMatDispoApprovedBy_QCSectionHead
												&& generalStatus == "OPEN") ?
				        							<Button className="ma-green-button"
				        								onClick={() => approveSupMatDisposal(3)}
				        							><i className="fa-solid fa-thumbs-up"></i> Approve</Button>
			        							:
				        							<Button className="ma-green-button"
					        								disabled
					        						><i className="fa-solid fa-thumbs-up"></i> Approve</Button>
			        					}
			        				</td>
			        				<td style={{height: 75, width: 130, textAlign: 'center', verticalAlign: 'middle'}}></td>
			        				<td style={{height: 75, width: 130, textAlign: 'center', verticalAlign: 'middle'}}></td>
			        				<td style={{height: 75, width: 130, textAlign: 'center', verticalAlign: 'middle'}}></td>
			        				<td style={{height: 75, width: 130, textAlign: 'center', verticalAlign: 'middle'}}></td>
			        				<td style={{height: 75, width: 130, textAlign: 'center', verticalAlign: 'middle'}}></td>
	        					</tr>
	        					<tr>
	        						<td style={{textAlign: 'center', verticalAlign: 'middle'}}>
										{
											(supMatDispo_isApprovedBy_IQCSrSupervisor || supMatDispoApprovedBy_IQCSrSupervisor) ?
												supMatDispoApprovedBy_IQCSrSupervisor == "Rose de Leon"
												? "IQC Supvr"
												: "QC Engr."												
											:
												"IQC Supvr/QC Engr."
										}			        					
			        				</td>
			        				<td style={{textAlign: 'center', verticalAlign: 'middle'}}>
			        					QC Section Head
			        				</td>
			        				<td style={{textAlign: 'center', verticalAlign: 'middle'}}>
			        					QC Asst Mngr
			        				</td>
			        				<td style={{textAlign: 'center', verticalAlign: 'middle'}}></td>
			        				<td style={{textAlign: 'center', verticalAlign: 'middle'}}></td>
			        				<td style={{TextAlign: 'center', verticalAlign: 'middle'}}></td>
			        				<td style={{TextAlign: 'center', verticalAlign: 'middle'}}></td>
			        				<td style={{TextAlign: 'center', verticalAlign: 'middle'}}></td>
	        					</tr>	        					
	        					</>
	        					)
	        					}	        								        			
			        		</tbody>
			        	</Table>

			        	<Table>
			        		<tbody>
			        			<tr bordered="false">
			        				<td>
			        					{			        						
				        					((supMatDispoApprovedBy_IQCSrSupervisor || supMatDispo_isApprovedBy_IQCSrSupervisor)
				        					&& (supMatDispoApprovedBy_QCSectionHead || supMatDispo_isApprovedBy_QCSectionHead)
				        					&& (supMatDispoApprovedBy_QCAsstManager || supMatDispo_isApprovedBy_QCAsstManager)) ?
				        						<Button 
					        						className="align-right ma-blue-button"
					        						onClick={() => generateApprovedSupMatDispoPDF()}
					        					>
					        						<i className="fa-solid fa-file-export"></i> EXPORT PDF
					        					</Button>
					        				:
					        					<Button 
					        						className="align-right ma-blue-button"
					        						disabled
					        					>
					        						<i className="fa-solid fa-file-export"></i> EXPORT PDF
					        					</Button>
			        					}			        					
			        					<div className="align-right">&nbsp;</div>
					        			<Button className="align-right ma-gray-button"
					        					onClick={handleClose}
					        			>
					        				<i className="fa-solid fa-xmark"></i> Close
					        			</Button>
					        		</td>
			        			</tr>
			        		</tbody>
			        	</Table>
		        	</div>

		        	<div id="sqarThirdPage" style={{display: modalPage == 3 ? 'block' : 'none'}}>
		        		<Table bordered>
			        		<tbody>
			        			<tr>
			        				<td colSpan="6"><img src={maLogo} width="100" height="100" className="img-fluid ma-navbar-logo"/>
			        					<img src={assuredlogo} width="100" height="100" className="img-fluid align-right"/>
			        				</td>
			        			</tr>
			        			<tr>
			        				<td colSpan="4" style={{backgroundColor: '#9966FF', fontSize: 25}}><center><b>SUPPLIER QUALITY ABNORMALITY REPORT (SQAR)</b></center></td>		        				
			        				<td style={{backgroundColor: '#F8CBAD', textAlign: 'right', verticalAlign: 'middle'}}><b>CONTROL NO.</b></td>
			        				<td 
			        					style={{verticalAlign: 'middle', textAlign: 'center'}}>	
			        					{sqarControlNo}       				
			        				</td>      					        				
			        			</tr>
			        		</tbody>
			        	</Table>

			        	<Table bordered>
			        		<tbody>
			        			<tr>
			        				<td colSpan="8" style={{textAlign: 'center', backgroundColor: 'rgb(180, 198, 231)'}}>
			        					<b>SUPPLIER PORTION - ROOT CAUSE & CORRECTIVE ACTION</b>
			        				</td>
			        			</tr>
			        			{
			        				rccaImagesScript
			        			}
			        			{
			        				rccaImagesNames.length > 0 || isUploaded ?
			        					<></>
			        				:
				        				(isAddRccaImage || selectedRCCAImages.length < 1) ?
				        					attachImageScript()
				        				:
				        					<tr>
				        						<td>				        							
				        							<Button className="ma-blue-button align-right mx-1" onClick={() => uploadRccaImages()}>
				        								<i className="fa-solid fa-upload"></i> UPLOAD
				        							</Button>
				        							<Button className="ma-blue-button align-right" onClick={() => setIsAddRccaImage(true)}>
				        								<i className="fa-solid fa-plus"></i> ADD MORE
				        							</Button>
				        						</td>
				        					</tr>
				        		}	        						
			        		</tbody>
			        	</Table>
				        {isUploaded || rccaImagesNames.length > 0 ?
				        	<>
				        	<Table bordered>
				        		<tbody>
		        					<tr>
		        						<td colSpan='11' style={{textAlign: 'center', verticalAlign: 'middle', backgroundColor: 'rgb(180, 198, 231)'}}>
		        							<b>MAT IQA COMMENTS/REQUESTS:</b>
		        						</td>
		        					</tr>
		        					<tr>
		        						<td rowSpan='2' colSpan='10' style={{whiteSpace: 'pre-wrap'}}>
		        							{
		        								matCommentsRequest ? 
		        									matCommentsRequest
		        								:
		        									<>
		        									<textarea id='matRccaComments'
		        											  style={{whiteSpace: 'pre-wrap', width: '100%', height: '100%'}}
		        											  rows='4'
		        											  defaultValue={matComments}
		        											  onChange={event => setMatComments(event.target.value)}
		        											  >
		        									</textarea>	        									
		        									{
		        										matComments ?
		        											<Button className='ma-blue-button'
		        													onClick={() => saveMatRCCAComments()}
		        											>SAVE</Button>
		        										:
		        											<Button className='ma-blue-button'
		        													disabled
		        											>SAVE</Button>	
		        									}
		        									</>
		        							}	        							
		        						</td>
		        						<td style={{textAlign: 'center', verticalAlign: 'middle', backgroundColor: 'rgb(180, 198, 231)', width: 110, fontSize: 12}}>
		        							<b>Disposition</b>
		        						</td>
		        					</tr>
		        					<tr style={{fontSize: 14}}>
		        						<td>
		        							<Form>
		        								<Form.Check 
									            type='radio'
									            id='maDispositionApprove'
									            name='maDisposition'
									            label='Approve'
									            defaultChecked = {isRccaDispositionApproved}
									            onClick={() => approveRccaDisposition()}
									          />
									          <Form.Check
									            type='radio'								            
									            id='maDispositionDisapprove'
									            name='maDisposition'
									            label='Disapprove'
									            defaultChecked = {isRccaDispositionApproved != null && isRccaDispositionApproved != true}
									            onClick={() => disapproveRccaDisposition()}
									          />
		        							</Form>
		        						</td>
		        					</tr>
		        					<tr style={{textAlign: 'center', verticalAlign: 'middle', backgroundColor: 'rgb(180, 198, 231)', width: 120, fontSize: 12}}>
		        						<td colSpan='4'><b>CONFIRMATION</b></td>
		        						<td colSpan='9'><b>EFFECTIVENESS OF CORRECTIVE ACTION</b></td>
		        					</tr>
		        					<tr style={{fontSize: 11, fontWeight: 'bold', textAlign: 'center', verticalAlign: 'middle'}}>
		        						<td style={{backgroundColor: '#C6E0B4', width: 90}}>PREPARED</td>
		        						<td style={{backgroundColor: '#C6E0B4', width: 90}}>CHECKED</td>
		        						<td style={{backgroundColor: '#C6E0B4', width: 90}}>APPROVED</td>
		        						<td style={{backgroundColor: '#F8CBAD', width: 80, textAlign: 'center', verticalAlign: 'middle'}}>RESPONSE DATE</td>
		        						<td style={{backgroundColor: '#FFF2CC'}} colSpan='2'>1st Delivery</td>
		        						<td style={{backgroundColor: '#FFF2CC'}} colSpan='2'>2nd Delivery</td>
		        						<td style={{backgroundColor: '#FFF2CC'}} colSpan='2'>3rd Delivery</td>
		        						<td style={{backgroundColor: '#F8CBAD', textAlign: 'center', verticalAlign: 'middle'}}>SQAR STATUS</td>
		        					</tr>
		        					<tr>
		        						<td style={{textAlign: 'center', verticalAlign: 'middle'}} rowSpan='2'>
		        							{
		        								(supRCCAApprovedBy_IQCSrSupervisor || supRCCA_isApprovedBy_IQCSrSupervisor) ?
													supRCCAApprovedBy_IQCSrSupervisor == 'Rose de Leon'
													? <img src={iqcSrSpvrRoseSign} className='img-fluid'/>
													: <img src={qcEngineerAljadeSign} className='img-fluid'/>
		        								:
		        									(localStorage.getItem('position') == 'IQC Sr. Supervisor'
													|| localStorage.getItem('position') == 'QC Engineer'
													&& generalStatus == "OPEN") ?
		        										<Button 
					        							className="ma-green-button w-75"
					        							style={{height: 30, padding: 0}}
					        							onClick={() => approveSupRCCA(1)}
									        			>
									        				<i className="fa-solid fa-thumbs-up"></i>
									        			</Button>
									        		:
									        			<Button 
					        							className="ma-green-button w-75"
					        							style={{height: 30, padding: 0}}
									        			disabled
									        			>
									        				<i className="fa-solid fa-thumbs-up"></i>
									        			</Button>
		        							}	        							
						        		</td>
		        						<td style={{textAlign: 'center', verticalAlign: 'middle'}} rowSpan='2'>
		        							{
		        								(supRCCAApprovedBy_QCSectionHead || supRCCA_isApprovedBy_QCSectionHead) ?
		        									<img src={qcSectionHeadLoidaSign} className='img-fluid'/>
		        								:
		        									(localStorage.getItem('position') == 'QC Section Head' 
													&& supRCCAApprovedBy_IQCSrSupervisor
													&& generalStatus == "OPEN") ?
		        										<Button 
					        							className="ma-green-button w-75"
					        							style={{height: 30, padding: 0}}
					        							onClick={() => approveSupRCCA(2)}
									        			>
									        				<i className="fa-solid fa-thumbs-up"></i>
									        			</Button>
									        		:
									        			<Button 
					        							className="ma-green-button w-75"
					        							style={{height: 30, padding: 0}}
									        			disabled
									        			>
									        				<i className="fa-solid fa-thumbs-up"></i>
									        			</Button>
		        							}
						        		</td>
		        						<td style={{textAlign: 'center', verticalAlign: 'middle'}} rowSpan='2'>
		        							{
		        								(supRCCAApprovedBy_QCAsstManager || supRCCA_isApprovedBy_QCAsstManager) ?
													supRCCAApprovedBy_QCAsstManager == 'Cristy Bautista'
		        									? <img src={qcAsstmngrCristySign} className='img-fluid' />
		        									: <img src={qcAsstmngrGeorgeSign} className='img-fluid' />
		        								:
		        									(localStorage.getItem('position') == 'QC Assistant Manager'
													&& localStorage.getItem('fullName') == 'George Reyes'
													&& supRCCAApprovedBy_QCSectionHead
													&& generalStatus == "OPEN") ?
		        										<Button 
					        							className="ma-green-button w-75"
					        							style={{height: 30, padding: 0}}
					        							onClick={() => approveSupRCCA(3)}
									        			>
									        				<i className="fa-solid fa-thumbs-up"></i>
									        			</Button>
									        		:
									        			<Button 
					        							className="ma-green-button w-75"
					        							style={{height: 30, padding: 0}}
									        			disabled
									        			>
									        				<i className="fa-solid fa-thumbs-up"></i>
									        			</Button>
		        							}
						        		</td>
						        		<td style={{width: 80, textAlign: 'center', verticalAlign: 'middle', fontSize: 11, fontWeight: 'bold'}}>
							        		{
							        			rccaResponseDate ? 
							        				rccaResponseDate.slice(5, 10) + '-' + rccaResponseDate.slice(0, 4) 
							        			: 
							        				''
							        		}
						        		</td>
		        						<td style={{width: 80, textAlign: 'center', verticalAlign: 'middle', fontSize: 11, fontWeight: 'bold'}}>
		        							{
		        								(firstDeliveryCheck.date) ?
		        									firstDeliveryCheck.date.slice(5, 10) + '-' + firstDeliveryCheck.date.slice(0, 4)
		        								:
		        									(localStorage.getItem('position') == 'IQC Sr. Supervisor'
													|| localStorage.getItem('position') == 'QC Engineer') ?
		        										<input 
		        										type='date' 
		        										id='firstDeliveryDate' 
		        										style={{width: 80}}
		        										value={firstDeliveryCheckTemp.date}
		        										onChange={event => setFirstDeliveryCheckTemp(firstDeliveryCheckTemp => ({
		        											...firstDeliveryCheckTemp,
		        											date: event.target.value
		        										}))}
		        										/>
		        									:
		        										<input type='date' disabled style={{width: 80}} />
		        							}
		        						</td>
		        						<td style={{width: 100, textAlign: 'center', verticalAlign: 'middle', fontSize: 11, fontWeight: 'bold'}}>
		        							{
		        								(firstDeliveryCheck.quantity) ? 
		        									firstDeliveryCheck.quantity
		        								:
		        									(localStorage.getItem('position') == 'IQC Sr. Supervisor'
													|| localStorage.getItem('position') == 'QC Engineer') ?
		        										<input 
		        										type='number' 
		        										id='firstDeliveryQuantity' 
		        										style={{width: 80, border: 'none', textAlign: 'center'}} 
		        										placeholder='click to add'
		        										defaultValue={firstDeliveryCheckTemp.quantity}
		        										onChange={event => setFirstDeliveryCheckTemp(firstDeliveryCheckTemp => ({
		        											...firstDeliveryCheckTemp,
		        											quantity: event.target.value
		        										}))}
		        										/>
		        									:
		        										<input type='number' disabled style={{width: 80, border: 'none', textAlign: 'center'}} placeholder='click to add' />
		        							}
		        						</td>
		        						<td style={{width: 80, textAlign: 'center', verticalAlign: 'middle', fontSize: 11, fontWeight: 'bold'}}>
		        							{
		        								(secondDeliveryCheck.date) ?
		        									secondDeliveryCheck.date.slice(5, 10) + '-' + secondDeliveryCheck.date.slice(0, 4)
		        								:
		        									((localStorage.getItem('position') == 'IQC Sr. Supervisor'
													|| localStorage.getItem('position') == 'QC Engineer') && firstDeliveryCheck.date) ?
		        										<input 
		        										type='date'
		        										id='secondDeliveryDate'
		        										style={{width: 80}}
		        										defaultValue={secondDeliveryCheckTemp.date}
		        										onChange={event => setSecondDeliveryCheckTemp(secondDeliveryCheckTemp => ({
		        											...secondDeliveryCheckTemp,
		        											date: event.target.value
		        										}))}
		        										/>
		        									:
		        										<input type='date' disabled style={{width: 80}} />
		        							}
		        						</td>
		        						<td style={{width: 100, textAlign: 'center', verticalAlign: 'middle', fontSize: 11, fontWeight: 'bold'}}>
		        							{
		        								(secondDeliveryCheck.quantity) ? 
		        									secondDeliveryCheck.quantity
		        								:
		        									((localStorage.getItem('position') == 'IQC Sr. Supervisor'
													|| localStorage.getItem('position') == 'QC Engineer') && firstDeliveryCheck.quantity) ?
		        										<input 
		        										type='number' 
		        										style={{width: 80, border: 'none', textAlign: 'center'}} 
		        										placeholder='click to add'
		        										defaultValue={secondDeliveryCheckTemp.quantity}
		        										onChange={event => setSecondDeliveryCheckTemp(secondDeliveryCheckTemp => ({
		        											...secondDeliveryCheckTemp,
		        											quantity: event.target.value
		        										}))}
		        										/>
		        									:
		        										<input type='number' disabled style={{width: 80, border: 'none', textAlign: 'center'}} placeholder='click to add' />
		        							}
		        						</td>
		        						<td style={{width: 80, textAlign: 'center', verticalAlign: 'middle', fontSize: 11, fontWeight: 'bold'}}>
		        							{
		        								(thirdDeliveryCheck.date) ?
		        									thirdDeliveryCheck.date.slice(5, 10) + '-' + thirdDeliveryCheck.date.slice(0, 4)
		        								:
		        									((localStorage.getItem('position') == 'IQC Sr. Supervisor'
													|| localStorage.getItem('position') == 'QC Engineer') && secondDeliveryCheck.date) ?
		        										<input
		        										type='date'
		        										style={{width: 80}}
		        										defaultValue={thirdDeliveryCheckTemp.date}
		        										onChange={event => setThirdDeliveryCheckTemp(thirdDeliveryCheckTemp => ({
		        											...thirdDeliveryCheckTemp,
		        											date: event.target.value
		        										}))}
		        										/>
		        									:
		        										<input type='date' disabled style={{width: 80}} />
		        							}
		        						</td>
		        						<td style={{width: 100, textAlign: 'center', verticalAlign: 'middle', fontSize: 11, fontWeight: 'bold'}}>
		        							{
		        								(thirdDeliveryCheck.quantity) ? 
		        									thirdDeliveryCheck.quantity
		        								:
		        									((localStorage.getItem('position') == 'IQC Sr. Supervisor'
													|| localStorage.getItem('position') == 'QC Engineer') && secondDeliveryCheck.quantity) ?
		        										<input 
		        										type='number'
		        										style={{width: 80, border: 'none', textAlign: 'center'}}
		        										placeholder='click to add'
		        										defaultValue={thirdDeliveryCheckTemp.quantity}
		        										onChange={event => setThirdDeliveryCheckTemp(thirdDeliveryCheckTemp => ({
		        											...thirdDeliveryCheckTemp,
		        											quantity: event.target.value
		        										}))}
		        										/>
		        									:
		        										<input type='number' disabled style={{width: 80, border: 'none', textAlign: 'center'}} placeholder='click to add' />
		        							}
		        						</td>
		        						<td style={{textAlign: 'center', verticalAlign: 'middle'}} rowSpan='2'>
										{
											sqarStatus == 14 ? 
												<h5><Badge bg="danger">CLOSED</Badge></h5>
											:
												<></>
										}
										</td>
		        					</tr>
		        					<tr style={{fontSize: 10, fontWeight: 'bold', textAlign: 'center', verticalAlign: 'middle'}}>
										<td>
											<button
											style={{border: 0, fontSize: 20, backgroundColor: 'transparent', color: '#3C5393'}}
											onClick={() => updateRccaReceiveDate()}
											>
												<i className="fa-solid fa-pen-to-square"></i>
											</button>
										</td>
		        						<td style={{backgroundColor: '#C6E0B4'}}>CHECKED BY:</td>
		        						<td >
		        							{
		        								(firstDeliveryCheck.isOk == true) ?
													firstDeliveryCheck.approver == 'Rose de Leon'
		        									? <img src={iqcSrSpvrRoseSign2} style={{height: 30, width: 50}}/>
		        									: <img src={qcEngineerAljadeSign} style={{height: 30, width: 50}}/>
		        								:
		        									((localStorage.getItem('position') == 'IQC Sr. Supervisor'
														|| localStorage.getItem('position') == 'QC Engineer')
		        									&& firstDeliveryCheckTemp.date
		        									&& firstDeliveryCheckTemp.quantity
													&& (supRCCAApprovedBy_QCAsstManager || supRCCA_isApprovedBy_QCAsstManager)
													&& (supMatDispoApprovedBy_QCAsstManager || supMatDispo_isApprovedBy_QCAsstManager)
													&& generalStatus == "OPEN") ?
			        									<Button 
					        							className="ma-green-button w-75"
					        							style={{height: 30, padding: 0}}
					        							onClick={() => saveDeliveryCheck(1)}
									        			>
									        				<i className="fa-solid fa-thumbs-up"></i>
									        			</Button>
									        		:
									        			<Button 
					        							className="ma-green-button w-75"
					        							style={{height: 30, padding: 0}}
									        			disabled
									        			>
									        				<i className="fa-solid fa-thumbs-up"></i>
									        			</Button>
		        							}        							        							
		        							
		        						</td>
		        						<td style={{backgroundColor: '#C6E0B4'}}>CHECKED BY:</td>
		        						<td style={{}}>
		        							{
		        								(secondDeliveryCheck.isOk == true) ?
													secondDeliveryCheck.approver == 'Rose de Leon'
													? <img src={iqcSrSpvrRoseSign2} style={{height: 30, width: 50}}/>
													: <img src={qcEngineerAljadeSign} style={{height: 30, width: 50}}/>
		        								:
		        									((localStorage.getItem('position') == 'IQC Sr. Supervisor'
													|| localStorage.getItem('position') == 'QC Engineer')
													&& secondDeliveryCheckTemp.date
		        									&& secondDeliveryCheckTemp.quantity
													&& firstDeliveryCheck.isOk == 1
													&& generalStatus == "OPEN") ?
			        									<Button 
					        							className="ma-green-button w-75"
					        							style={{height: 30, padding: 0}}
					        							onClick={() => saveDeliveryCheck(2)}
									        			>
									        				<i className="fa-solid fa-thumbs-up"></i>
									        			</Button>
									        		:
									        			<Button 
					        							className="ma-green-button w-75"
					        							style={{height: 30, padding: 0}}
									        			disabled
									        			>
									        				<i className="fa-solid fa-thumbs-up"></i>
									        			</Button>
		        							}
		        						</td>
		        						<td style={{backgroundColor: '#C6E0B4'}}>CHECKED BY:</td>
		        						<td style={{}}>
		        							{
		        								(thirdDeliveryCheck.isOk == true) ?
												thirdDeliveryCheck.approver == 'Rose de Leon'
													? <img src={iqcSrSpvrRoseSign2} style={{height: 30, width: 50}}/>
													: <img src={qcEngineerAljadeSign} style={{height: 30, width: 50}}/>
		        								:
		        									((localStorage.getItem('position') == 'IQC Sr. Supervisor'
													|| localStorage.getItem('position') == 'QC Engineer')
													&& thirdDeliveryCheckTemp.date
		        									&& thirdDeliveryCheckTemp.quantity
													&& secondDeliveryCheck.isOk == 1
													&& generalStatus == "OPEN") ?
			        									<Button 
					        							className="ma-green-button w-75"
					        							style={{height: 30, padding: 0}}
					        							onClick={() => saveDeliveryCheck(3)}
									        			>
									        				<i className="fa-solid fa-thumbs-up"></i>
									        			</Button>
									        		:
									        			<Button 
					        							className="ma-green-button w-75"
					        							style={{height: 30, padding: 0}}
									        			disabled
									        			>
									        				<i className="fa-solid fa-thumbs-up"></i>
									        			</Button>
		        							}        							
		        						</td>
		        					</tr>
	        					</tbody>
				        	</Table>

				        	<Table>
				        		<tbody>
				        			<tr bordered="false">
				        				<td>
				        					{			        						
					        					((supRCCAApprovedBy_IQCSrSupervisor || supRCCA_isApprovedBy_IQCSrSupervisor)
					        					&& (supRCCAApprovedBy_QCSectionHead || supRCCA_isApprovedBy_QCSectionHead)
					        					&& (supRCCAApprovedBy_QCAsstManager || supRCCA_isApprovedBy_QCAsstManager)) ?
					        						<Button 
						        						className="align-right ma-blue-button"
						        						onClick={() => generateSQARSupMatDispoWithSupRCCA()}
						        					>
						        						<i className="fa-solid fa-file-export"></i> EXPORT PDF
						        					</Button>
						        				:
						        					<Button 
						        						className="align-right ma-blue-button"
						        						disabled
						        					>
						        						<i className="fa-solid fa-file-export"></i> EXPORT PDF
						        					</Button>
				        					}			        					
				        					<div className="align-right">&nbsp;</div>
						        			<Button className="align-right ma-gray-button"
						        					onClick={handleClose}
						        			>
						        				<i className="fa-solid fa-xmark"></i> Close
						        			</Button>
						        		</td>
				        			</tr>
				        		</tbody>
				        	</Table>
				        	</>
				        :
				        	<>
				        	</>
				        }
		        	</div>

		        	<div id="sqarFourthPage" style={{display: modalPage == 4 ? 'block' : 'none'}}>
			        	<Table>
			        		<tbody>
			        			<tr>
			        				<td colSpan="4" style={{textAlign: 'center', fontSize: 20, backgroundColor: 'rgb(180, 198, 231)'}}>
			        					<b>SUPPORTING DOCUMENTS</b>
			        				</td>
			        			</tr>
			        			<tr>
			        				<td colSpan='4' style={{textAlign: 'left'}}>
			        					<Form.Group controlId="fileInput" className="mb-3">
			        						<Form.Label>ADD SUPPORTING DOCUMENTS (doc,docx,pdf,txt,csv,xlsx | max: 5MB)</Form.Label>
								        	<Form.Control type="file"
								        	className="mb-2"
								        	disabled={!(localStorage.getItem('position') == 'IQC Sr. Supervisor'
												|| localStorage.getItem('position') == 'QC Section Head'
												|| localStorage.getItem('position') == 'QC Assistant Manager'
												|| localStorage.getItem('position') == 'QC Engineer')
											}
								        	onChange={(event) => {
								        		setSelectedFile(event.target.files[0])
								        	}}
								        	/>

								        	<Form.Control type="text"
								        	placeholder='Add Description'
								        	className="mb-2 fileDescription"
								        	defaultValue={fileDescription}
								        	onChange={(event) => setFileDescription(event.target.value)}
								        	/>
								        	{
								        		selectedFile ? 
									        		<Button className="ma-blue-button"
									        		onClick={() => addSupportDocuments()}
									        		>
					        						<i className="fa-solid fa-stapler"></i> ATTATCH FILE

					        						</Button>
					        					:
					        						<Button className="ma-blue-button"
					        						disabled
					        						>			        						
					        						<i className="fa-solid fa-stapler"></i> ATTATCH FILE
					        						</Button>
								        	}		
								        </Form.Group>			        					
			        				</td>
			        			</tr>
			        			<tr>
			        				<th style={{width: 200}}>FILE TYPE</th>
			        				<th>TITLE</th>
			        				<th>DESCRIPTION</th>
			        				<th style={{width: 150}}>ACTION</th>
			        			</tr>
			        			{
			        				generateTableRowScript()
			        			}
			        		</tbody>
			        	</Table>
		        	</div>
					{
						generalStatus == 'CANCELLED' ?
							<Alert variant='danger'>
								<b>Status:</b> CANCELLED &nbsp; &nbsp;
								<b>Reason:</b> {cancelReason}
							</Alert>
						:
							null
					}
		        </Modal.Body>
		    </Modal>
			<CancelSqar
			data={sqarControlNo}
			show={cancelSqarModalShow}
			onHide={(cancelSuccess) => {setCancelSqarModalShow(false); if(cancelSuccess) handleClose()}}
			/>

			<Card className="shadow m-3 mt-4" style={{height: 'auto'}}>
				<Card.Body>
					<div className="row mx-2 pb-2">
						<div className="col-12 col-lg-4">
							<ButtonGroup>
								<Button className="ma-blue-button" onClick={() => viewForProcess()}>For SQAR Process</Button>
								<Button className="selected-tab-button">Generated SQAR</Button>
							</ButtonGroup>
						</div>
						<div className="col">
							<div className="d-flex justify-content-end">
								<Button className="ma-blue-button" onClick={() => refresh()}><i className="fa-solid fa-arrow-rotate-right"></i> Refresh</Button> &nbsp;		          
								<Dropdown>
									<Dropdown.Toggle className="view-dropdown" id="dropdown-basic" style={{width: 90}}>
										{
											(viewTable) ?
												<>Table </>
											:
												<>Cards </>
										}
									</Dropdown.Toggle>
									<Dropdown.Menu>
									{
										(viewTable) ?
											<>
												<Dropdown.Item onClick={() => {
													setForProcess([])
													setViewTable(false)
												}}>
												Cards
												</Dropdown.Item>
											</>
										:
											<>
												<Dropdown.Item onClick={() => {
													setForProcess([])
													setViewTable(true)
												}}>				        
												Table
												</Dropdown.Item>
											</>
									}
									</Dropdown.Menu>
								</Dropdown>	
							</div>
						</div>
						<div className="col-12 col-md-6 col-lg-4">
							<InputGroup>
								<Form.Control
								type="search"
								placeholder="Search"
								id="searchBox"
								aria-label="search"
								aria-describedby="basic-addon2"
								value={searchKeyword}
								onChange={event => searchForSQARProcess(event)}
								/>
								<InputGroup.Text id="basic-addon2" className="ma-blue-background ma-white-font"><i className="fa-solid fa-magnifying-glass"></i></InputGroup.Text>
							</InputGroup>
						</div>
					</div>

					<div className="table-container" style={{height: '65vh'}}>
						{
						(viewTable) ?				
							<Table bordered hover style={{whiteSpace: 'nowrap', fontSize: 14}}>
								<tbody>								
									<tr style={{position: 'sticky', top: 0, zIndex: 1}}>
										<th>#</th>
										<th>Action</th>
										<th>Status</th>							
										<th>Control No</th>
										<th>Prepared By</th>
										<th>Date Prepared</th>
										<th>PO No</th>
										<th>Invoice No</th>
										<th>Part Name</th>							
										<th>Part No</th>							
										<th>Supplier Name</th>								
										<th>Maker Name</th>								
										<th>Reject Rate</th>								
										<th>Date Received</th>
										<th>MatCodeBox</th>
										<th>
											Sub Status
											<br/>
											{/* <Form.Select
											id="select-SubStatus"
											size='sm'
											>
												<option value={0} label="All" />
												{sqarStatusDefinition?.map((item, index) => <option key={index} value={item.SQAR_Status_No} label={item.SQAR_Status} />)}
											</Form.Select> */}
										</th>			
									</tr>
									{sqars}																	
								</tbody>
							</Table>
						:
							<Row>
								{sqars}
							</Row>
						}
						{
							(sqars.length > 0) ?
								<>
								</>
							:
								<Container className="mt-2">
									<Loading/>
								</Container>																					
						}
					</div>

					<Row style={{margin: 0, userSelect: 'none'}}>
						<Col>
							{
								(searchedItemsLength < 1) ?
									<><p style={{marginLeft: 15, fontSize: 14}}>Loading...</p></>
								:
									(currentPage * 100 > searchedItemsLength) ?
										<>
										<p style={{marginLeft: 15, fontSize: 14}}>Showing {currentPage * 100 - 99} to {(currentPage * 100) - (currentPage * 100 - searchedItemsLength)} of {sqarData.length} entries</p>								
										</>
									:
										<>
										<p style={{marginLeft: 15, fontSize: 14}}>Showing {currentPage * 100 - 99} to {currentPage * 100} of {sqarData.length} entries</p>								
										</>
							}
						</Col>
						<Col>
							<Row className="pagination">
								<Col style={{padding: 0, margin: 0}}>
									{
										(currentPage == 1) ?
											<Button className="ma-blue-button" disabled><i className="fa-solid fa-arrow-left"></i></Button>
										:
											<Button 
												className="ma-blue-button"
												onClick={() => prevPage()}
											>
												<i className="fa-solid fa-arrow-left"></i>
											</Button>
									}
								</Col>
								<Col style={{margin:0, paddingLeft: 5, paddingRight: 5, userSelect: 'none'}}>
									<div className="rounded ma-blue-background" 
										style={{height: 40, width: 45, textAlign: 'center', verticalAlign: 'middle', fontSize: 25, color: 'white'}}
									>
										{currentPage}
									</div>
								</Col>
								<Col style={{padding: 0, margin: 0}}>
									{
										(currentPage == Math.ceil(sqarData.length / 100)) ?
											<Button className="ma-blue-button" disabled><i className="fa-solid fa-arrow-right"></i></Button>
										:
											<Button 
												className="ma-blue-button"
												onClick={() => nextPage()}
											>
												<i className="fa-solid fa-arrow-right"></i>
											</Button>
									}
								</Col>
							</Row>
						</Col>
					</Row>
				</Card.Body>
			</Card>
		</>
	)
});

export default Sqars;