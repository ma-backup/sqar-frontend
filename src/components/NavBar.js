import {Navbar, Nav, Dropdown, DropdownButton, Row, Col} from 'react-bootstrap'
import {NavLink} from 'react-router-dom'
import ChangePasswordModal from './ChangePasswordModal'
import { useEffect, useState } from 'react'
import faceIcon from './user-icon2.jpg'
import NotificationOffCanvas from './NotificationsOffCanvas'

export default function NavBar({ OpenSqarModal }){
	
	const [modalShow, setModalShow] = useState(false)
	const [showNotifications, setShowNotifications] = useState(false)
	const [notifications, setNotifications] = useState({data: [], unreadCount: 0})

	const GetNotifications = async () => {
		if(!localStorage.getItem("empNum")) return

		let paramData = 'empNo=' + localStorage.getItem("empNum");
		let response = await fetch(`${process.env.REACT_APP_SQAR_API}/api/notifications/get?${paramData}`);
		let result = await response.json();
		if(result.success){
			setNotifications(result.data)
		}
	} 

	useEffect(() => { GetNotifications(); }, [])

	return(
		<Navbar collapseOnSelect className="navbar">
	        <Navbar.Brand as={NavLink} to="/"><img src="/malogo.png" width="75" height="75" className="img-fluid ma-navbar-logo"/></Navbar.Brand>
	        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
	        <Navbar.Collapse id="responsive-navbar-nav">
	          <Nav className="me-auto">
	          	<Nav.Link as={NavLink} to="/"><h4 className="header-title">SQAR SYSTEM</h4></Nav.Link>       
	          </Nav>
	          <Nav>
	          	{
	          		(localStorage.getItem("empNum") !== null) ?
		          		<>							
							<div
							className={`d-flex justify-content-center align-items-center me-4 ${notifications.unreadCount > 0 ? 'fa-shake' : ''}`}
							style={{cursor: 'pointer', position: 'relative', userSelect: 'none'}}
							onClick={() => setShowNotifications(true)}
							>
								<i className="fa-solid fa-bell fs-3 text-white"></i>
								<span className="badge bg-danger rounded-circle"
								style={{position: 'absolute', top: 15, left: 15, fontSize: 9}}
								>
									{notifications.unreadCount > 0 ? notifications.unreadCount : ''}
								</span>
							</div>
							<NotificationOffCanvas
							notifications={notifications}
							GetNotifications={GetNotifications}
							show={showNotifications}
							setShow={setShowNotifications}
							OpenSqarModal={OpenSqarModal}
							/>

							<DropdownButton
								className="me-3 rounded-circle"
								drop="start"
								variant="primary"			
								title={
									<span>
										{/* <i className="fa-solid fa-circle-user" style={{fontSize: '2rem', color: '#3C5393'}}></i>	 */}
										<img src={faceIcon} height="40" width="40" className="image img-fluid ma-navbar-logo rounded-circle ms-2"/>										
									</span>
								}
								>
								<div className="ms-3">
									<span className="fw-bold text-dark py-0">{localStorage.getItem("fullName")}</span>
									<br/>
									<span className="text-secondary" style={{fontSize: 14}}>{localStorage.getItem("position")}</span>
								</div>
								<Dropdown.Divider/>
								<Dropdown.Item
								onClick={() => {setModalShow(true)}}
								>
									<i className="fa-solid fa-gear"></i> Change Password
								</Dropdown.Item>
								<Dropdown.Divider />
								<Dropdown.Item
								onClick={() => window.location.href = "/logout"}
								>
									<i className="fa-solid fa-right-from-bracket"></i> Log Out
								</Dropdown.Item>
							</DropdownButton>
			          	</>
			        :
			        	<></>
	          	}	          	 
	          </Nav>
	        </Navbar.Collapse>
			<ChangePasswordModal
			show={modalShow}
			onHide={() => setModalShow(false)}
			/>
	    </Navbar>
	)
}