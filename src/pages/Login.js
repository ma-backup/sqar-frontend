import {useState, useEffect, useContext} from 'react'
import {Form, Button, Row, Col, Container, Card} from 'react-bootstrap'
import {Navigate, useNavigate} from 'react-router-dom'

import UserContext from '../UserContext'

import malogo from '../components/malogo.png'
import Swal from 'sweetalert2'

export default function Login(){
	const {user, setUser} = useContext(UserContext)
	const navigate = useNavigate()
	const [empNum, setEmpNum] = useState('');
	const [password, setPassword] = useState('');

	const login = (event) => {
		event.preventDefault()
		fetch(`${process.env.REACT_APP_SQAR_API}/api/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				username: empNum,
				password: password,
				softwareName: 'SQAR'
			})
		})
		.then((response) => response.json())
		.then((result) => {
			if(result){
				localStorage.setItem("fullName", result.Full_Name)
				localStorage.setItem("position", result.Position)
				localStorage.setItem("empNum", result.id)
				setEmpNum('')
				setPassword('')
				setUser({
					empNum: result.id,
					fullName: result.Full_Name
				})
				Swal.fire({
					title: 'Success',
					icon: 'success',
					confirmButtonColor: "#3C5393",
					text: "You are now logged in!"
				})
				window.location.reload()
			}else{
				Swal.fire({
					title: 'Error',
					icon: 'error',
					confirmButtonColor: "#3C5393",
					text: "Wrong username or password."
				})
			}
		})
	}

	useEffect(() => {
		if(localStorage.getItem('empNum') != null){
			window.location.href = '/'
		}
	}, [localStorage])

	return(
		// (localStorage.getItem('empNum') === null) ?
			<Row className="mt-5" style={{margin: 0}}>
				<Col md="3" lg="4">
				</Col>
				<Col md="6" lg="4">
					<Card className="login-card">
				      <Card.Header className="login-card-header py-2"><h3 className="text-white">Login</h3></Card.Header>
				      <Card.Body>
				      	<center><img src={malogo} width="150" height="200" className="img-fluid my-3"/></center>
				      	<Form onSubmit={event => login(event)}>			      	
							<Form.Group controlId="userEmail" className="mb-2">
				                <Form.Label>Employee Number</Form.Label>
				                <Form.Control 
					                type="number" 
					                placeholder="Enter employee number"
					                value={empNum}
					                onChange={event => setEmpNum(event.target.value)}
					                required
				                />
				            </Form.Group>
				            <Form.Group controlId="password">
				                <Form.Label>Password</Form.Label>
				                <Form.Control 
					                type="password" 
					                placeholder="Password" 
					                value={password}
					                onChange={event => setPassword(event.target.value)}
					                required
				                />
				            </Form.Group>  
				            	<Button type="submit" id="submitBtn" className="mt-3 mb-2 login-button">
				            		Log in <i className="fa-solid fa-right-to-bracket"></i>
				            	</Button>     
				        </Form>
				      </Card.Body>			      
				    </Card>
				</Col>
				<Col md="3" lg="4"></Col>
			</Row>
		// :
		// 	<Navigate to="/"/>
	)
}