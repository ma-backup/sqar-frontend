import {Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import {useContext, useEffect} from 'react'

export default function Logout() {
	const {unsetUser} = useContext(UserContext)	
	useEffect(() => {
		unsetUser()
	}, [])
	return(
		<Navigate to="/login"/>
	)
}