import ForSqarProcess from '../components/ForSqarProcess'
import Sqars from '../components/Sqars'
import {ViewProvider} from '../ViewContext'

import {useState, useEffect, forwardRef, useImperativeHandle, useRef } from 'react'
import { Navigate } from 'react-router-dom'

const Main = forwardRef((props, ref) => {

	const [view, setView] = useState({
		viewForProcess: true
	})
	const [forSqarData, setForSqarData] = useState(Array)
	const [sqarData, setSqarData] = useState(Array)
	const [sqarWaiter, setSqarWaiter] = useState(true)
	const [forProcessWaiter, setForProcessWaiter] = useState(true)
	const [majorDefects, setMajorDefects] = useState(null)
	const [sqarStatusDefinition, setSqarStatusDefinition] = useState(null)
	const [techSuppMngrSupplier, setTechSuppMngrSupplier] = useState(null)

	const getSqars = () => {
		fetch(`${process.env.REACT_APP_SQAR_API}/api/sqar`)
		.then(response => response.json())
		.then(result => {
			try {
			  	setSqarData(result)
			}
			catch(err) {
			  
			}		
		})
	}

	const getForSQARProcess = () => {
		fetch(`${process.env.REACT_APP_SQAR_API}/api/for-sqar-process`)
		.then(response => response.json())
		.then(result => {			
			try {
			  	setForSqarData(result)
			}
			catch(err) {
			  
			}		
		})		
	}

	const getMajorDefects = () => {
		fetch(`${process.env.REACT_APP_SQAR_API}/api/get-major-defects`)
		.then((response) => response.json())
		.then((result) =>{
			console.log(result)
			setMajorDefects(result)
		})
	}

	const getSqarStatusDefinition = () => {
		fetch(`${process.env.REACT_APP_SQAR_API}/api/get-sqar-status-definition`)
		.then((response) => response.json())
		.then((result) =>{
			console.log(result)
			setSqarStatusDefinition(result)
		})
	}

	const getTechSuppMngrSupplier = () => {
		fetch(`${process.env.REACT_APP_SQAR_API}/api/get-tech-supp-mngr-supplier`)
		.then((response) => response.json())
		.then((result) => {
			console.log(result)
			setTechSuppMngrSupplier(result)
		})
	}

	useEffect(() => {getTechSuppMngrSupplier()}, [])
	useEffect(() => {getSqarStatusDefinition()},[])
	useEffect(() => {getMajorDefects()}, [])
	useEffect(() => {
		getForSQARProcess()
	}, [forProcessWaiter])

	useEffect(() => {
		getSqars()
	}, [sqarWaiter])

	const childRef = useRef(null);

	const GetData = (item) => {
		childRef.current?.getData(item)
	}

	//expost the function to the parent
	useImperativeHandle(ref, () => {
		return { 
			setView,
			sqarData,
			GetData
		}
	})

	return(
		<>	
			<ViewProvider value={{view, setView, forSqarData, setForSqarData, sqarData, setSqarData, sqarWaiter, setSqarWaiter, forProcessWaiter, setForProcessWaiter, majorDefects, techSuppMngrSupplier, sqarStatusDefinition}}>
				<div className="main-container">
					{
						(localStorage.getItem('empNum') !== null) ?
							(view.viewForProcess) ?
								<ForSqarProcess/>
							:
								 <Sqars ref={childRef} />
						:
							<Navigate to="/login"/>
					}					
				</div>
			</ViewProvider>
		</>
	)
});

export default Main;