import NavBar from './components/NavBar'
import Login from './pages/Login'
import Logout from './pages/Logout'
import {UserProvider} from './UserContext'
import {useState, useRef} from 'react'
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'
import './App.css'
import Main from './pages/Main'

function App() {

  const [user, setUser] = useState({
      empNum: null,
      fullName: null
  })

  const unsetUser = () => {
    localStorage.clear()
    setUser({
			empNum: null,
			fullName: null
		})
  }

  const childRef = useRef(null);

  const OpenSqarModal = (clickedNotif) => {
    if(!childRef.current || !clickedNotif) return

    childRef.current?.setView({
      viewForProcess: false
    });

    let selectedSqar = "";
    childRef.current.sqarData.map((item) => {
      if(item.SQAR_Control_Number == clickedNotif.SqarControlNo){
        selectedSqar = item;
      }
    })

    if(!selectedSqar) return
    setTimeout(() => {
      childRef.current.GetData(selectedSqar)
    }, 300)
  }

  return (
    <>
      <UserProvider value={{user, setUser, unsetUser}}>
       <Router>
        <NavBar OpenSqarModal={OpenSqarModal} />
        <Routes>
          <Route path="/" element={<Main ref={childRef} />}/>
          <Route path="/login" element={<Login/>}/>
          <Route path="/logout" element={<Logout/>}/>          
        </Routes>
       </Router>
      </UserProvider>
    </>
  );
}

export default App;
