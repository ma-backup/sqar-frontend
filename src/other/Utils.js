export function ToReadableDatetime(datetime){
    let originalDatetime = new Date(datetime);

    let formattedDatetime = originalDatetime.toLocaleString('en-US', {
        month: 'short',
        day: '2-digit',
        year: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        hour12: true
    });

    return formattedDatetime;
}